<?php

require_once('servicos.php');
/**
 * Pontuacao
 * 
 * @param string $username
 * @param string $nomeMapa
 * @param int $pontos
 * @return boolean Response string
 * @pw_set global=1 <- Declare this method as global (=not within a class)
 */
function pontuacao($username, $nomeMapa,$pontos){
		$deu = pontuar($username, $nomeMapa,$pontos);
		
		return $deu;
}
?>