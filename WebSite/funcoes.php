<?php
/**
* This class has several function's to backup other classes.
*/
	require("DAL.php");
/**
* This function validates if a user is logged in.
*/	
	function validarSessao(){
		session_start();
		
		if(!isset($_SESSION['username'])){
			header("Location:login.php");
		}
		
	}
/**
* This function obtains a user id based on it's username.
* @return the user id or a error message
*/	
	function obterIDUtilizador(){
		$username = $_SESSION['username'];
		$classDal = new DAL(); 
		$mysqli = $classDal->connect();
		$result = $mysqli->query("SELECT id from Utilizador where username='$username'");
		$row_cnt = $result->num_rows;
				
		if($row_cnt>0){
			$row = mysqli_fetch_assoc($result);
			$id_Utilizador = $row['id'];
			return $id_Utilizador;
		}else{
			return "ERRO";
		}
	}
/**
* This function obtains a username based on it's id.
* @param the user's id
* @return the username or a error message
*/	
	function obterNomeUtilizador($id){
		$classDal = new DAL(); 
		$mysqli = $classDal->connect();
		$result = $mysqli->query("SELECT username from Utilizador where id=$id");
		$row_cnt = $result->num_rows;
				
		if($row_cnt>0){
			$row = mysqli_fetch_assoc($result);
			$nomeUtilizador = $row['username'];
			return $nomeUtilizador;
		}else{
			return "ERRO";
		}
	}
/**
* This function obtains a map id based on it's name.
* @param the map name
* @return the map id or a error message
*/	
	function obterIDMapa($nome){
		$classDal = new DAL(); 
		$mysqli = $classDal->connect();
		$result = $mysqli->query("SELECT id from Mapa where nome='$nome'");
		$row_cnt = $result->num_rows;
				
		if($row_cnt>0){
			$row = mysqli_fetch_assoc($result);
			$id_Mapa = $row['id'];
			return $id_Mapa;
		}else{
			return "ERRO";
		}
	}
/**
* This function reads an xml file and return's it's data.
* @param the xml file name
* @return the data of the xml file or a error message
*/	
	function lerXML($nome_mapa){
		$ficheiro = $nome_mapa.".xml";
		if(file_exists($ficheiro)){
			$xml = file_get_contents($ficheiro);
			return $xml;
		}else{
			return "ERRO"; 
		}

	
	}
/**
* This function checks if a user is admin.
* @param the username
* @return the type of the username or a error message
*/	
	function check_type($utilizador){
		$classDal = new DAL(); 
		$mysqli = $classDal->connect();
			$result = $mysqli->query("SELECT tipo from Utilizador where username='$utilizador'");
			$row_cnt = $result->num_rows;	
			if($row_cnt==1){
				$row=mysqli_fetch_row($result);
				if($row[0]=="admin"){
					return "ADMIN";
				}else if($row[0]=="user"){
					return "USER";
				}
			}
			else{
				return "ERRO";
			}
			
	}
/**
* This function changes the acess to a map only if the user is admin.
* @param the map name and the user type
* @return nothing if the acess was altered or a error message
*/	
	function alterAcess($map_name,$type){
		$classDal = new DAL(); 
		$mysqli = $classDal->connect();
		if(strcmp($type,"ADMIN") == 0){
			$result = $mysqli->query("SELECT acesso from Mapa WHERE nome='$map_name'");
			$row_cnt = $result->num_rows;
			if($row_cnt == 1){
				$row=mysqli_fetch_row($result);
				if($row[0]=="publico"){
				$result2 = $mysqli->query("UPDATE Mapa SET acesso='privado' WHERE nome='$map_name'");
				}else if($row[0]=="privado"){
				$result3 = $mysqli->query("UPDATE Mapa SET acesso='publico' WHERE nome='$map_name'");
				}			
			}else{
				return "ERRO";
			}
		}else{
			return "NAO TEM PERMISSAO";
		}
	}


?>