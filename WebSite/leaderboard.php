<?php
require("funcoes.php");

//iniciar
validarSessao();
$mapas = listaMapas();

?>

<html>
<head>
<meta charset='UTF-8'/>
<title>
</title>
<script type="text/javascript"> 
function contruirLink(){
	var nome = document.getElementById("mapaselect").value;
	window.location = "leaderboard.php?mapa=" + nome;
}

function caixaInicio(){
	var mapa = _GET("mapa");
	if(mapa!=null){
		document.getElementById('mapaselect').value = mapa;
	}
}

function _GET(name){
  var url   = window.location.search.replace("?", "");
  var itens = url.split("&");

  for(n in itens){
    if( itens[n].match(name) ){
      return decodeURIComponent(itens[n].replace(name+"=", ""));
    }
  }
  
  return null;
}
</script>
</head>
<body onload="caixaInicio();">
Mapas:
<?php
if($mapas!="SEM MAPAS!"){
?>
<select id="mapaselect" onchange="contruirLink();">
	<?php
		foreach ($mapas as &$mapa) {
			echo "<option value='".$mapa."'>".$mapa."</option>";
		}
	?>
</select>
<br>
<?php
	if(!isset($_GET['mapa'])){
		if($mapas!="SEM MAPAS!"){
			$posicao = minhaPosicao($mapas[0]);
			if(is_int($posicao)){
				echo "Sua Posição: ".$posicao."º";
				?><br><?php
				$meuRecord = minhaPontuacao($mapas[0]);
				echo "Seu Record: ".$meuRecord;
			}else{
				echo $posicao;
			}
			?><br><?php
			tabela($mapas[0]);
		}
	}else{
		$posicao = minhaPosicao($_GET['mapa']);
		if(is_int($posicao)){
			echo "Sua Posição: ".$posicao."º";
			?><br><?php
			$meuRecord = minhaPontuacao($_GET['mapa']);
			echo "Seu Record: ".$meuRecord;
		}else{
			echo $posicao;
		}
		?><br><?php
		tabela($_GET['mapa']);
	}
?>
<?php
}else{
	echo " $mapas";
}
?>
<br>
<input type='button' name='voltar' value='voltar' onclick="window.location.href='inicio.php'"/>
</body>
</html>

<!-- funcoes -->
<?php
function listaMapas(){
	$mapas = array();

	$classDal = new DAL(); 
	$mysqli = $classDal->connect();
	
	$result = $mysqli->query("SELECT * from Mapa");
	$row_cnt = $result->num_rows;
				
	if($row_cnt>0){
		while($row=mysqli_fetch_assoc($result)){
			array_push($mapas,$row['nome']);
		}
	}else{
		return "SEM MAPAS!";
	}
	
	return $mapas;
}

function minhaPontuacao($nomeMapa){
	$classDal = new DAL(); 
	$mysqli = $classDal->connect();
	
	$idUtilizador = obterIDUtilizador();
	$idMapa = obterIDMapa($nomeMapa);
	
	$result = $mysqli->query("SELECT record from LeaderBoard where id_Mapa=$idMapa AND id_Utilizador=$idUtilizador");
	$row_cnt = $result->num_rows;
				
	if($row_cnt>0){
		$row = mysqli_fetch_assoc($result);
		return intval($row['record']);
	}else{
		return "Sem Pontuação para este MAPA!";
	}
}

function minhaPosicao($nomeMapa){
	$classDal = new DAL(); 
	$mysqli = $classDal->connect();
	
	$idUtilizador = obterIDUtilizador();
	$idMapa = obterIDMapa($nomeMapa);
	
	$result = $mysqli->query("SELECT Rank from LeaderBoard where id_Mapa=$idMapa AND id_Utilizador=$idUtilizador");
	
	$row_cnt = $result->num_rows;
				
	if($row_cnt>0){
		$row = mysqli_fetch_assoc($result);
		return intval($row['Rank']);
	}else{
		return $_SESSION['username']." não se encontra neste ranking";
	}
}

function tabela($nomeMapa){
	$classDal = new DAL(); 
	$mysqli = $classDal->connect();
	
	$idMapa = obterIDMapa($nomeMapa);
	
	$result = $mysqli->query("SELECT * from LeaderBoard where id_Mapa=$idMapa order by Rank");
	$row_cnt = $result->num_rows;
				
	if($row_cnt>0){
		echo "<table border='1'>";
			echo "<thead>";
				echo "<tr>";
					echo "<td>Pos</td>";
					echo "<td>Nome</td>";
					echo "<td>Pontos</td>";
				echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
				while($row=mysqli_fetch_assoc($result)){
					$pos = $row['Rank'];
					$idUtilizador = $row['id_Utilizador'];
					$nomeUtilizador = obterNomeUtilizador($idUtilizador);
					$pontos = $row['record'];
					echo "<tr>";
						if(strcasecmp($nomeUtilizador,$_SESSION['username'])==0){
							echo "<td><b>$pos"."º</b></td>";
							echo "<td><b>$nomeUtilizador</b></td>";
							echo "<td><b>$pontos</b></td>";
						}else{
							echo "<td>$pos"."º</td>";
							echo "<td>$nomeUtilizador</td>";
							echo "<td>$pontos</td>";
						}
					echo "</tr>";
				}
			echo "</tbody>";
		echo "</table>";
	}else{
		echo "Sem Classificação";
	}
}
?>