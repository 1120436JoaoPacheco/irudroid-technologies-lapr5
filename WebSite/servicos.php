<?php
/**
* This class contain's the web services in php.
*/
require("funcoes.php");
/**
* This function autenticates a user.
* @param the user name
* @param the user password
* @return true if the user exists in the database and false if it doesn't
*/
     function autenticar($username,$password){
		$classDal = new DAL(); 
		$mysqli = $classDal->connect();
		
		$result = $mysqli->query("SELECT username, password from Utilizador where username='$username' and password='$password'");
		$row_cnt = $result->num_rows;
		if($row_cnt>0){
			return true;
		}else{
			return false;
		}
    }
	

	function percurso($username,$nomeMapa,$teclas){
		$classDal = new DAL(); 
		$mysqli = $classDal->connect();
		
		$result = $mysqli->query("SELECT id from Utilizador where username='$username'");
		$row_cnt = $result->num_rows;
		if($row_cnt>0){
			$row = mysqli_fetch_assoc($result);
			$id_Utilizador = $row['id'];
			
			$result2 = $mysqli->query("SELECT id from Mapa where nome='$nomeMapa'");
			$row_cnt2 = $result2->num_rows;
			if($row_cnt2>0){
				$row2 = mysqli_fetch_assoc($result2);
				$id_Mapa = $row2['id'];
			}
		}
		$result3 = $mysqli->query("INSERT INTO Percurso(id_Utilizador,id_Mapa) VALUES($id_Utilizador,$id_Mapa)");
		$ultimo = $mysqli->insert_id;
		$tec = explode(',',$teclas);
		
		$contador = 1;
		for($i = 1; $i < count($tec); $i++){
			if($tec[$i]==$tec[$i-1]){
				$contador++;
			}else{
				$anterior = $tec[$i-1];
				$result4 = $mysqli->query("INSERT INTO Tecla(id_Percurso,nome,vezes) VALUES($ultimo,'$anterior',$contador)");
				$contador = 1;
			}
		}
		
		$anterior = $tec[count($tec)-1];
		$result5 = $mysqli->query("INSERT INTO Tecla(id_Percurso,nome,vezes) VALUES($ultimo,'$anterior',$contador)");
	}

/**
* This function insert's or update's a user record on the database.
* @param the user name
* @param the map name
* @param the score
* @return true if the record was beaten or if the it wasn't and false if any error occurred
*/	
	function pontuar($username, $nomeMapa,$pontos){
		$classDal = new DAL(); 
		$mysqli = $classDal->connect();
		
		$result = $mysqli->query("SELECT id from Utilizador where username='$username'");
		$row_cnt = $result->num_rows;
		if($row_cnt>0){
			$row = mysqli_fetch_assoc($result);
			$id_Utilizador = $row['id'];
			
			$result2 = $mysqli->query("SELECT id from Mapa where nome='$nomeMapa'");
			$row_cnt2 = $result2->num_rows;
			if($row_cnt2>0){
				$row2 = mysqli_fetch_assoc($result2);
				$id_Mapa = $row2['id'];
				
				$result3 = $mysqli->query("SELECT record from Mapa_Utilizador where id_Mapa=$id_Mapa AND id_Utilizador=$id_Utilizador");
				$row_cnt3 = $result3->num_rows;
				if($row_cnt3>0){
					$row3 = mysqli_fetch_assoc($result3);
					$recordatual = $row3['record'];
					if($pontos>$recordatual){
						//update
						$result5 = $mysqli->query("UPDATE Mapa_Utilizador SET record = $pontos WHERE id_Mapa = $id_Mapa AND id_Utilizador=$id_Utilizador");
						return true;
					}
					else{
						//NAO BATEU O RECORD
						return true;
					}
				}
				else{
					//Inserir Primeiro Record
					$result4 = $mysqli->query("INSERT INTO Mapa_Utilizador(id_Mapa,id_Utilizador,record) VALUES($id_Mapa,$id_Utilizador,$pontos)");
					return true;
				}
			}
			else{
				//nome do mapa invalido
				return false;
			}
		}
		else{
			//nome do utilizador invalido
			return false;
		}
    }
/**
* This function check's if a map exists in the database and if so it read's an xml file that represents the map.
* @param the map name
* @return a string with the xml file content or a error message if any error occurred
*/
	function mapadata($nomeMapa){
			$classDal = new DAL(); 
			$mysqli = $classDal->connect();
			$result = $mysqli->query("SELECT * FROM Mapa WHERE nome='$nomeMapa'");
			$row_cnt = $result->num_rows;
			if($row_cnt>0){
				$map_data = lerXML($nomeMapa);
				if($map_data == "ERRO"){
					return "ERRO";
				}else{
					return $map_data;
				}
			}else{
			return "ERRO";
			}
		
	
	}
	


?>