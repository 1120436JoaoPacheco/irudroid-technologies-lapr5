#include "soapSoapWebServiceSoapProxy.h" 
#include "SoapWebServiceSoap.nsmap" 
#include <conio.h>

using namespace std;

int main()
{
	SoapWebServiceSoapProxy service;
	bool result = false;
	
	while (!result){
		cout << "Username: ";
		string username;
		cin >> username;
		cout << endl;

		string password = "";
		char ch;
		cout << "Password: ";
		ch = _getch();
		while (ch != 13){//character 13 is enter
			if (ch != 8){
				password.push_back(ch);
				cout << '*';
			}
			ch = _getch();
		}
		cout << endl << endl;

		if (service.login(username, password, result) == SOAP_OK)
		if (result){
			cout << "SUCESSO" << endl;
		}
		else{
			cout << "ERRO" << endl << endl;
		}
		else
			service.soap_stream_fault(std::cerr);


		service.destroy(); // delete data and release memory 
	}

	cout << endl;

	std::cin.get();
}