#include "soapSoapWebServiceSoapProxy.h" 
#include "SoapWebServiceSoap.nsmap" 
#include <conio.h>

using namespace std;

int main()
{
	SoapWebServiceSoapProxy service;
	bool result = false;

	string username;
	while (!result){
		cout << "Username: ";
		cin >> username;
		cout << endl;

		string password = "";
		char ch;
		cout << "Password: ";
		ch = _getch();
		while (ch != 13){//character 13 is enter
			if (ch != 8){
				password.push_back(ch);
				cout << '*';
			}
			ch = _getch();
		}
		cout << endl << endl;

		if (service.login(username, password, result) == SOAP_OK)
		if (result){
			cout << "SUCESSO NO LOGIN" << endl;
		}
		else{
			cout << "ERRO NO LOGIN" << endl << endl;
		}
		else
			service.soap_stream_fault(std::cerr);


		service.destroy(); // delete data and release memory 
	}

	cout << endl;

	std::cin.get();


	//TESTAR PONTOS
	if (service.pontuacao(username, "MAPA1", 201, result) == SOAP_OK)
	if (result){
		cout << "SUCESSO UPLOAD PONTUACAO" << endl << endl;
	}
	else{
		cout << "ERRO UPLOAD PONTUACAO" << endl << endl;
	}
	else
		service.soap_stream_fault(std::cerr);


	service.destroy(); // delete data and release memory 
}