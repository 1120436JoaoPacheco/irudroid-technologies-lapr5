#pragma once
#include "Input.h"
#include "Rotation.h"
#include "PhysicsBox.h"

namespace Engine
{
	class Object;
}

namespace Logic{

	class Player ;
	class Wall;
	class Obstacle;
	class Door;

	class GameObject {
	private:
		int idGameObject;
		Engine::Object* obj;
		glm::vec3 position;
		Engine::Rotation rotation;
		PhysicsBox box;
		bool to_delete;

	public:
		GameObject();
		GameObject(int, Engine::Object*, glm::vec3, Engine::Rotation);
		GameObject::GameObject(glm::vec3 p);
		GameObject::GameObject(PhysicsBox& b, Engine::Object*);
		~GameObject();
		GameObject(const GameObject& go);

		bool getDelete()const{ return to_delete; };
		void setDelete(bool del){ to_delete = del; };
		int getidGameObject() const;
		Engine::Object* getObject() const;
		glm::vec3 getPosition() const;

		Engine::Rotation getMeshRotation() const;
		void setMeshRotation(Engine::Rotation);

		PhysicsBox* getBox() { return &box; }

		virtual bool isColliding(GameObject* gO) 
		{ 
			if (typeid(*this) == typeid(Player) && typeid(*gO) == typeid(Wall))
			{
				printf("Player against the wall\n", typeid(*this), typeid(*gO));
			}
			if (typeid(*this) == typeid(Player) && typeid(*gO) == typeid(Obstacle))
			{
				printf("Player against Obstacle\n", typeid(*this), typeid(*gO));
			}
			if (typeid(*this) == typeid(Player) && typeid(*gO) == typeid(Door))
			{
				printf("Player against Door\n", typeid(*this), typeid(*gO));
			}
			return true; 
		
		}

		void rotateBoxY(float angle) { box.rotate(angle); }

		void setBox(PhysicsBox& b) { box = b; }

		void setidGameObject(int);
		void setObject(Engine::Object*);
		void setPosition(glm::vec3);
		virtual void update(Input&, float delta) = 0;
	};
}