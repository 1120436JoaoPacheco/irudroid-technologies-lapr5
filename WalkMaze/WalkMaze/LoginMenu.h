#pragma once

#include "Menu.h"
#include "Input.h"
#include "Services.h"
namespace Logic
{

	/**
	*	The login menu of the game
	*	@see Menu
	*/
	class LoginMenu : public Menu
	{
	private:
		MenuStruct login, password, button;
		string loginStr, passwordStr;
		bool loginActive, passwordActive, error;
	public:
		LoginMenu() { loginActive = false; passwordActive = false; error = false; };
		~LoginMenu() {};
		
		bool process(Input&);
		void initUp();
		void drawAll();
		void inputLogin(Input&, float, float);
		void inputPassword(Input&, float, float);
		void startInputs(Input&);
		bool validateLogin(Input& input);
	};
}