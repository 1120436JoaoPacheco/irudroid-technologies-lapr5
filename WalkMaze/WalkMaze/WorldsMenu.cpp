#include "WorldsMenu.h"

using namespace Logic;

std::string WorldsMenu::process(Input& input)
{
	startUp();
	drawAll();
	return getMap(input);
}

void WorldsMenu::startUp(){

	float incr = 5;
	
	mapa1.x0 = 1;
	mapa1.y0 = 4;
	mapa1.x1 = 4.25;
	mapa1.y1 = 7;
	mapa1.size = 42;
	mapa1.str = "Mapa 1";

	mapa2.x0 = mapa1.x0 + incr;
	mapa2.y0 = mapa1.y0;
	mapa2.x1 = mapa1.x1 + incr;
	mapa2.y1 = mapa1.y1;
	mapa2.size = mapa1.size;
	mapa2.str = "Mapa 2";
}

void WorldsMenu::drawAll()
{
	drawViewport();

	glm::vec3 boxColor(0.5f, 0.5f, 0.5f);
	createBox(glm::vec2(mapa1.x0, mapa1.y0), glm::vec2(mapa1.x1, mapa1.y1), boxColor);
	createBox(glm::vec2(mapa2.x0, mapa2.y0), glm::vec2(mapa2.x1, mapa2.y1), boxColor);

	glm::vec3 textColor(0, 0, 1);
	createText(mapa1.size*1.5, "Walk the Maze", textColor, glm::vec2(2, 1.5));
	createText(mapa1.size, mapa1.str, textColor, glm::vec2(mapa1.x0 + 0.25, mapa1.y1 - mapa1.y1*0.25));
	createText(mapa2.size, mapa2.str, textColor, glm::vec2(mapa2.x0 + 0.25, mapa2.y1 - mapa1.y1*0.25));
}

std::string WorldsMenu::getMap(Input& input)
{
	float mouseX, mouseY;
	getMouseCoordinates(input, mouseX, mouseY);
	
	bool m1 = mouseX > mapa1.x0 && mouseY > mapa1.y0 && mouseX < mapa1.x1 && mouseY < mapa1.y1;
	bool m2 = mouseX > mapa2.x0 && mouseY > mapa2.y0 && mouseX < mapa2.x1 && mouseY < mapa2.y1;

	Services s;
	std::string mapa, mapaXML;
	bool result;

	if (input.getKeyPressed(Key::MouseClick) && m1)
	{
		mapa = "MAPA1";
		result = s.downmaps(mapa, mapaXML);
		if (!result)
			return "";
		//printf("Mapa 1");
		return mapaXML;
	} else if (input.getKeyPressed(Key::MouseClick) && m2)
	{
		mapa = "MAPA2";
		result = s.downmaps(mapa, mapaXML);
		if (!result)
			return "";
		//printf("Mapa 2");
		return mapaXML;
	}
	
	return "";
}
