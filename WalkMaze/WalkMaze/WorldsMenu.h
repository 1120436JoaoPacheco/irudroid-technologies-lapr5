#pragma once

#include "Menu.h"
#include "Services.h"

namespace Logic
{

	/**
	*	The world selection menu of the game
	*	@see Menu
	*/
	class WorldsMenu : public Menu
	{
	private:
		MenuStruct mapa1, mapa2;
	public:
		WorldsMenu() {};
		~WorldsMenu() {};

		std::string process(Input&);
		void startUp();
		void drawAll();
		std::string getMap(Input&);
	};
}