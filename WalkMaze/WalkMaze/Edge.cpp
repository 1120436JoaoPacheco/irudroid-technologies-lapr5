#include "Edge.h"

using namespace Logic;

int Edge::nextid = 0;

Edge::Edge(Node *noini, Node *nofin, float weight, float width) : weight(weight), width(width) {
	 id = nextid++;
	 noi = noini;
	 nof = nofin;
}

 Edge::~Edge(){

}

Edge::Edge(){
	 id = nextid++;
}

Node* Edge::getNoi() const {
	return noi;
}
Node* Edge::getNof() const {
	return this->nof;
}
float Edge::getWidth() const{
	return this->width;
}
float Edge::getWeight() const{
	return this->weight;
}

int Edge::getId()const
{
	return id;
}

Object* Edge::getObject()const{
	return object;
}

void Edge::setNof(Node *no){
	nof = no;
}
void Edge::setNoi(Node *no){

}
void Edge::setWeight(float w){
	weight = w;
}
void Edge::setWidth(float w){
	width = w;
}


void Edge::printEdge(Edge edge){
	cout << "No in�cio:";
	edge.getNoi()->printNode();
	cout << "N� final:";
	edge.getNof()->printNode();
	cout<< endl;
	cout <<  "Weight:" << edge.getWeight()<< "Width:" << edge.getWidth() << endl;
}