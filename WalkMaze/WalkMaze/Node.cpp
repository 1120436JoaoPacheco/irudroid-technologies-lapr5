
#include "Node.h"

using namespace Logic;

int Node::nextid = 0;

 Node::Node(vec3& position, float width) : position(position), width(width) {
	
	 id = nextid++;
 }

 Node::Node(vec3& position) : position(position), width(0) {

	 id = nextid++;
 }

 Node::~Node(){

 }

 Node::Node(){
	 id = nextid++;

 }

 void Node::setPosition(vec3 newpos){

	 position = newpos;
 }

 void Node::setWidth(float newwidth){

	 width = newwidth;
 }

 void Node::setObject(Object *newidNodeScene){
	 object = newidNodeScene;
 }
 float Node::getWidth()const{

	 return width;
 }

 vec3 Node::getPosition()const{

	 return position;
 }
 int Node::getId()const
 {
	 return id;
 }
 
 Object* Node::getObject()const{
	 return object;
 }

 
void  Node::printNode(){
	cout << "X:" << position.x << "Y:" << position.y << "Z:" << position.z <<"Width"<<width<< endl;
}





