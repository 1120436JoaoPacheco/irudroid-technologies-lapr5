#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <urlmon.h>
#pragma comment(lib, "urlmon.lib")
using namespace std;

/**
*  This class is responsable for the download of all components of the game. Such as
*  textures,sounds,enemies....
*  A Door has two types: Connection or Exit.
*/

class Download{

private:

	void downSounds();
	void downTextures();
	void downEnemies();
	void downObstacles();
	void downPlayer();

public:
	Download();
	~Download();

	void downloadAll();

};


