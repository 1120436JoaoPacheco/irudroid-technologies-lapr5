#pragma once

#include "Camera.h"

using namespace Logic;

/*
	FIXME: ID IS 0 ..
	*/
glm::vec3 Camera::getDirection()
{

	return direction;
}

Camera::Camera(Engine::Object* obj)
	: GameObject(0, obj, glm::vec3(0, 0, -3), Engine::Rotation()),
	target(NULL), angle_y(0), angle_x(0), type(Engine::CameraType::TVS)

{

}

Camera::Camera(Engine::Object* cam, GameObject * target)
	: GameObject(0, cam, glm::vec3(0, 0, 0), Engine::Rotation()),
	target(target), angle_x(0), angle_y(0), type(Engine::CameraType::FPS)
{
}

Camera::Camera(const Camera& c) : GameObject(c)
{
	target = c.target;
	type = c.type;
	angle_x = c.angle_x;
	angle_y = c.angle_y;
	direction = c.direction;
}
void Camera:: setType(Engine::CameraType type)
{
	this->type = type;
	switch (type)
	{
	case Engine::CameraType::FPS:
		angle_x = angle_y = 0;
		break;
	case Engine::CameraType::TPS:
		angle_x = 90;
		angle_y = -180;
		break;
	case Engine::CameraType::TVS:
		angle_x = angle_y = 0;
		break;
	default:
		break;
	}
}


void Camera::update(Input& i, float delta)
{
	//Update position
	if (target != NULL)
		setPosition(target->getPosition());

	switch (type)
	{
	case Engine::CameraType::FPS:
		updateFPSCamera(i, delta);
		break;
	case Engine::CameraType::TPS:
		updateTPSCamera(i, delta);
		break;
	case Engine::CameraType::TVS:
		updateTVSCamera(i, delta);
		break;
	default:
		break;
	}

	Engine::GetCameraDirectionMessage g(0);
	getObject()->sendMessage(&g);
	direction = *g.getFront();
}
void Camera::updateFPSCamera(Input& i, float delta)
{
	glm::vec2 mouseDelta = i.getMouseDelta();

	angle_y += mouseDelta.x * 0.05f;
	angle_x += mouseDelta.y* 0.05f;

	if (angle_y >= 360 || angle_y <= -360)
	{
		angle_y = 0;
	}

	if (angle_x >= 360 || angle_x <= -360)
	{
		angle_x = 0;
	}

	glm::vec3 temp = target->getPosition();
	temp.y += 1.5f;
	Engine::SetFpsCamMessage camMessage(0, &temp, angle_y, angle_x); // Create message

	getObject()->sendMessage(&camMessage); // Send To Object
}

void Camera::updateTPSCamera(Input& i, float delta)
{
	glm::vec2 mouseDelta = i.getMouseDelta();


	angle_y += mouseDelta.x;
	angle_x += mouseDelta.y;

	if (i.getKeyPressed(Key::RightPlayer)){
		angle_y -= 2;
	}
	if (i.getKeyPressed(Key::LeftPlayer)){
		angle_y += 2;
	}

	if (angle_y >= 360 || angle_y <= -360)
	{
		angle_y = 0;
	}

	if (angle_x >= 360 || angle_x <= -360)
	{
		angle_x = 0;
	}
	//update camera

	Engine::SetTpsCamMessage camMessage(0, &target->getPosition(), angle_x, angle_y, 8); // Create message
	getObject()->sendMessage(&camMessage); // Send To Object

}

void Camera::updateTVSCamera(Input& i, float delta)
{
	//update camera
	glm::vec2 mouseDelta = i.getMouseDelta();

	angle_x += mouseDelta.x* 0.05f;

	if (i.getKeyPressed(Key::RightPlayer)){
		angle_x -= 0.05;
	}
	if (i.getKeyPressed(Key::LeftPlayer)){
		angle_x += 0.05;
	}

	Engine::SetTpvCamMessage camMessage(0, &target->getPosition(), 20, angle_x); // Create message
	getObject()->sendMessage(&camMessage); // Send To Object
}