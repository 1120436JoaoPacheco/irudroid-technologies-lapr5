#pragma once

#include "Scene.h"
#include "Input.h"
#include "GameObject.h"
#include "SceneRenderer.h"
#include "SetTpsCamMessage.h"
namespace Logic
{
		/**
		*  The camera encapsulation of the Camera object
		*/
		class Camera : public GameObject{

		public:
			Camera(Engine::Object* obj);
			Camera(Engine::Object*, GameObject* target);
			Camera(const Camera&);
			~Camera() {}

			GameObject* getTarget() { return target; }
			float getAngleX() { return angle_x; }
			float getAngleY() { return angle_y; }
			glm::vec3 getDirection();
			void setTarget(GameObject* o) { target = o; }
			void setAngleX(float x) { if (x > 360 || x < -360) angle_x = 0; else angle_x = x; }
			void setAngleY(float y) { if (y > 360 || y < -360) angle_y = 0; else angle_y = y; }
			
			void setType(Engine::CameraType type);
			virtual void update(Input&, float delta);

			Engine::CameraType getType() { return type; }
		private:

			void updateFPSCamera(Input&, float delta);
			void updateTPSCamera(Input&, float delta);
			void updateTVSCamera(Input& i, float delta);


			GameObject* target;
			float angle_y;
			float angle_x;
			glm::vec3 direction;
			Engine::CameraType type;
		};


	}
