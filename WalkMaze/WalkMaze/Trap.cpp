#include "Trap.h"

using namespace Logic;
using namespace Engine;

Trap::Trap(){}

Trap::Trap(int id,glm::vec3 pos) : id(id), pos(pos){

	PhysicsBox trap_box(pos, glm::vec3(0.05f, 1.0f, 0.05f));
	setBox(trap_box);
	black = false;
}

Trap::~Trap(){}

void Trap::update(Input& i, float delta ){}

void Trap::drawTrap(Engine::Scene *sc){
	 obj = sc->addNewObject();

	Texture texttrap = TextureFactory::GetInstance()->createTexture("trap.png");

	 mesh = new Mesh(true);

	glm::vec3 v1(pos.x - 1, -0.4999f, pos.z - 1);
	glm::vec3 v2(pos.x - 1, -0.4999f, pos.z + 1);
	glm::vec3 v3(pos.x + 1, -0.4999f, pos.z - 1);
	glm::vec3 v4(pos.x + 1, -0.4999f, pos.z + 1);

	QuadPrimitive quad(v3,v4,v1,v2);

	mesh->addPrimitive(quad);

	mesh->setTexture(texttrap);
	compo = new RenderComponent(mesh);
	obj->addComponent(compo);
}

bool Trap::isColliding(GameObject* gO){
	if (typeid(*gO) == typeid(Player))
	{
		//printf("Trap against the player\n", typeid(*this), typeid(*gO));
		changeblack();
	}
	return true;
}

void Trap::changeblack(){
	if (black == false){
		black = true;
		Texture textblack = TextureFactory::GetInstance()->createTexture("trapblack.png");
		mesh = new Mesh(true);
		glm::vec3 v1(pos.x - 1, -0.4999f, pos.z - 1);
		glm::vec3 v2(pos.x - 1, -0.4999f, pos.z + 1);
		glm::vec3 v3(pos.x + 1, -0.4999f, pos.z - 1);
		glm::vec3 v4(pos.x + 1, -0.4999f, pos.z + 1);

		QuadPrimitive quad(v3, v4, v1, v2);

		mesh->addPrimitive(quad);
		mesh->setTexture(textblack);
		compo = new RenderComponent(mesh);
		obj->addComponent(compo);
		
	}
}