#pragma once
#include "Scene.h"
#include "Input.h"
#include "QuadPrimitive.h"
#include "RenderComponent.h"
#include "Texture.h"
#include "TextureFactory.h"
#include <vector>
#include "GameObject.h"

using namespace std;
/**
*  This class represents an object Trap child of GameObject.
*
*  @see GameObject
*
*/
namespace Logic
{

	class Trap : public GameObject{

	private:
		int id;
		glm::vec3 pos;
		bool black;
		Engine::Mesh* mesh;
		Engine::Object* obj;
		Engine::RenderComponent* compo;
	public:
		Trap();
		Trap(int,glm::vec3);
		~Trap();

		int getID(){ return id; };

		glm::vec3 getPos(){ return pos; };

		void update(Input&, float delta);
		/**
		*  Draw a trap in the Scene.
		* @see Scene
		*/
		void drawTrap(Engine::Scene *s);

		bool isColliding(GameObject*);
		/**
		*  The animation of the trap is created when the player falls into the trap.
		*
		*/
		void changeblack();
		void changeoriginal();
	};


}