#pragma once

#include "glm\vec3.hpp"
#include <vector>
#include "Input.h"
/**
*  Class that is used for saving the route of the Player, using the Input Keys.
*  @see Player
*  @see Input
*/

namespace Logic{

	class Path{

	private:
		std::vector<Input> teclas;
	
	public:
		Path();
		~Path();

		std::vector<Input> getTeclas() { return teclas; };
		/**
		*  Insert the pressed key in a list.
		*/

		void addKey(Input);

		int getSize();

	};




}