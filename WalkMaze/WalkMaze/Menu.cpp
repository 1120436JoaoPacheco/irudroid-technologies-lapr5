#include "Menu.h"

using namespace Logic;
std::string inputText;

void Menu::start(GLFWwindow* wind)
{
	window = wind;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	glfwSetKeyCallback(window, invoke);

	glClearColor(1, 1, 1, 0);
	glDisable(GL_LIGHTING);
	glDisable(GL_CULL_FACE);
	glDisable(GL_TEXTURE);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_DEPTH);
}

void Menu::end()
{
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	glfwSetKeyCallback(window, NULL);

	glEnable(GL_LIGHTING);
	glEnable(GL_CULL_FACE);
	glEnable(GL_TEXTURE);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH);
}

void Menu::drawViewport()
{
	int w, h;
	glfwGetWindowSize(window, &w, &h);
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, ortho_right, ortho_bottom, 0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void Menu::createText(int size, std::string text, glm::vec3 color, glm::vec2 position)
{
		// Create a pixmap font from a TrueType file.
		FTGLPixmapFont font("Fonts/arial.ttf");
		if(font.Error())
			return;
	
		font.FaceSize(size);

		const char* textChar = text.c_str();
		
		glPushMatrix();
			glRasterPos3d(position.x, position.y, 1);
			glPixelTransferf(GL_RED_BIAS, color.r);
			glPixelTransferf(GL_GREEN_BIAS, color.g);
			glPixelTransferf(GL_BLUE_BIAS, color.b);
			font.Render(textChar);
		glPopMatrix();

		glPixelTransferf(GL_RED_BIAS, 0);
		glPixelTransferf(GL_GREEN_BIAS, 0);
		glPixelTransferf(GL_BLUE_BIAS, 0);
}

void Menu::createBox(glm::vec2 pos1, glm::vec2 pos2, glm::vec3 color)
{
	glLoadIdentity();
	glColor3f(color.r, color.g, color.b);
	glRectf(pos1.x, pos1.y, pos2.x, pos2.y);
}

void invoke(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if (action == GLFW_PRESS)
	{
		if ((key >= 'A' && key <= 'Z'))
		{
			if (mods == GLFW_MOD_SHIFT)
				inputText += (char)key;
			else
				inputText += tolower((char)key);
		}

		if ((key == GLFW_KEY_SPACE))
			inputText += ' ';

		if ((key >= '0' && key <= '9'))
			inputText += (char)key;

	}

	if (action == GLFW_PRESS || action == GLFW_REPEAT)
	{
		if (key == GLFW_KEY_BACKSPACE)
		{
			if (inputText.length() > 0)
				inputText = inputText.substr(0, inputText.length() - 1);
		}
	}
}

void Menu::getMouseCoordinates(Input& input, float &mouseX, float &mouseY){
	input.update(window, false, 0); 
		
	int width, height;
	glfwGetWindowSize(window, &width, &height);
	mouseX = input.getMousePos().x / width * ortho_right;
	mouseY = input.getMousePos().y / height * ortho_bottom;
}