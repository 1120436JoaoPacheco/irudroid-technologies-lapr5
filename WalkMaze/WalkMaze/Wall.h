#pragma once


#include "glm\glm.hpp"

#include "SetTransformMessage.h"
#include "CubeMeshFactory.h"
#include "GameObject.h"
namespace Logic{
	class Wall : public GameObject {

	private:
		glm::vec3 extent;
		float rotation;

	public:
		Wall(glm::vec3, glm::vec3, Engine::Object* o);
		Wall(PhysicsBox, Engine::Object* o);
		~Wall();
		void update(Input&,float) {  }
	};
}