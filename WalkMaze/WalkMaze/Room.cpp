#include "Room.h"

using namespace Logic;

bool day = true;


Character* c;
Player* Room::getPlayer()const{
	return player;
}



void Room::setPlayer(Player* p){
	player = p;
}

Room::Room(pugi::xml_node& n)
: dimension(100, 50, 100), scene()
{

	//create room
	idRoom = stoi(n.attribute("id").value());
	graph.leGrafo(&n.child("graph"));
	graph.drawGraph(scene, objectList);

	//desenha as portas
	drawRoomWalls(n, scene);

	//desenha as armadilhas
	drawTraps(n, scene);

	//desenha os obstaculos
	drawRoomObjects(n, scene);


	//desenha os enimigos


	drawRoomEnemies(n, scene);


	//desenha os bonus 
	drawRoomBonus(n, scene);
	

	glm::vec3 position(2.0f, 0.6f, 2.0f);
	player = new Player(0,position,  scene);

	projetil = NULL;

	
	//Defini��o das luzes
	glm::vec4 gl_ambient(0.1, 0.1, 0.1, 0.1), gl_diffuse(0.7, 0.7, 0.7, 1.0),
		gl_specular(1.0, 1.0, 1.0, 1.0);
	glm::vec3 gl_spot_direction(0.0, 0.0, -1.0);
	GLfloat gl_spot_cutoff(180), gl_spot_exponent(1.0), gl_linear_atteniation(0.3f), quadratic_atteniation(0.05);

	glm::vec4 gl_position(25.0f, 7, 20.0f, 1.0);

	lc = scene.addNewObject();
	//--------------------------------
 
	glm::vec4 gl_position7(5.0f, 10.0f, 5.0f, 0.0);
	GLenum gl_light7(GL_LIGHT7);
	GLfloat gl_spot_cutoff1(22.5);
	glm::vec3 gl_spot_direction1(player->getCamera().getDirection());

	light = Light(gl_light7, gl_ambient, gl_diffuse, gl_specular,
		gl_position7, gl_spot_direction1, gl_spot_cutoff1, gl_spot_exponent, gl_linear_atteniation, quadratic_atteniation);
	LightComponent *lcomp = new Engine::LightComponent();
	lc->addComponent(lcomp);
	lc->sendMessage(&Engine::SetLightMessage(0, &light));

	sb = new Skybox();

	Object* obj = scene.addNewObject();
	obj->addComponent(sb);

	std::vector<Transform> transforms;
	transforms.push_back(Transform{ glm::vec3(50, 0, 50), glm::vec3(1, 1, 1), Rotation() });
	obj->sendMessage(&Engine::SetTransformMessage(0, &transforms));

	day = !day;
	sb->sendMessage(&DayTimeMessage(0, DayTime::NIGHT));



}

Room::~Room(){}

int Room::getIdRoom()const{
	return idRoom;
}
Graph Room::getGraph()const{
	return graph;
}

vec3 Room::getDimension()const{
	return dimension;
}

Scene Room::getScene()const{
	return scene;
}

vector<GameObject*> Room::getObjectList()const{
	return objectList;
}

vector<Door*> Room::getDoorlist()const{
	return listaportas;
}

void Room::drawRoom(int width, int height){
	scene.renderScene(width, height);
}

void Room::updateRoom(Input& i, float delta)
{

	for each (GameObject* ol in objectList){
		ol->update(i, delta);
		if (ol->getDelete())
			deleteList.push_back(ol);
	}

	player->update(i, delta);
	if (i.getKeyPressed(Key::Shoot)){
		vec3 playerfront = player->getPlayerFront();
		playerfront = glm::normalize(playerfront);
		float rot_y = player->calculate_rotate_angle(playerfront);
		createProjetil(player->getPosition(), playerfront, rot_y);
	}
	if (projetil != NULL){
		if (projetil->getDestroy() == false){
			projetil->update(i, delta);
		}
		else{
			int id = projetil->getidProjectil();
			auto it = find(begin(objectList), end(objectList), projetil);

			objectList.erase(it);
			scene.removeObject(id);

			delete(projetil);
			projetil = NULL;
		}
	}
	if (i.getKeyPressed(Key::DayTime))
	{

		sb->sendMessage(&DayTimeMessage(0, day ? DayTime::NIGHT : DayTime::DAY));
		day = !day;

		if (day)
		{
			glDisable(GL_FOG);


		}
		else
		{
			bool   gp;                      // G Pressed? ( New )
			GLuint filter;                      // Which Filter To Use
			GLuint fogfilter = 1;                    // Which Fog To Use
			glClearColor(0.5f, 0.5f, 0.5f, 1.0f);          // We'll Clear To The Color Of The Fog ( Modified )
			GLfloat fogColor[] = { 0.5f, 0.5f, 0.5f, 1 };
			glFogfv(GL_FOG_COLOR, fogColor);

			glFogi(GL_FOG_MODE, GL_EXP2);
			glHint(GL_FOG_HINT, GL_NICEST);
			//glFogi(GL_FOG_MODE, GL_LINEAR);
			//glFogf(GL_FOG_START, 10.0f);
			//glFogf(GL_FOG_END, 20.0f);
			glFogf(GL_FOG_DENSITY, 0.015f);
			//glEnable(GL_FOG);       
			glEnable(GL_FOG);
		}
	}

	glm::vec4 s = glm::vec4(player->getPosition(), 1);
	this->light.setGl_position(s);
	glm::vec3 d = player->getCamera().getDirection();
	this->light.setGl_spot_direction(d);

	lc->sendMessage(&Engine::SetLightMessage(0, &light));

	PhysicsEngine e;
	e.addGameObject(player);
	if (projetil != NULL)
	{
		e.addGameObject(projetil);
	}
	e.stepObjects(objectList);

	for each (GameObject* ol in deleteList){
		int id = ol->getObject()->getObjectId();
		auto it = find(begin(objectList), end(objectList), ol);

		objectList.erase(it);
		scene.removeObject(id);
		delete(ol->getObject());
	}

	deleteList.clear();
}
void Room::drawRoomEnemies(pugi::xml_node& n, Scene& s){
	pugi::xml_node ramoenemies = n.child("enemies");
	if (ramoenemies == NULL)
		return;
	int numenemies = stoi(ramoenemies.attribute("number").value());



	int idfirstenemy = stoi(ramoenemies.first_child().attribute("id").value());
	int max = idfirstenemy + numenemies;

	for (int i = 0; i < max; i++){
		string idtemp = to_string(i);
		char* idobject = (char*)idtemp.c_str();
		pugi::xml_node enemy = ramoenemies.find_child_by_attribute("id", idobject);
		int id = stoi(enemy.attribute("id").value());
		float x = stof(enemy.attribute("x").value());
		float y = stof(enemy.attribute("y").value());
		float z = stof(enemy.attribute("z").value());
		string object_type = enemy.attribute("type").value();

		ModelType MODEL_TYPE;

		if (object_type == "ALIEN"){
			MODEL_TYPE = ModelType::ENEMY_ALIEN;

			objectList.push_back(new Enemy(s, MODEL_TYPE, glm::vec3(x, y, z)));
		}
		else if (object_type == "RAPTOR"){
			MODEL_TYPE = ModelType::ENEMY_RAPTOR;
			objectList.push_back(new Enemy(s, MODEL_TYPE, glm::vec3(x, y, z)));

		}

	}


}

void Room::drawRoomObjects(pugi::xml_node& n, Scene& s){


	glFogi(GL_FOG_MODE, GL_EXP2);
	//glFogi(GL_FOG_MODE, GL_LINEAR);
	//glFogf(GL_FOG_START, 10.0f);
	//glFogf(GL_FOG_END, 20.0f);
	glFogf(GL_FOG_DENSITY, 0.01f);

	pugi::xml_node ramoobjecto = n.child("objects");
	int numObjs = stoi(ramoobjecto.attribute("number").value());



	int idfirsrobject = stoi(ramoobjecto.first_child().attribute("id").value());
	int max = idfirsrobject + numObjs;

	for (int i = 0; i < max; i++){
		string idtemp = to_string(i);
		char* idobject = (char*)idtemp.c_str();
		pugi::xml_node object = ramoobjecto.find_child_by_attribute("id", idobject);
		int id = stoi(object.attribute("id").value());
		float x = stof(object.attribute("x").value());
		float y = stof(object.attribute("y").value());
		float z = stof(object.attribute("z").value());

		string object_type = object.attribute("type").value();

		ModelType MODEL_TYPE;

		if (object_type == "CRATE"){
			MODEL_TYPE = ModelType::CRATE;

			objectList.push_back(new Obstacle(0, s, MODEL_TYPE, glm::vec3(x, y, z)));
		}
		else if (object_type == "SHELVE"){
			MODEL_TYPE = ModelType::SHELVE;
			objectList.push_back(new Obstacle(0, s, MODEL_TYPE, glm::vec3(x, y, z)));

		}
		else if (object_type == "STATUE"){
			MODEL_TYPE = ModelType::STATUE;
			objectList.push_back(new Obstacle(0, s, MODEL_TYPE, glm::vec3(x, y, z)));

		}
		else  if (object_type == "WRECKEDTABLE"){


			MODEL_TYPE = ModelType::WRECKEDTABLE;
			objectList.push_back(new Obstacle(0, s, MODEL_TYPE, glm::vec3(x, y, z)));

		}
		else  if (object_type == "BARREL"){


			MODEL_TYPE = ModelType::BARREL;
			objectList.push_back(new Obstacle(0, s, MODEL_TYPE, glm::vec3(x, y, z)));

		}

		else  if (object_type == "PIANO"){


			MODEL_TYPE = ModelType::PIANO;
			objectList.push_back(new Obstacle(0, s, MODEL_TYPE, glm::vec3(x, y, z)));

		}
		else  if (object_type == "PLANT"){


			MODEL_TYPE = ModelType::PLANT;
			objectList.push_back(new Obstacle(0, s, MODEL_TYPE, glm::vec3(x, y, z)));

		}

		else  if (object_type == "CHAIR"){


			MODEL_TYPE = ModelType::CHAIR;
			objectList.push_back(new Obstacle(0, s, MODEL_TYPE, glm::vec3(x, y, z)));

		}
		else{
			MODEL_TYPE = ModelType::WARDROBE;
			objectList.push_back(new Obstacle(0, s, MODEL_TYPE, glm::vec3(x, y, z)));

		}

	}
}


void Room::drawRoomWalls(pugi::xml_node& n, Scene& s) {


	pugi::xml_node ramoporta = n.child("doors");
	int numportas = stoi(ramoporta.attribute("number").value());

	int idprimeiraporta = stoi(ramoporta.first_child().attribute("id").value());

	int max = idprimeiraporta + numportas;
	for (int i = idprimeiraporta; i < max; i++){
		string idtemp = to_string(i);
		char* idporta = (char*)idtemp.c_str();
		pugi::xml_node porta = ramoporta.find_child_by_attribute("id", idporta);
		int id = stoi(porta.attribute("id").value());


		string tipoporta = porta.attribute("type").value();
		float x = stof(porta.attribute("x").value());

		float y = stof(porta.attribute("y").value());

		float z = stof(porta.attribute("z").value());


		DoorType type;

		if (tipoporta == "Connection"){
			type = DoorType::Connection;
			int nextR = stoi(porta.attribute("nextroom").value());
			int nextD = stoi(porta.attribute("nextdoor").value());


			objectList.push_back(new Door(i, nextR, nextD, type));
			//((Door*)objectList.back())->setPosition(glm::vec3(x, y, z));
			((Door*)objectList.back())->setBox(PhysicsBox(glm::vec3(x, y, z), glm::vec3(0, 0, 0)));
			((Door*)objectList.back())->drawDoor(&scene);
			listaportas.push_back(((Door*)objectList.back()));
		}
		else{
			type = DoorType::Exit;

			objectList.push_back(new Door(i, type));
			//	((Door*)objectList.back())->setPosition(glm::vec3(x, y, z));
			((Door*)objectList.back())->setBox(PhysicsBox(glm::vec3(x, y, z), glm::vec3(0, 0, 0)));
			((Door*)objectList.back())->drawDoor(&scene);
			listaportas.push_back(((Door*)objectList.back()));

		}



	}

}

void Room::drawTraps(pugi::xml_node& n, Scene& s) {
	pugi::xml_node ramotraps = n.child("traps");
	if (ramotraps == NULL)
		return;

	int numtraps = stoi(ramotraps.attribute("number").value());

	int idprimeiratrap = stoi(ramotraps.first_child().attribute("id").value());
	int max = idprimeiratrap + numtraps;

	for (int i = idprimeiratrap; i < max; i++){

		string idtemp = to_string(i);
		char* idtrap = (char*)idtemp.c_str();

		pugi::xml_node trap = ramotraps.find_child_by_attribute("id", idtrap);
		int id = stoi(trap.attribute("id").value());

		float x = stof(trap.attribute("x").value());
		float y = stof(trap.attribute("y").value());
		float z = stof(trap.attribute("z").value());

		Trap *nova = new Trap(i, glm::vec3(x, y, z));
		objectList.push_back(nova);

		nova->drawTrap(&scene);
	}

}


void Room::drawRoomBonus(pugi::xml_node& n, Scene& s){
	pugi::xml_node ramobonus = n.child("bonus");
	if (ramobonus == NULL)
		return;


	int numBonus = stoi(ramobonus.attribute("number").value());

	int idprimeiroBonus = stoi(ramobonus.first_child().attribute("id").value());
	int max = idprimeiroBonus + numBonus;

	for (int i = idprimeiroBonus; i < max; i++){

		string idtemp = to_string(i);
		char* idbonus = (char*)idtemp.c_str();

		pugi::xml_node bonusnode = ramobonus.find_child_by_attribute("id", idbonus);
		int id = stoi(bonusnode.attribute("id").value());

		float x = stof(bonusnode.attribute("x").value());
		float y = stof(bonusnode.attribute("y").value());
		float z = stof(bonusnode.attribute("z").value());

		int value = stoi(bonusnode.attribute("value").value());
		string tipoBonus = bonusnode.attribute("type").value();

		ModelType MODEL_TYPE;
		if (tipoBonus == "HEALTH"){
			MODEL_TYPE = ModelType::LIFEBONUS;

			objectList.push_back(new Bonus(s, MODEL_TYPE, glm::vec3(x, y, z), value));
		}
		else if (tipoBonus == "POINTS"){
			MODEL_TYPE = ModelType::POINTBONUS;
			objectList.push_back(new Bonus(s, MODEL_TYPE, glm::vec3(x, y, z), value));

		}


	}
}
void Room::createProjetil(glm::vec3 pos, glm::vec3 pfront, float ret_y){
	if (projetil == NULL){
		projetil = new Projectil();

		projetil->drawBullet(&scene, pos, pfront, ret_y);
		objectList.push_back(projetil);
	}

}