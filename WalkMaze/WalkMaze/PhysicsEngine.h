#pragma once
#include <stack>

#include "GameObject.h"
class PhysicsEngine
{
public:
	
	PhysicsEngine();
	~PhysicsEngine();

	void stepObjects(std::vector<Logic::GameObject*> gameObjects);
	void addGameObject(Logic::GameObject* o){ objectsToTest.push(o); };
private:
	std::stack<Logic::GameObject*> objectsToTest;
};

