#pragma once

#include "Node.h"
using namespace std;

#define _MAX_GRAPH_EDGES 200

/**
*  This class represents an object Edge that will represent an edge in a graph.
*  @see Graph
*
*/
namespace Logic{
	class Edge{

	private:
		static int nextid;
		Node *noi;
		Node *nof;
		float weight, width;
		int id;
		Object *object;

	public:
		Edge(Node *noi, Node *nof, float weight, float width);
		Edge();
		~Edge();
		void printEdge(Edge);


		Node* getNoi() const;
		Node* getNof() const;
		void setNoi(Node *no);
		void setNof(Node *no);
		float getWeight() const;
		float getWidth() const;
		void setWeight(float w);
		void setWidth(float w);
		int getId() const;
		Object *getObject()const;
	};
}



