#pragma once
#include "Scene.h"
#include "GameObject.h"
#include "glm\vec3.hpp"
#include <vector>
#include "Input.h"
#include "Graph.h"
#include "ModelComponent.h"
#include "ModelFactory.h"
/**
*  This class represents an object Bonus, child of GameObject.
*  @see GameObject
*/

using namespace std;

namespace Logic{
	class Bonus : public GameObject{
	private:
		int bonusID;
		int value;
		std::vector<Engine::Transform> transforms;
		float angle;
		Engine::ModelType model;
	public:
		Bonus();
		Bonus(Engine::Scene& cena, Engine::ModelType& model, glm::vec3 position, int value);
		~Bonus();
		int getBonusID()const{ return bonusID; };
		int getValue()const{ return value; };
		void setValue(int v){ value = v; };
		ModelType getType()const{ return model; };
		
		/**
		*  Update the Bonus.
		*
		*/


		void update(Input&, float delta);

		/**
		*  Check if a Bonus was catched by the Player.
		*  
		*/
		bool isColliding(GameObject*);
	};
}