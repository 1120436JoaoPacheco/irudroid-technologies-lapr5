#include "Projectil.h"


using namespace Logic;
using namespace Engine;




Projectil::~Projectil(){}
Projectil::Projectil(){

}

void Projectil::update(Input &i, float delta){
	vec3 pos = getPosition();
	 pos +=playerFront* SPEED ;
	
	 pos.y = YBALA;
	 float distanciapercorrida = sqrt((pos.x - posini.x)*(pos.x - posini.x)
									+ (pos.z - posini.y)*(pos.z - posini.y));
	 if (distanciapercorrida > ALCANCE){
		 destroy = true;
	 }
	 setPosition(pos);
	
	 Transform dTransform;
	 vector<Transform> transformations;

	 dTransform.scale = escala;
	 dTransform.rotation = rotation;
	 dTransform.position = getPosition();

	 transformations.push_back(dTransform);
	 
	 SetTransformMessage mx = SetTransformMessage(idProjectil, &transformations);
	 model->sendMessage(&mx);
	
}



void Projectil::drawBullet(Engine::Scene *sc, glm::vec3 pos, glm::vec3 pfront, float rot_y){
	rotation = Engine::Rotation(rot_y, Engine::Axis::Y_AXIS);
	pos += playerFront*5.0f;
	escala = glm::vec3(0.2f, 0.2f, 0.2f);

	this->setBox(PhysicsBox(pos, escala));

	posini.x = pos.x;
	posini.y = pos.z;

	playerFront = pfront;
	pos.y = YBALA;
	
	setPosition(pos);
 model = sc->addNewObject();
 Model * modelo;
 ModelComponent* renderC;
 ModelFactory* meshFac;
 Mesh * mesh = new Mesh(true);
 QuadPrimitive q;
	meshFac = ModelFactory::Get();
	modelo = meshFac->createModel(ModelType::BULLET);
	renderC = new ModelComponent(modelo);
	mesh->addPrimitive(q);
	idProjectil = model->getObjectId();
	model->addComponent(renderC);
	vector<Transform> transformations;
	Transform dTransform;
	dTransform.position = getPosition();
	dTransform.scale = escala;
	dTransform.rotation = rotation;

	transformations.push_back(dTransform);

	SetTransformMessage mx = SetTransformMessage(idProjectil, &transformations);
	model->sendMessage(&mx);


}
bool Projectil::getDestroy(){
	return destroy;
}
int Projectil::getidProjectil(){
	return idProjectil;
}

bool Projectil::isColliding(GameObject* gO){
	if (typeid(*gO) == typeid(Enemy) || typeid(*gO) == typeid(Wall) || typeid(*gO) == typeid(Door) || typeid(*gO) == typeid(Obstacle) || typeid(*gO) == typeid(Bonus))
	{
		destroy = true;
		//printf("Projectil against the enemy\n", typeid(*this), typeid(*gO));
		// set life points to 0
	}

	return true;
}