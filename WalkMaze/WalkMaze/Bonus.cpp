#include "Bonus.h"

using namespace Logic;
using namespace Engine;
Bonus::Bonus(){}

Bonus::Bonus(Engine::Scene &cena, Engine::ModelType& type, glm::vec3 pos, int v) :value(v), model(type){
	PhysicsBox box(pos,glm::vec3(0.1f,0.1f,0.1f));
	setBox(box);
	Object * object;

	switch (type){

	case ModelType::LIFEBONUS:
	{
		angle = 0;

		object = cena.addNewObject();

		ModelFactory* meshFac = ModelFactory::Get();
		Model * life = meshFac->createModel(type);
		ModelComponent* renderC = new ModelComponent(life);

		object->addComponent(renderC);

		Transform t;
		t.position = pos;
		t.scale = glm::vec3(0.05f, 0.05f, 0.05f);
		t.rotation = Rotation(-90, Axis::X_AXIS);
		transforms.push_back(t);

		object->sendMessage(&Engine::SetTransformMessage(0, &transforms));

		break;
	}
	case ModelType::POINTBONUS:
	{
		angle = 0;

		object = cena.addNewObject();
		ModelFactory* meshFac = ModelFactory::Get();
		Model * points = meshFac->createModel(Engine::ModelType::POINTBONUS);
		ModelComponent* renderC = new ModelComponent(points);
		object->addComponent(renderC);
		Transform t;
		t.position = pos;
		t.scale = glm::vec3(0.1f, 0.1f, 0.1f);
		t.rotation = Rotation(-90, Axis::X_AXIS);
		transforms.push_back(t);


		object->sendMessage(&Engine::SetTransformMessage(0, &transforms));

		break;
	}
	}
	setObject(object);
}
Bonus::~Bonus(){}

void Bonus::update(Input& i, float delta){
	angle++;

	Transform dTransform;
	vector<Transform> transformations;

	dTransform.scale = glm::vec3(0.025f, 0.025f, 0.025f);
	dTransform.rotation = Rotation(-90, Axis::X_AXIS);
	dTransform.position = this->getPosition();

	transformations.push_back(dTransform);

	dTransform.scale = glm::vec3(1, 1, 1);
	dTransform.rotation = Rotation(angle, Axis::Z_AXIS);
	dTransform.position = this->getPosition();

	transformations.push_back(dTransform);

	SetTransformMessage mx = SetTransformMessage(bonusID, &transformations);

	getObject()->sendMessage(&mx);
}

bool Bonus::isColliding(GameObject* gO){
	if (typeid(*gO) == typeid(Player))
	{
	//	printf("Bonus against the player\n", typeid(*this), typeid(*gO));
		setDelete(true);
	}
	return true;
}