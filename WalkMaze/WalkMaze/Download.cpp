#include "Download.h"

Download::Download(){}

Download::~Download(){}

void Download::downloadAll(){
	downTextures();
	downSounds();
	downEnemies();
	downObstacles();
	downPlayer();
}

void Download::downSounds(){

	string url0 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/musica1.wav";
	string url1 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/boring.wav";
	string url2 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/ho.wav";
	string url3 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/do.wav";

	string filestring0 = "musica1.wav";
	string filestring1 = "boring.wav";
	string filestring2 = "ho.wav";
	string filestring3 = "do.wav";


	HRESULT hr0 = URLDownloadToFile(NULL, url0.c_str(), filestring0.c_str(), 0, NULL);
	HRESULT hr1 = URLDownloadToFile(NULL, url1.c_str(), filestring1.c_str(), 0, NULL);
	HRESULT hr2 = URLDownloadToFile(NULL, url2.c_str(), filestring2.c_str(), 0, NULL);
	HRESULT hr3 = URLDownloadToFile(NULL, url3.c_str(), filestring3.c_str(), 0, NULL);

	if (hr0 == S_OK && hr1 == S_OK && hr2 == S_OK && hr3 == S_OK)
	{
		printf("Download de musicas com sucesso\n");
	}
	else
	{
		printf("Erro no download das musicas\n");
	}


}
void Download::downTextures(){


	string url0 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/wall.jpg";
	string url1 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/floor.jpg";
	string url2 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/sky.jpg";
	string url3 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/trap.png";
	string url4 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/sky.png";
	string url5 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/door.png";
	string url6 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/doorblack.png";
	string url7 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/trapblack.png";
	//nome ficheiro saida, grava na pasta do projeto

	string filestring0 = "wall.jpg";
	string filestring1 = "floor.jpg";
	string filestring2 = "sky.jpg";
	string filestring3 = "trap.png";
	string filestring4 = "sky.png";
	string filestring5 = "door.png";
	string filestring6 = "doorblack.png";
	string filestring7 = "trapblack.png";

	HRESULT hr0 = URLDownloadToFile(NULL, url0.c_str(), filestring0.c_str(), 0, NULL);
	HRESULT hr1 = URLDownloadToFile(NULL, url1.c_str(), filestring1.c_str(), 0, NULL);
	HRESULT hr2 = URLDownloadToFile(NULL, url2.c_str(), filestring2.c_str(), 0, NULL);
	HRESULT hr3 = URLDownloadToFile(NULL, url3.c_str(), filestring3.c_str(), 0, NULL);
	HRESULT hr4 = URLDownloadToFile(NULL, url4.c_str(), filestring4.c_str(), 0, NULL);
	HRESULT hr5 = URLDownloadToFile(NULL, url5.c_str(), filestring5.c_str(), 0, NULL);
	HRESULT hr6 = URLDownloadToFile(NULL, url6.c_str(), filestring6.c_str(), 0, NULL);
	HRESULT hr7 = URLDownloadToFile(NULL, url7.c_str(), filestring7.c_str(), 0, NULL);

	if (hr0 == S_OK && hr1 == S_OK && hr2 == S_OK && hr3== S_OK && hr4 == S_OK && hr5 == S_OK && hr6 == S_OK && hr7 == S_OK)
	{
		printf("Download de texturas com sucesso\n");
	}
	else
	{
		printf("Erro no download das texturas\n");
	}

}

void Download::downEnemies(){
	string url0 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/scream.mdl";
	string url1 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/raptor.mdl";
	//nome ficheiro saida, grava na pasta do projeto


		string filestring0 = "scream.mdl";
		string filestring1 = "raptor.mdl";

		HRESULT hr0 = URLDownloadToFile(NULL, url0.c_str(), filestring0.c_str(), 0, NULL);
		HRESULT hr1 = URLDownloadToFile(NULL, url1.c_str(), filestring1.c_str(), 0, NULL);
		if (hr0 == S_OK && hr1 == S_OK)
			printf("Download dos inimigos com sucesso\n");
		else
			printf("Erro no download dos inimigos\n");


}

void Download::downObstacles(){
	string url0 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/barrel.mdl";
	string url1 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/chair.mdl";
	string url2 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/crate.mdl";
	string url3 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/icecream.mdl";
	string url4 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/piano.mdl";
	string url5 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/plant.mdl";
	string url6 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/rshell.mdl";
	string url7 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/shelves_wood.mdl";
	string url8 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/star.mdl";
	string url9 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/statue2.mdl";
	string url10 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/wardrobe.mdl";
	string url11 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/wrecked_table.mdl";
	//nome ficheiro saida, grava na pasta do projeto


	string filestring0 = "barrel.mdl";
	string filestring1 = "chair.mdl";
	string filestring2 = "crate.mdl";
	string filestring3 = "icecream.mdl";
	string filestring4 = "piano.mdl";
	string filestring5 = "plant.mdl";
	string filestring6 = "rshell.mdl";
	string filestring7 = "shelves_wood.mdl";
	string filestring8 = "star.mdl";
	string filestring9 = "statue2.mdl";
	string filestring10 = "wardrobe.mdl";
	string filestring11 = "wrecked_table.mdl";

	HRESULT hr0 = URLDownloadToFile(NULL, url0.c_str(), filestring0.c_str(), 0, NULL);
	HRESULT hr1 = URLDownloadToFile(NULL, url1.c_str(), filestring1.c_str(), 0, NULL);
	HRESULT hr2 = URLDownloadToFile(NULL, url2.c_str(), filestring2.c_str(), 0, NULL);
	HRESULT hr3 = URLDownloadToFile(NULL, url3.c_str(), filestring3.c_str(), 0, NULL);
	HRESULT hr4 = URLDownloadToFile(NULL, url4.c_str(), filestring4.c_str(), 0, NULL);
	HRESULT hr5 = URLDownloadToFile(NULL, url5.c_str(), filestring5.c_str(), 0, NULL);
	HRESULT hr6 = URLDownloadToFile(NULL, url6.c_str(), filestring6.c_str(), 0, NULL);
	HRESULT hr7 = URLDownloadToFile(NULL, url7.c_str(), filestring7.c_str(), 0, NULL);
	HRESULT hr8 = URLDownloadToFile(NULL, url8.c_str(), filestring8.c_str(), 0, NULL);
	HRESULT hr9 = URLDownloadToFile(NULL, url9.c_str(), filestring9.c_str(), 0, NULL);
	HRESULT hr10 = URLDownloadToFile(NULL, url10.c_str(), filestring10.c_str(), 0, NULL);
	HRESULT hr11 = URLDownloadToFile(NULL, url11.c_str(), filestring11.c_str(), 0, NULL);

	if (hr0 == S_OK && hr1 == S_OK && hr2 == S_OK && hr3 == S_OK && hr4 == S_OK && hr5 == S_OK && hr6 == S_OK && hr7 == S_OK && hr8 == S_OK && hr9 == S_OK && hr10 == S_OK && hr11 == S_OK)
		printf("Download dos obstaculos com sucesso\n");
	else
		printf("Erro no download dos obstaculos\n");


}

void Download::downPlayer(){
	string url0 = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/recursos/daffy.mdl";
	//nome ficheiro saida, grava na pasta do projeto


	string filestring0 = "daffy.mdl";

	HRESULT hr0 = URLDownloadToFile(NULL, url0.c_str(), filestring0.c_str(), 0, NULL);
	if (hr0 == S_OK)
		printf("Download do jogador com sucesso\n");
	else
		printf("Erro no download do jogador\n");


}