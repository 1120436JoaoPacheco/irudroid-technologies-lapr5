#pragma once
#include "GL\glew.h"
#include "Player.h"
#include "Map.h"
#include "Services.h"

using namespace std;

/**
*  This class represents an object Game.
*  A Game is constitued by a Player and a Map.
*  There is a GameStatus that represents the current state of a Game.
*  @see Player
*  @see Map
*/
namespace Logic{

	enum class GameStatus
	{
		LoginMenu, WorldsMenu, Playing, GameOver, Replay
	};

	class Game{

	private:
		Player* player;
		GameStatus status;
		Map* map;
		Services service;
		pugi::xml_document* root;
	public:
		

		Game(){ status = GameStatus::LoginMenu; };
		Game(pugi::xml_document &root);

		void update(Input& w, float delta);

		~Game();

		Player* getPlayer() const;
		Map* getMap() const;
		GameStatus getGameStatus() const { return status; };

		void setPlayer(Player*);
		void setMap(Map*);
		void setGameStatus(GameStatus gs) {	status = gs; };


		void reset(Player*);

		void renderGame(int width, int height);

	};
}