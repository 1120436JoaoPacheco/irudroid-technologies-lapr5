#include "Door.h"

using namespace Logic;
using namespace Engine;

Door::Door(int id,int nextR,int nextD, DoorType d) : id(id),nextRoom(nextR),nextDoor(nextD), type(d) {
	black = false;
}
Door::Door(int id,  DoorType d) : id(id),  type(d){
	black = false;
}
Door::Door(){}
Door::~Door(){}


int Door::getNextRoom() const{
	return nextRoom;
}
int Door::getNextDoor() const{
	return nextDoor;
}
int Door::getId() const{
	return id;
}
DoorType Door::getDoorType() const{
	return type;
}


void Door::setNextRoom(int n){
	nextRoom = n;
}

void Door::setDoorType(DoorType dt){
	type = dt;
}

void Door::drawDoor( Engine::Scene *sc){
	 obj = sc->addNewObject();
	
	vector<Transform> transformations;
	Transform dTransform;
	dTransform.position = this->getPosition();
	dTransform.scale = glm::vec3(2.1, 5, 2.1);
	dTransform.rotation = Rotation();
	transformations.push_back(dTransform);
	
	Texture textdoor = TextureFactory::GetInstance()->createTexture("door.png");
	SetTransformMessage mx = SetTransformMessage(obj->getObjectId(), &transformations);
	obj->sendMessage(&mx);
	
	 mexer = CubeMeshFactory::GetInstance()->createMesh();
	
	
	mexer->setTexture(textdoor);
	compo = new RenderComponent(mexer);
	obj->addComponent(compo);
	
}

void Door::update(Input& i, float delta){

}
void Door::changeblack(){
	if (black == false){
		black = true;
		Texture textdoor = TextureFactory::GetInstance()->createTexture("doorblack.png");
		mexer = CubeMeshFactory::GetInstance()->createMesh();
		mexer->setTexture(textdoor);
		compo = new RenderComponent(mexer);
		obj->addComponent(compo);
	}
	
	
}
void Door::changedoor(){
	if (black == true){
		black = false;
		Texture textdoor = TextureFactory::GetInstance()->createTexture("door.png");
		mexer = CubeMeshFactory::GetInstance()->createMesh();
		mexer->setTexture(textdoor);
		compo = new RenderComponent(mexer);
		obj->addComponent(compo);
	}


}