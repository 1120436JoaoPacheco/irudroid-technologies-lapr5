#pragma once
#include "Scene.h"
#include "GameObject.h"
#include "glm\vec3.hpp"
#include "Scene.h"
#include "Graph.h"
#include "Input.h"
#include "ModelComponent.h"
#include "ModelFactory.h"
#include "GameObject.h"
#include "Enemy.h"
#include "Bonus.h"

#define ALCANCE 20
#define SPEED 0.55f
#define YBALA 1.5
using namespace std;

/**
*  This class represents an object Projectil, child of GameObject.
*  @see Character
*/
namespace Logic{
	class Projectil : public GameObject {

	private:
		int idProjectil;
		glm::vec3 escala;
		glm::vec3 direction;
		float speed;
		Rotation rotation;
		Engine::Object* model;
		std::vector<Engine::Transform> transforms; 
		bool destroy = false;
		vec2 posini;
		vec3 playerFront;
	public:
		Projectil();
		~Projectil();
		int getidProjectil() ;
		bool getDestroy();
		void setSpeed(float V){ speed = V; };
		float getSpeed()const{ return speed; };
		
		void drawBullet(Engine::Scene *s, glm::vec3 pos,glm::vec3 pfront, float);
		

		void update(Input&, float delta);

		bool isColliding(GameObject* gO);

	};









}