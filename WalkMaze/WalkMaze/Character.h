#pragma once
#include "glm\vec3.hpp"
#include "GameObject.h"

using namespace std;
/**
*  This class represents an object Character child of GameObject.
* @see GameObject
*/

namespace Logic{
	class Character : public GameObject{

	private:
		float speed;
		int lifePoints;
		glm::vec3 direction;

	public:
		Character() {};
		Character(float, int, glm::vec3);
		~Character() {};

		float getSpeed() const;
		glm::vec3 getDirection() const;
		int getLifePoints() const;
		void setSpeed(float);
		void setDirection(glm::vec3);
		void setLifePoints(int);
		void update(Input&, float delta);
	};
}