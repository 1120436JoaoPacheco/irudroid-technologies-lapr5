#include "Input.h"

using namespace Logic;

Input::Input(const Input& in){
	for (int i = 0; i < NUMBER_OF_KEYS; i++)
	{
		keysArray[i] = in.keysArray[i];
	}

	mouseDelta = in.mouseDelta;
	mousePos = in.mousePos;
}

void Input::update(GLFWwindow* window, bool center, float delta){
	
	updateKeyboard(window, delta);
	updateMouse(window, center, delta);
}

std::string Input::key_to_String(std::vector<Input> teclas){
	std::string ret = "";
	for (int i = 0; i < teclas.size(); i++)
	{
		if (teclas[i].getKeyPressed(Key::UpPlayer)){
			ret += "UP,";
		}
		if (teclas[i].getKeyPressed(Key::DownPlayer)){
			ret += "DOWN,";
		}
		if (teclas[i].getKeyPressed(Key::LeftPlayer)){
			ret += "LEFT,";
		}
		if (teclas[i].getKeyPressed(Key::RightPlayer)){
			ret += "RIGHT,";
		}
	}

	ret = ret.substr(0, ret.size()-1);

	return ret;
}

void Input::updateKeyboard(GLFWwindow*& window, float delta)
{
	// KeyBoard
	if (glfwGetKey(window, UP_CAMERA) == GLFW_PRESS)
		keysArray[(int)Key::UpCamera] = true;
	else
		keysArray[(int)Key::UpCamera] = false;

	if (glfwGetKey(window, DOWN_CAMERA) == GLFW_PRESS)
		keysArray[(int)Key::DownCamera] = true;
	else
		keysArray[(int)Key::DownCamera] = false;

	if (glfwGetKey(window, LEFT_CAMERA) == GLFW_PRESS)
		keysArray[(int)Key::LeftCamera] = true;
	else
		keysArray[(int)Key::LeftCamera] = false;

	if (glfwGetKey(window, RIGHT_CAMERA) == GLFW_PRESS)
		keysArray[(int)Key::RightCamera] = true;
	else
		keysArray[(int)Key::RightCamera] = false;

	// Player
	if (glfwGetKey(window, UP_PLAYER) == GLFW_PRESS)
		keysArray[(int)Key::UpPlayer] = true;
	else
		keysArray[(int)Key::UpPlayer] = false;

	if (glfwGetKey(window, DOWN_PLAYER) == GLFW_PRESS)
		keysArray[(int)Key::DownPlayer] = true;
	else
		keysArray[(int)Key::DownPlayer] = false;

	if (glfwGetKey(window, LEFT_PLAYER) == GLFW_PRESS)
		keysArray[(int)Key::LeftPlayer] = true;
	else
		keysArray[(int)Key::LeftPlayer] = false;

	if (glfwGetKey(window, RIGHT_PLAYER) == GLFW_PRESS)
		keysArray[(int)Key::RightPlayer] = true;
	else
		keysArray[(int)Key::RightPlayer] = false;

	if (glfwGetKey(window, MAP_1) == GLFW_PRESS)
		keysArray[(int)Key::Map1] = true;
	else
		keysArray[(int)Key::Map1] = false;

	if (glfwGetKey(window, MAP_2) == GLFW_PRESS)
		keysArray[(int)Key::Map2] = true;
	else
		keysArray[(int)Key::Map2] = false;

	if (glfwGetKey(window, MAP_3) == GLFW_PRESS)
		keysArray[(int)Key::Map3] = true;
	else
		keysArray[(int)Key::Map3] = false;

	if (glfwGetKey(window, FirstPS) == GLFW_PRESS)
		keysArray[(int)Key::Fps] = true;
	else
		keysArray[(int)Key::Fps] = false;

	if (glfwGetKey(window, ThirdPS) == GLFW_PRESS)
		keysArray[(int)Key::Tps] = true;
	else
		keysArray[(int)Key::Tps] = false;

	if (glfwGetKey(window, TopVS) == GLFW_PRESS)
		keysArray[(int)Key::Tvs] = true;
	else
		keysArray[(int)Key::Tvs] = false;

	if (glfwGetKey(window, DAYTIME) == GLFW_PRESS)
		keysArray[(int)Key::DayTime] = true;
	else
		keysArray[(int)Key::DayTime] = false;

	if (glfwGetKey(window, EXECUTE_PATH) == GLFW_PRESS)
		keysArray[(int)Key::ExecutePath] = true;
	else
		keysArray[(int)Key::ExecutePath] = false;

	if (glfwGetKey(window, SHOOT) == GLFW_PRESS)
		keysArray[(int)Key::Shoot] = true;
	else
		keysArray[(int)Key::Shoot] = false;

	// Mouse
	if (glfwGetMouseButton(window, MOUSE_CLICK) == GLFW_PRESS)
		keysArray[(int)Key::MouseClick] = true;
	else
		keysArray[(int)Key::MouseClick] = false;
}
void Input::updateMouse(GLFWwindow*& window, bool center, float delta)
{
	int width, height;
	glfwGetWindowSize(window, &width, &height);

	double centerx, centery, x, y;
	centerx = (double)width / 2;
	centery = (double)height / 2;

	glfwGetCursorPos(window, &x, &y);

	this->mousePos.x = (float)x;
	this->mousePos.y = (float)y;

	this->mouseDelta.x = (centerx - this->mousePos.x) * 0.05f;
	this->mouseDelta.y = (centery - this->mousePos.y) * 0.05f;

	if (center)
		glfwSetCursorPos(window, centerx, centery);
}