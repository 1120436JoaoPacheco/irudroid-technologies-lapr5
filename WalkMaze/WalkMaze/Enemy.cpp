#include "Enemy.h"

using namespace Logic;
using namespace Engine;


Enemy::Enemy(Scene& cena, ModelType &type, glm::vec3 pos) : modelType(type), stop(true)
{

	Object * model;
	switch (type){

	case ModelType::ENEMY_ALIEN:
	{
								   model = cena.addNewObject();
								   ModelFactory* meshFac = ModelFactory::Get();
								   Model * alien = meshFac->createModel(Engine::ModelType::ENEMY_ALIEN);
								   ModelComponent* renderC = new ModelComponent(alien);
								   model->addComponent(renderC);
								   setPosition(pos);
								   Transform t;
								   t.position = pos;
								   t.position.y += 0.7f;
								   t.scale = glm::vec3(0.04f, 0.04f, 0.04f);
								   t.rotation = Rotation(-90, Axis::X_AXIS);
								   transforms.push_back(t);

								   model->sendMessage(&Engine::SetTransformMessage(0, &transforms));

								   LifeBB = new Billboard(glm::vec3(t.position.x, t.position.y + 2, t.position.z), glm::vec3(1, 1, 1), glm::vec3(0, 1, 0));

								   Object* objbb = cena.addNewObject();
								   objbb->addComponent(LifeBB);

								   setLifePoints(100);

								   LifeBB->sendMessage(&Engine::TranformBillboardMessage(0, glm::vec3(1, 1, 1), glm::vec3(0, 1, 0)));
								   setBox(PhysicsBox(t.position, glm::vec3(1, 2, 1)));

								   break;
	}
	case ModelType::ENEMY_RAPTOR:
	{
									model = cena.addNewObject();
									ModelFactory* meshFac = ModelFactory::Get();
									Model * raptor = meshFac->createModel(Engine::ModelType::ENEMY_RAPTOR);
									ModelComponent* renderC = new ModelComponent(raptor);
									model->addComponent(renderC);
									Transform t;
									t.position = pos;
									t.position.y += 1.1;
									t.scale = glm::vec3(0.04f, 0.04f, 0.04f);
									t.rotation = Rotation(-90, Axis::X_AXIS);
									transforms.push_back(t);

									model->sendMessage(&Engine::SetTransformMessage(0, &transforms));

									LifeBB = new Billboard(glm::vec3(t.position.x, t.position.y + 3, t.position.z), glm::vec3(1, 1, 1), glm::vec3(0, 1, 0));

									Object* objbb = cena.addNewObject();
									objbb->addComponent(LifeBB);

									setLifePoints(100);

									LifeBB->sendMessage(&Engine::TranformBillboardMessage(0, glm::vec3(1, 1, 1), glm::vec3(0, 1, 0)));
									setBox(PhysicsBox(t.position, glm::vec3(1, 2, 1)));

									break;
	}


	}
	setObject(model);

}

Enemy::~Enemy(){}




void Enemy::update(Input &i, float delta){
	int life = getLifePoints();
	float value = (float)life / 100;


	if (life>50){
		LifeBB->sendMessage(&Engine::TranformBillboardMessage(0, glm::vec3(value, 1, 1), glm::vec3(0, 1, 0)));
	}
	else if (life>25){
		LifeBB->sendMessage(&Engine::TranformBillboardMessage(0, glm::vec3(value, 1, 1), glm::vec3(1, 0.8f, 0)));
	}
	else{
		LifeBB->sendMessage(&Engine::TranformBillboardMessage(0, glm::vec3(value, 1, 1), glm::vec3(1, 0, 0)));
	}


	if (stop){
		this->getObject()->sendMessage(&SequenceMessage(0, 0));
		if (getLifePoints()<=0){
			setDelete(true);
		}
	}
	else{
		this->delta -= delta;
		if (this->delta <= 0){
			stop = true;
		}
	}

}

bool Enemy::isColliding(GameObject* gO){
	if (typeid(*gO) == typeid(Player))
	{
		if (modelType == ModelType::ENEMY_RAPTOR){
			stop = false;
			delta = 0.33f;
			this->getObject()->sendMessage(&SequenceMessage(0, 4));
		}
		else if (modelType == ModelType::ENEMY_ALIEN){
			stop = false;
			delta = 0.24f;
			this->getObject()->sendMessage(&SequenceMessage(0, 26));
		}
	}
	if (typeid(*gO) == typeid(Projectil))
	{
		setLifePoints(getLifePoints() - 10);
		if (getLifePoints() <= 0){
			if (modelType == ModelType::ENEMY_RAPTOR){
				stop = false;
				delta = 0.86f;
				this->getObject()->sendMessage(&SequenceMessage(0, 8));
			}
			else if (modelType == ModelType::ENEMY_ALIEN){
				stop = false;
				delta = 0.48f;
				this->getObject()->sendMessage(&SequenceMessage(0,19));
			}
		}
	}
	return true;
}