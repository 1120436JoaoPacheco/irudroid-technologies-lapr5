#pragma once

#include "glm\vec2.hpp"
#include "glm\vec3.hpp"
#include <stdlib.h>
#include <string>
#include <gl\glew.h>
#include <FTGL\ftgl.h>
#include <GLFW\glfw3.h>
#include "GL\glew.h"
#include "Input.h"

void invoke(GLFWwindow*, int, int, int, int);

namespace Logic
{
	/**
	*	The menu class
	*	@see LoginMenu
	*/
	class Menu
	{
	protected:
		GLFWwindow* window;

		typedef struct {
			float x0, y0, x1, y1;
			unsigned int size;
			std::string str;
		} MenuStruct;

		float ortho_right, ortho_bottom;
	public:
		Menu() { ortho_right = 10, ortho_bottom = 10; };
		~Menu() {};

		void start(GLFWwindow*);
		void createText(int, std::string, glm::vec3, glm::vec2);
		void createBox(glm::vec2, glm::vec2, glm::vec3);
		void drawViewport();
		void end();
		void getMouseCoordinates(Input&, float&, float&);
	};
}