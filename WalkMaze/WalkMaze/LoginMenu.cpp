#include "LoginMenu.h"

using namespace Logic;

extern std::string inputText;

bool LoginMenu::process(Input& input)
{
	initUp();
	drawAll();
	startInputs(input);
	return validateLogin(input);
}

void LoginMenu::initUp()
{
	float incrY = 2;

	login.x0 = 3;
	login.y0 = 3.5;
	login.x1 = 9.5;
	login.y1 = 4.5;
	login.str = "Login: ";
	login.size = 42;

	password.x0 = login.x0;
	password.y0 = login.y0 + incrY;
	password.x1 = login.x1;
	password.y1 = login.y1 + incrY;
	password.str = "Pass: ";
	password.size = login.size;

	button.x0 = 5;
	button.y0 = 7;
	button.x1 = 6;
	button.y1 = 8;
	button.str = "OK";
	button.size = login.size;
}

void LoginMenu::drawAll()
{
	drawViewport();

	glm::vec3 boxColor(0.5f, 0.5f, 0.5f);

	createBox(glm::vec2(login.x0, login.y0), glm::vec2(login.x1, login.y1), boxColor);
	createBox(glm::vec2(password.x0, password.y0), glm::vec2(password.x1, password.y1), boxColor);
	createBox(glm::vec2(button.x0, button.y0), glm::vec2(button.x1, button.y1), boxColor);

	glm::vec3 textColor(0, 0, 1);
	createText(login.size * 2, "Walk the Maze", textColor, glm::vec2(1, 2));
	createText(login.size, login.str, textColor, glm::vec2(login.x0*0.25, login.y1 - 0.25));
	createText(password.size, password.str, textColor, glm::vec2(password.x0*0.25, password.y1 - 0.25));
	createText(button.size, button.str, textColor, glm::vec2(button.x0, button.y1 - 0.2));
}

void LoginMenu::inputLogin(Input& input, float mouseX, float mouseY)
{

	if (input.getKeyPressed(Key::MouseClick))
	{
		if (mouseX > login.x0 && mouseX < login.x1)
		{
			if (mouseY > login.y0  && mouseY < login.y1)
			{
				loginActive = true;
				passwordActive = false;
				inputText = loginStr;
			}
		}
	}

	if (loginActive){
		if (loginStr.length() > 15)
			inputText = inputText.substr(0, inputText.length() - 1);

		loginStr = inputText;
	}

	createText(login.size, loginStr, glm::vec3(0, 0, 1), glm::vec2(login.x0, login.y1 - 0.2));
}

void LoginMenu::inputPassword(Input& input, float mouseX, float mouseY)
{
	string temp = "";
	if (input.getKeyPressed(Key::MouseClick))
	{
		if (mouseX > password.x0 && mouseX < password.x1)
		{
			if (mouseY > password.y0 && mouseY < password.y1)
			{
				passwordActive = true;
				loginActive = false;
				inputText = passwordStr;
			}
		}
	}
	
	if (passwordActive) {
		if (passwordStr.length() > 15)
			inputText = inputText.substr(0, inputText.length() - 1);

		passwordStr = inputText;
	}

	for (int i = 0; i < passwordStr.size(); i++) { temp += "*"; }
	createText(password.size, temp, glm::vec3(0, 0, 1), glm::vec2(password.x0, password.y1));

}

void LoginMenu::startInputs(Input& input)
{
	float mouseX, mouseY;
	Menu::getMouseCoordinates(input, mouseX, mouseY);

	inputLogin(input, mouseX, mouseY);
	inputPassword(input, mouseX, mouseY);
}

bool LoginMenu::validateLogin(Input& input)
{
	float mouseX, mouseY;
	getMouseCoordinates(input, mouseX, mouseY);

	string text = "", errorStr = "Username ou Password Errado!";
	if (error)
		text = errorStr;

	createText(button.size / 2, text, glm::vec3(1, 0, 0), glm::vec2(button.x0 - 2, button.y1 + 1));

	if (input.getKeyPressed(Key::MouseClick) && mouseX > button.x0 && mouseY > button.y0 && mouseX < button.x1 && mouseY < button.y1)
	{
		Services s;
		bool valido = s.login(loginStr, passwordStr);
		error = !valido;
		return valido;
	}
	else {
		return false;
	}
}