#pragma once
#include "Game.h"
#include "Door.h"
#include "Input.h"
#include "Menu.h"
#include "Services.h"
#define BLACK		5
#define PORTA		1.8

using namespace Logic;
/**
*  Class that checks if the player changed the actual room.
*  @see Room
*/
class ChangeRoom{

private:
	float delta;
	bool gameOver;
public:
	ChangeRoom() : delta(0), gameOver(false){

	}
	~ChangeRoom(){

	}
	/**
	*   Checks if the player changed the actual room.
	*  @see Game
	*  @see Door
	*/

	bool check(Game* game, vector<Door*>* listaportas, Input* input, float delta){
		Menu m;
		Services s;
		if (gameOver)
		{
			if (this->delta > 5)
				exit(1);
			else
			{
				this->delta += delta;
			}
		}
		for (Door *door : *listaportas)
		{
			//so verifica se a porta for de conexao

			//guarda o x e y da porta q esta a verificar
			int xp = door->getPosition().x;
			int zp = door->getPosition().z;
			//guarda posicao do jogador
			float x = game->getMap()->getActiveRoom()->getPlayer()->getPosition().x;
			float z = game->getMap()->getActiveRoom()->getPlayer()->getPosition().z;
			if (x < xp + BLACK && x > xp - BLACK && z < zp + BLACK && z > zp - BLACK){
				//PORTA FICA A PRETO
				door->changeblack();

				//caso esteja na porta muda de quarto
				if (x < xp + PORTA && x > xp - PORTA && z < zp + PORTA && z > zp - PORTA){
					if (door->getDoorType() == DoorType::Connection){
						//vai buscar o id da porta para a qual vai mudar
						int proximaporta = door->getNextDoor();
						//vai buscar o id do quarto para o qual vai mudar
						int proximoquarto = door->getNextRoom();

						//muda o activeRoom

						game->getMap()->setActiveRoom(proximoquarto);
						//vai buscar as portas do novo activeRoom
						vector<Door*> portas_atuais = game->getMap()->getActiveRoom()->getDoorlist();
						for (int k = 0; k < portas_atuais.size(); k++){
							//se o id da porta for igual ao id da porta para o qual � suposto mudar, muda
							if (portas_atuais[k]->getId() == proximaporta){
								float xplayer, zplayer;

								//ifs para q o jogador fica � frente da porta para a qual mudou
								if (portas_atuais[k]->getPosition().x == 100){
									xplayer = portas_atuais[k]->getPosition().x - 10;
								}
								else{
									if (portas_atuais[k]->getPosition().x == 0){
										xplayer = portas_atuais[k]->getPosition().x + 10;
									}
									else{
										xplayer = portas_atuais[k]->getPosition().x;
									}

								}
								if (portas_atuais[k]->getPosition().z == 0){
									zplayer = portas_atuais[k]->getPosition().z + 10;
								}
								else{
									if (portas_atuais[k]->getPosition().z == 100){
										zplayer = portas_atuais[k]->getPosition().z - 10;
									}
									else{
										zplayer = portas_atuais[k]->getPosition().z;
									}

								}

								//muda posicao do jogador
								vec3 olo(xplayer, 0.6f, zplayer);
								game->getMap()->getActiveRoom()->getPlayer()->setPosition(olo);
								game->getMap()->getActiveRoom()->getPlayer()->getBox()->setPos();
								break;
							}
						}

						//a lista de portas que se vai verificar passa a ser a do novo activeRoom
						*listaportas = portas_atuais;

						game->update(*input, delta);
						return true;
					}
					else{
						gameOver = true;
				
						delta = 0;
						float pontos_float = game->getMap()->getActiveRoom()->getPlayer()->getScore();
						int pontos = pontos_float;
						s.score("admin","mapa2",pontos);
						string pontos_string = to_string(pontos);
						glViewport(0, 600, 10, 10);
						m.createText(30, "CONGRATS", glm::vec3(1.0f, 1.0f, 1.0f), glm::vec2(1, 2));
						glViewport(0, 500, 10, 10);

						m.createText(30, "SCORE: "+pontos_string, glm::vec3(1.0f, 1.0f, 1.0f), glm::vec2(3, 4));
						glViewport(0, 0, 0, 0);
						//exit(1); 
					}
				}
			}
			else{
				//PORTA VOLTA AO NORMAL
				door->changedoor();
			}
		}
		return false;
	}


};