#pragma once
#include "glm\vec3.hpp"
#include <vector>
#include "Edge.h"
#include "Model.h"
#include "ModelFactory.h"
#include "ModelComponent.h"
#include "Character.h"
#include "Billboard.h"
#include "Projectil.h"

/**
*  This class represents an object Enemy, child of Character.
*  @see Character
*/
namespace Logic{
	class Enemy : public Character {
	private:
		bool stop;
		float delta;
		Billboard* LifeBB;
		Engine::ModelType modelType;

		std::vector<Engine::Transform> transforms; 
	public:

		Enemy(Engine::Scene&cena, Engine::ModelType& modeltype, glm::vec3 pos);
		~Enemy();

		void update(Input&, float delta);




		virtual void setMeshRotation(float rot_y) {
			GameObject::setMeshRotation(Engine::Rotation(rot_y, Engine::Axis::Z_AXIS));
			transforms[1].rotation = Engine::Rotation(rot_y, Engine::Axis::Z_AXIS);

		}
		/**
		*  Check if a enemy was hited by the Player and/or by a Projectile.
		*  @see Player
		*  @see Projectile
		*/
		bool isColliding(GameObject*);
	};


}