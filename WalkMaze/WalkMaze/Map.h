#pragma once
#include <iostream>
#include <vector>
#include "pugixml.hpp"
#include "Room.h"
using namespace std;

	/**
	*  This class represents an object Map that contains a list of rooms.
	*
	*  @see Room
	*
	*/
namespace Logic{
	class Map{

	private:
		vector<Room*> rooms;
		Room* activeRoom;
		pugi::xml_document *node;
	public:
		Map() {};
		Map(pugi::xml_document &node);
		~Map();

		vector<Room*> getRooms() const;
		Room* getActiveRoom() const;

		void setActiveRoom(int);
		/**
		*  Update the room
		*
		*/
		void updateRoom(Input&, float delta);

	};
}