#include "PhysicsEngine.h"


PhysicsEngine::PhysicsEngine()
{
}


PhysicsEngine::~PhysicsEngine()
{
}
//MANDAR DELTA PARA O ISCOLLIDING
void PhysicsEngine::stepObjects(std::vector<Logic::GameObject*> gameObjects)
{
	bool result =  false;
	Logic::GameObject * object;
	while (!objectsToTest.empty())
	{
		object = objectsToTest.top();
		PhysicsBox* boxToTest = object->getBox();
		objectsToTest.pop();
		result = false;
		for (Logic::GameObject* ob : gameObjects)
		{
			if (ob != object && boxToTest->testCollision(*ob->getBox()))
			{
				ob->isColliding(object);
				result |= object->isColliding(ob);
			}

		}

		if (!result)
			if (object->getBox()->isDirty())
				object->getBox()->setPos();
	}

}
