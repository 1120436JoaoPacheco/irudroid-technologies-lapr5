#pragma once
#include <AL/alut.h>
#include <string>
using namespace std;

/**
*  Class that is responsable to reproduce the sounds during the game.
*/
class Music{
	typedef struct {
		ALuint buffer[4], source[2];
		ALboolean tecla_s;
	} Estado;
private:
	Estado estado;
public:
	Music();
	~Music();
	void start(int argc, char* argv[],int num);
	void porta();
	void colide();
	void parado();
	void mudasom(int id);
};