#include "PhysicsBox.h"

PhysicsBox::PhysicsBox(glm::vec3 pos, glm::vec3 scale) :
_pos(pos), _scale(scale), _rotation(0, 0, 0), angle(0), dirty(false)
{
	box.center[0] = pos.x;
	box.center[1] = pos.y;
	box.center[2] = pos.z;

	box.extent[0] = scale.x;
	box.extent[1] = scale.y;
	box.extent[2] = scale.z;
}

PhysicsBox::PhysicsBox(glm::vec3 scale) :
_pos(0, 0, 0), _scale(scale), _rotation(0, 0, 0), angle(0), dirty(false)
{
	box.center[0] = _pos.x;
	box.center[1] = _pos.y;
	box.center[2] = _pos.z;

	box.extent[0] = scale.x;
	box.extent[1] = scale.y;
	box.extent[2] = scale.z;
}

PhysicsBox::PhysicsBox() :
_pos(0, 0, 0), _scale(1, 1, 1), _rotation(0, 0, 0), angle(0)
{
	box.center[0] = _pos.x;
	box.center[1] = _pos.y;
	box.center[2] = _pos.z;

	box.extent[0] = _scale.x;
	box.extent[1] = _scale.y;
	box.extent[2] = _scale.z;
}

PhysicsBox::PhysicsBox(const PhysicsBox& o)
{
	box = o.box;
	_pos = o._pos;
	_scale = o._scale;
	_rotation = o._rotation;
	angle = o.angle;
}
void PhysicsBox::updatePos(glm::vec3 pos) {
	_tempPos = pos;

	box.center[0] = _tempPos.x;
	box.center[1] = _tempPos.y;
	box.center[2] = _tempPos.z;

	dirty = true;
}

void PhysicsBox::setPos()
{
	if (dirty)
	{
		_pos = _tempPos;
		box.center[0] = _pos.x;
		box.center[1] = _pos.y;
		box.center[2] = _pos.z;
		dirty = false;

	}
}

void  PhysicsBox::rotate(float angle)
{
	glm::vec3 currXAxis(1, 0, 0);
	glm::vec3 currZAxis(0, 0, 1);

	glm::vec3 rotatedXAxis = glm::normalize(glm::rotateY(currXAxis, glm::radians(angle)));
	glm::vec3 rotatedZAxis = glm::normalize(glm::rotateY(currZAxis, glm::radians(angle)));

	box.axis[0][0] = rotatedXAxis.x;
	box.axis[0][1] = rotatedXAxis.y;
	box.axis[0][2] = rotatedXAxis.z;

	box.axis[2][0] = rotatedZAxis.x;
	box.axis[2][1] = rotatedZAxis.y;
	box.axis[2][2] = rotatedZAxis.z;
	this->angle = angle;
}

bool PhysicsBox::testCollision(const PhysicsBox& a) const
{

	gte::TIQuery<float, gte::OrientedBox3<float>, gte::OrientedBox3<float>> c;
	gte::TIQuery<float, gte::OrientedBox3<float>, gte::OrientedBox3<float>>::Result r;

	r = c(box, a.box);

	return r.intersect && glm::abs(glm::dot((float)r.separating[0], (float)r.separating[1])) >= 1 - r.epsilon;
}
