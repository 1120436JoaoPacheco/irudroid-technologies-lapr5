#include "Player.h"
#include "CubeMeshFactory.h"
#include "RenderComponent.h"
#include "Menu.h"
#include "Services.h"
using namespace Logic;
using namespace Engine;

Menu m;
Services s;

Player::Player(int s, glm::vec3 pos, Engine::Scene & cena) : score(s), c(cena.getCameraObject()), delta(0), canLoseLife(true), gameOver(false)
{
	teste = cena.addNewObject();
	c.setTarget(this);
	model = cena.addNewObject();
	ModelFactory* meshFac = ModelFactory::Get();
	Model * homer = meshFac->createModel(Engine::ModelType::PLAYER);
	ModelComponent* renderC = new ModelComponent(homer);
	Mesh * mesh = new Mesh(false);
	QuadPrimitive q;
	mesh->addPrimitive(q);
	MiniMapRenderComponent* renderM = new MiniMapRenderComponent(mesh, glm::vec3(1, 0, 0), glm::vec3(2, 2, 2), 0);

	LifeBar = new LifeBarRenderComponent(glm::vec3(1, 1, 1), glm::vec3(0, 1, 0));

	model->addComponent(renderM);
	model->addComponent(renderC);

	model->addComponent(LifeBar);

	Transform t;
	t.position = glm::vec3(0, 0, 0);
	t.scale = glm::vec3(0.03f, 0.03f, 0.03f);
	t.rotation = Rotation(-90, Axis::X_AXIS);
	transforms.push_back(t);

	t.scale = glm::vec3(1.0f, 1.0f, 1.0f);
	t.rotation = Rotation();
	transforms.push_back(t);

	glm::vec3 scale(0.6f, 1, 0.6f);
	PhysicsBox box(pos, scale);
	setBox(box);
	setObject(model);

	Mesh * mesh1 = CubeMeshFactory::GetInstance()->getMesh();
	Transform tra;
	tra.position = getBox()->getPos();
	tra.scale = glm::vec3(getBox()->getScale().x * 2, getBox()->getScale().y * 2, getBox()->getScale().z * 2);
	tra.rotation = Engine::Rotation(getBox()->getRotation(), Engine::Axis::Y_AXIS);

	v.push_back(tra);
	setLifePoints(100);
}

Player::~Player(){}

void Player::setScore(int n) {
	score = n;
}

int Player::getScore() const {
	return score;
}

Engine::CameraType Player::getCameraType(){
	return c.getType();
}


void Player::update(Input &i, float delta){

	if (gameOver)
	{
		if (this->delta > 5)
			exit(1);
		else
		{
			this->delta += delta;
		}
	}
	int life = getLifePoints();
	float value = (float)life / 100;


	if (life > 50){
		LifeBar->sendMessage(&Engine::TranformLifeBarMessage(0, glm::vec3(value, 1, 1), glm::vec3(0, 1, 0)));
	}
	else if (life > 25){
		LifeBar->sendMessage(&Engine::TranformLifeBarMessage(0, glm::vec3(value, 1, 1), glm::vec3(1, 0.8f, 0)));
	}
	else{
		LifeBar->sendMessage(&Engine::TranformLifeBarMessage(0, glm::vec3(value, 1, 1), glm::vec3(1, 0, 0)));
	}

	glm::vec3 camPos = getPosition();
	//guarda a posicao para a qual o jogador vai mudar, so muda se nao bater nas paredes
	glm::vec3 tempcamPos = getPosition();
	glm::vec3 playerFront;
	Engine::GetCameraDirectionMessage g(0);

	c.getObject()->sendMessage(&g);

	if (i.getKeyPressed(Key::Fps))
	{
		c.setType(Engine::CameraType::FPS);
	}
	if (i.getKeyPressed(Key::Tps))
	{
		c.setType(Engine::CameraType::TPS);

	}
	if (i.getKeyPressed(Key::Tvs))
	{
		c.setType(Engine::CameraType::TVS);
	}
	switch (c.getType())
	{
	case Engine::CameraType::FPS:
	case Engine::CameraType::TPS:
		playerFront = *(g.getFront());
		break;
	case Engine::CameraType::TVS:
		playerFront = *(g.getUp());
		break;
	default:
		break;
	}
	playerFront.y = 0;
	playerFront = glm::normalize(playerFront);
	if (playerFront.x != playerFront.x)
	{
		c.update(i, delta);
		return;
	}
	float y = getPosition().y;
	bool walk = false;
	glm::vec3 temp = playerFront;
	if (i.getKeyPressed(Key::UpPlayer))
	{
		camPos += playerFront * 0.4f;
		tempcamPos += playerFront * 0.7f;
		walk = true;
		getObject()->sendMessage(&SequenceMessage(0, 3));
	}
	if (i.getKeyPressed(Key::DownPlayer))
	{
		camPos -= playerFront* 0.4f;
		tempcamPos -= playerFront* 0.7f;
		walk = true;
		getObject()->sendMessage(&SequenceMessage(0, 3));
	}

	if (c.getType() == CameraType::FPS){
		if (i.getKeyPressed(Key::RightPlayer))
		{
			camPos -= *(g.getRight())* 0.4f;
			tempcamPos -= *(g.getRight())* 0.7f;
		}
		if (i.getKeyPressed(Key::LeftPlayer))
		{
			camPos += *(g.getRight())* 0.4f;
			tempcamPos += *(g.getRight())* 0.7f;
		}

	}

	if (!walk)
		getObject()->sendMessage(&SequenceMessage(0, 1));

	float angle_to_rotate;

	angle_to_rotate = calculate_rotate_angle(glm::normalize(temp));


	setMeshRotation(angle_to_rotate);


	camPos.y = y;

	setPosition(camPos);

	c.update(i, delta);
	rotateBoxY(angle_to_rotate);
	model->sendMessage(&Engine::SetTransformMessage(0, &transforms));
	v[0].rotation = Rotation(angle_to_rotate, Axis::Y_AXIS);
	v[0].position = getPosition();
	v[0].scale = getBox()->getScale();
	teste->sendMessage(&Engine::SetTransformMessage(0, &v));

	if (!canLoseLife)
	{ 
		this->delta += delta;
		if (this->delta>0.5f){
			canLoseLife = true;
		}
	}

	if (getLifePoints() <= 0){
		gameOver = true;
		
		delta = 0;
		float pontos_float = getScore();
		int pontos = pontos_float;
		s.score("admin","mapa2",pontos);
		string pontos_string = to_string(pontos);
		glViewport(0, 675, 10, 10);
		m.createText(30, "GAME OVER", glm::vec3(1.0f, 1.0f, 1.0f), glm::vec2(1, 2));
		glViewport(0, 600, 10, 10);

		m.createText(30, "SCORE: " + pontos_string, glm::vec3(1.0f, 1.0f, 1.0f), glm::vec2(3, 4));
		glViewport(0, 0, 0, 0);
	}


}

float Player::calculate_rotate_angle(glm::vec3& front){

	glm::vec3 homerVec = glm::vec3(-1, 0, 0);
	glm::vec3 frontVec = (front);

	float dot = homerVec.x * frontVec.x + homerVec.z * frontVec.z;
	float cross = homerVec.x * frontVec.z - homerVec.z * frontVec.x;

	return  180 - glm::degrees(atan2(cross, dot));
}
glm::vec3 Player::getPlayerFront(){
	vec3 playerFront;
	Engine::GetCameraDirectionMessage g(0);

	c.getObject()->sendMessage(&g);
	switch (c.getType())
	{
	case Engine::CameraType::FPS:
	case Engine::CameraType::TPS:
		playerFront = *(g.getFront());
		break;
	case Engine::CameraType::TVS:
		playerFront = *(g.getUp());
		break;
	default:
		break;
	}
	return playerFront;

}

bool Player::isColliding(GameObject* gO){

	if (typeid(*gO) == typeid(Trap))
	{
		//printf("Player against the trap\n", typeid(*this), typeid(*gO));
		// set life points to 0
		setLifePoints(0);
		this->model->sendMessage(&SequenceMessage(0, 21));
		// game over
	}

	if (typeid(*gO) == typeid(Enemy)){
		//printf("Player against the enemy\n", typeid(*this), typeid(*gO));
		if (canLoseLife){
			setLifePoints(getLifePoints() - 10);
			if (getLifePoints() <= 0){
				this->model->sendMessage(&SequenceMessage(0, 20));
				// game over
				
			}
			canLoseLife = false;
			delta = 0;
		}
	}
	if (typeid(*gO) == typeid(Bonus))
	{
		//printf("Player against the bonus\n", typeid(*this), typeid(*gO));
		Bonus* b = static_cast<Bonus*>(gO);
		if (b->getType() == ModelType::LIFEBONUS){
			setLifePoints(getLifePoints() + b->getValue());
		}
		else if (b->getType() == ModelType::POINTBONUS){
			setScore(getScore() + b->getValue());
		}
	}
	return true;
	return true;
}