#pragma once

#include "Game.h"

using namespace Logic;


Game::Game(pugi::xml_document& root) : root(&root), status(GameStatus::LoginMenu)
{
	map = new Map(root);
	// para ja o room e que tem o jogador
}

Game::~Game(){}


Player* Game::getPlayer()const{
	return player;
}
Map* Game::getMap()const{
	return map;
}


void Game::setPlayer(Player* p){
	player = p;
}

void Game::setMap(Map* m){
	map = m;
}

void Game::reset(Player* p){
	p->setPosition(glm::vec3(2,0.6f,2));
	p->getBox()->setPos();
	p->setLifePoints(100);
	// set enemies life to 100
	for each (GameObject* o in getMap()->getActiveRoom()->getObjectList()){
	//	cout << (typeid(*o).name());
		if (typeid(*o) == typeid(Enemy))
		{
			Enemy* e = (Enemy*)o;
			e->setLifePoints(100);
		}
	}
	p->setCameraType(CameraType::TVS);
}

void Game::update(Input& w, float delta)
{
	map->updateRoom(w, delta);
}
void Game::renderGame(int width, int height){
	map->getActiveRoom()->drawRoom(width,height);
}

