#pragma once

#include <iostream>
#include <fstream>
#include "glm\vec3.hpp"
#include <list>
#include "Scene.h"

using namespace Engine;
using namespace std;
using namespace glm;

#define _MAX_GRAPH_NODES 100

/**
*  This class represents a object Node that will represent a node in a graph.
*  @see Graph
*  
*/
namespace Logic{
	class Node{


	private:
		vec3 position;
		/**
		*  The position of the node (X,Y,Z)
		*/

		/**
		*  The id of the object with auto-increment
		*/
		static int nextid;

		float width;
		/**
		*  The width of the node
		*/
		int id;

		/**
		*  The id of the object
		*/

		Object *object;
		/**
		*  The object that will be used in the game engine
		*/
	public:
		Node();
		Node(vec3&, float);

		/**
		*  Constructs a  Node object
		*
		*  @param vec3 position, that represents a position in X,Y,Z axis, and a float width that represents a width of the Object.
		*
		*/
		Node(vec3&);

		/**
		*  Constructs a  Node object
		*
		*  @param vec3 position, that represents a position in X,Y,Z axis.
		*
		*/

		~Node();

		/**
		* Print the coordinates of the node.
		*/

		void printNode();


		void setWidth(float);

		/**
		*  Sets the width of the object
		*  @param width, the width of the node
		*/
		void setPosition(vec3);
		/**
		*  Sets the position of the object
		*  @param vec3 position, the position in X,Y,Z axis of the node
		*/

		void setObject(Object*);

		/**
		*  Sets the Object to the game engine
		*  @param Object
		*/

		/**
		*  Gets the width of the object
		*  @return the object width
		*/
		float getWidth() const;
		/**
		*  Gets the position (vec3) of the object
		*  @return the object position
		*/
		vec3 getPosition() const;

		/**
		*  Gets the id of the object
		*  @return the object id
		*/
		int getId() const;
		/**
		*  Gets the object in the game engine
		*  @return the object
		*/
		Object *getObject()const;
	};
}