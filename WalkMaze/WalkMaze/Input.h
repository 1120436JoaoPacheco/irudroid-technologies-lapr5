#pragma once
#include "GLFW\glfw3.h"
#include "glm\vec2.hpp"
#include <vector>
#include <algorithm>
/**
*  Class that represents the Key Inputs from the user.
*
*/
#define UP_CAMERA GLFW_KEY_W
#define DOWN_CAMERA GLFW_KEY_S
#define LEFT_CAMERA GLFW_KEY_A
#define RIGHT_CAMERA GLFW_KEY_D
#define UP_PLAYER GLFW_KEY_UP
#define DOWN_PLAYER GLFW_KEY_DOWN
#define LEFT_PLAYER GLFW_KEY_LEFT
#define RIGHT_PLAYER GLFW_KEY_RIGHT
#define MAP_1 GLFW_KEY_1
#define MAP_2 GLFW_KEY_2
#define MAP_3 GLFW_KEY_3
#define FirstPS GLFW_KEY_U
#define ThirdPS GLFW_KEY_I
#define TopVS GLFW_KEY_O
#define MOUSE_CLICK GLFW_MOUSE_BUTTON_LEFT
#define DAYTIME GLFW_KEY_X
#define EXECUTE_PATH GLFW_KEY_E 
#define SHOOT GLFW_KEY_SPACE

#define NUMBER_OF_KEYS 18

enum class Key
{
	UpPlayer, DownPlayer, LeftPlayer, RightPlayer,
	UpCamera, DownCamera, LeftCamera, RightCamera,
	Map1,Map2,Map3,Fps,Tps,Tvs, DayTime, ExecutePath,
	Shoot, MouseClick
};

namespace Logic{

	
	/**
	*  This class manages the inputs received
	*
	*/
	class Input{

	private:
		bool keysArray[NUMBER_OF_KEYS];

		glm::vec2 mouseDelta, mousePos;
		void updateMouse(GLFWwindow*&, bool, float);
		void updateKeyboard(GLFWwindow*&, float);
		
	public:
		Input() { std::fill(keysArray, keysArray + NUMBER_OF_KEYS, false); };
		Input(const Input&);
		~Input() {};

		bool getKeyPressed(Key d)  { return keysArray[(int)d]; };

		glm::vec2 getMouseDelta() { return mouseDelta; }
		glm::vec2 getMousePos() { return mousePos; }

		/*
		* This method updates both the mouse and the keyboard inputs ie keys received and mouse position or clicks
		* 
		* @param window GLFWwindow*, the pointer to the game window
		* @param center bool, true if the mouse should be centered. Needed for gameplay, but not for menus.
		*/
		void update(GLFWwindow*, bool, float);
		std::string key_to_String(std::vector<Input>);
	};
}