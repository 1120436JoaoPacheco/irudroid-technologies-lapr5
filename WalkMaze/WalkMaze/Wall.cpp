#include "Wall.h"

using namespace Logic;
using namespace Engine;
Wall::Wall(glm::vec3 pos, glm::vec3 extent, Engine::Object *o) : GameObject(0, o, pos, Engine::Rotation()), extent(extent), rotation(0)
{
	setBox(PhysicsBox(pos, extent));
}

Wall::Wall(PhysicsBox b, Engine::Object* o) : GameObject(b, o), extent(extent), rotation(0)
{
	extent = b.getScale();
	rotation = b.getRotation();

}

Wall::~Wall(){}




