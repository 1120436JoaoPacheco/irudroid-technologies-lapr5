#include "Character.h"

using namespace Logic;

Character::Character(float speed, int life, glm::vec3 direction) : speed(speed), lifePoints(life), direction(direction){}

float Character::getSpeed() const {
	return speed;
}

int Character::getLifePoints() const {
	return lifePoints;
}

glm::vec3 Character::getDirection() const {
	return direction;
}


void Character::setSpeed(float spd) {
	speed = spd;
}

void Character::setLifePoints(int lp) {
	if (lp<0){
		lifePoints = 0;
	}
	else if (lp>100){
		lifePoints = 100;
	}
	else{
		lifePoints = lp;
	}
}

void Character::setDirection(glm::vec3 dir){
	direction = dir;
}

void Character::update(Input& i, float delta){

}