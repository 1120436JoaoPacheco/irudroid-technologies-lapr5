#include "GameObject.h"

using namespace Logic;

GameObject::GameObject(int id, Engine::Object* o, glm::vec3 p, Engine::Rotation r) : idGameObject(id), obj(o), position(p), rotation(r){
	to_delete = false;
}

GameObject::GameObject(glm::vec3 p) : idGameObject(0), obj(NULL), position(p){
	to_delete = false;
}
GameObject::GameObject(PhysicsBox& b, Engine::Object* o) : box(b), obj(o) { 
	position = b.getPos();
	to_delete = false;
}
GameObject::GameObject() : to_delete(false), position(0,0,0), idGameObject(0), obj(NULL){}
GameObject::~GameObject(){}

GameObject::GameObject(const GameObject& go)
{
	idGameObject = go.idGameObject;
	obj = go.obj;
	position = go.position;
	rotation = go.rotation;
	box = go.box;
	to_delete = go.to_delete;
}


int GameObject::getidGameObject() const{
	return idGameObject;
}

Engine::Object* GameObject::getObject() const{
	return obj;
}

glm::vec3 GameObject::getPosition() const{
	return box.getPos();
}

Engine::Rotation GameObject::getMeshRotation() const{
	return rotation;
}

void GameObject::setidGameObject(int idGO){
	idGameObject = idGO;
}

void GameObject::setObject(Engine::Object* ob){
	obj = ob;
}

void GameObject::setPosition(glm::vec3 po){
	box.updatePos(po);
	position = po;
}

void GameObject::setMeshRotation(Engine::Rotation ro){
	rotation = ro;
}