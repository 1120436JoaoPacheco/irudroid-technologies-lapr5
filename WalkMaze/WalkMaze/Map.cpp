#include "Map.h"

using namespace Logic;

Map::Map(pugi::xml_document& mapNode): node(&mapNode) {
	
	//Load the first Room
	activeRoom = new Room(mapNode.child("map").child("rooms").find_child_by_attribute("id", "1"));
	rooms.push_back(activeRoom);

	//para mudar de quarto rapido tem que carregar os quatros logo ao criar o mapa
	int numeroquartos = stoi(mapNode.child("map").child("rooms").attribute("number").value());
	for (int i = 2; i <= numeroquartos; i++){
		string idtemp = to_string(i);
		char* idquarto = (char*)idtemp.c_str();
		rooms.push_back(new Room(mapNode.child("map").child("rooms").find_child_by_attribute("id", idquarto)));
	}
};

Map::~Map(){}

vector<Room*> Map::getRooms() const {
	return rooms;
}

Room* Map::getActiveRoom() const {
	return activeRoom;
}


void Map::updateRoom(Input& i, float delta)
{
	
	int room = 0;
	if (i.getKeyPressed(Key::Map1))
	{
		room = 1;
	}
	else if (i.getKeyPressed(Key::Map2))
	{
		room = 2;
	}
	else if (i.getKeyPressed(Key::Map3))
	{
		room = 3;
	}
	
	if (room > 0 && room != activeRoom->getIdRoom())
	{
		setActiveRoom(room);
	}
	activeRoom->updateRoom(i, delta);
}

void Map::setActiveRoom(int id) {


	Room * next = NULL;
	for (Room * room : rooms)
	{
		if (room->getIdRoom() == id)
			next = room;
	}

	if (next == NULL)
	{
		pugi::xml_node a = (*this->node).child("map").child("rooms").find_child_by_attribute("id", to_string(id).c_str());
		activeRoom = new Room(a);
		rooms.push_back(activeRoom);
	}
	else
	{
		
		Engine::CameraType a = activeRoom->getPlayer()->getCameraType();
		next->getPlayer()->setCameraType(a);
		next->getPlayer()->setLifePoints(activeRoom->getPlayer()->getLifePoints());
		activeRoom = next;
	}

}
