#pragma once

#include "glm\vec3.hpp"
#include <vector>
#include "Scene.h"
#include "Door.h"
#include "Graph.h"
#include "Input.h"
#include "Player.h"
#include "ModelComponent.h"
#include "ModelFactory.h"
#include "Obstacle.h"
#include "Skybox.h"
#include "LightComponent.h"
#include "SetLightMessage.h"
#include "Enemy.h"
#include "DayTimeMessage.h"
#include "Trap.h"
#include "PhysicsEngine.h"
#include "Projectil.h"
#include "Bonus.h"
#include"CylinderComponent.h"
using namespace std;



namespace Logic{
	class Room{

	private:
		int idRoom;
		Graph graph;
		vector<GameObject*> objectList;
		vector<GameObject*> deleteList;
		vector<Door*> listaportas;
		vec3 dimension;
		Scene scene;
		Player* player;
		Light light;
		Object* lc;
		Skybox* sb;
	
		Projectil* projetil;
	public:
		Room();
		Room(pugi::xml_node& n);
		~Room();

		int getIdRoom()const;
		Graph getGraph()const;
		vec3 getDimension()const;
		vector<Bonus*> getBonuslist()const;
		vector<GameObject*> getObjectList()const;
		Scene getScene() const;

		void drawRoom(int width, int height);
		void updateRoom(Input&, float delta);

		/**
		*  Draws Room Walls
		*
		*/
		void drawRoomWalls(pugi::xml_node& n, Scene& s);
		void drawRoomObjects(pugi::xml_node& n, Scene& s);
		void drawRoomEnemies(pugi::xml_node& n, Scene& s);
		void drawTraps(pugi::xml_node& n, Scene& s);
		void drawRoomBonus(pugi::xml_node& n, Scene &s);

		Player* getPlayer() const;
		vector<Obstacle*> getObstaclelist()const ;
		
		vector<Door*> getDoorlist()const;
		
		void setPlayer(Player*);

		void createProjetil(glm::vec3 pos,glm::vec3 pfront, float);
	};
}