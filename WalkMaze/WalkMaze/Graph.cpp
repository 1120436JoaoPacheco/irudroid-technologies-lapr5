﻿#include "Graph.h"
#include <string.h>

using namespace Logic;

Graph::Graph(){

}

Graph::~Graph(){

}

void Graph::addNode(Node * no){
	if (nodelist.size() < _MAX_GRAPH_NODES){
		nodelist.push_back(no);

	}
}

void Graph::deleteNode(int indNode){
	if (indNode >= 0 && indNode < (int)nodelist.size())
		nodelist.erase(nodelist.begin() + (indNode - 1));
}

void Graph::listEdges(){
	for (Edge *arco : edgelist)
	{
		cout << "No início:";
		arco->getNoi()->printNode();
		cout << "No final:";
		arco->getNof()->printNode();
		cout << endl;
		cout << "Weight:" << arco->getWeight() << "Width:" << arco->getWidth() << endl;
	}
}
void Graph::leGrafo(pugi::xml_node * source){
	ifstream myfile;
	pugi::xml_document doc;
	int numNodes, numArcos = 0;
	pugi::xml_node ramoarcos = source->child("edges").child("edge");
	pugi::xml_node ramo_nos = source->child("nodes").child("node");
	vec3 temp; //vector onde as posicoes vao ser guardadas.
	//array de apontadores que vai guardar os n�s para n�o criar conflitos no ficheiro do grafo.
	vector<Node*> nosId;
	float tempwidth;

	numNodes = stoi(source->child("nodes").attribute("number").value());
	nosId.clear();
	nodelist.clear();
	//grava numero de nos
	int i = 0;
	for (ramo_nos; ramo_nos; ramo_nos = ramo_nos.next_sibling("node")){

		temp.x = stof(ramo_nos.attribute("x").value());
		temp.y = stof(ramo_nos.attribute("y").value());
		temp.z = stof(ramo_nos.attribute("z").value());
		tempwidth = stof(ramo_nos.attribute("width").value());

		Node *node = new Node(temp, tempwidth);
		nosId.push_back(node);
		nodelist.push_back(node);


		////depois de adicionar a  lista de nos, tamb�m adiciona no array de apontadores para depois no ficheiro do grafo
		////saber linha  a linha a que n� est� a ser referido.
		i++;
	}
	//ler arcos pelo xml
	int noitemp, noftemp;
	float pesotemp, larguratemp;
	//grava numero arcos
	//numArcos = stoi(doc.child("map").child("rooms").find_child_by_attribute("id", roomId).child("graph").child("edges").attribute("number").value());
	numArcos = stoi(source->child("edges").attribute("number").value());

	edgelist.clear();
	for (ramoarcos; ramoarcos; ramoarcos = ramoarcos.next_sibling("edge"))
	{
		//grava arco
		noitemp = stoi(ramoarcos.attribute("noi").value());
		noftemp = stoi(ramoarcos.attribute("nof").value());
		pesotemp = stof(ramoarcos.attribute("weight").value());
		larguratemp = stof(ramoarcos.attribute("width").value());

		edgelist.push_back(new Edge(nosId[noitemp], nosId[noftemp], pesotemp, larguratemp));



	}
}
vector<Node*> Graph::getNodelist() const {
	return nodelist;
}

vector<Edge*> Graph::getEdgelist() const{
	return edgelist;
}

void Graph::addEdge(Edge * arco){
	if (edgelist.size() < _MAX_GRAPH_EDGES){
		edgelist.push_back(arco);
	}

}

void Graph::deleteEdge(int edgenumber){
	if (edgenumber >= 0 && edgenumber < (int)edgelist.size())
		edgelist.erase(edgelist.begin() + (edgenumber - 1));
}


Engine::Mesh* getWallMeshR(const glm::vec3& start, glm::vec3& end, float rotY, float rotX, int height, int width)
{
	float halfHeight = height / 2.0f;
	float halfWidth = width / 2.0f;

	glm::vec3 offsetTL(-(halfWidth + infintesimo), height + infintesimo, halfWidth + infintesimo); //Top left
	glm::vec3 offsetBL(-(halfWidth + infintesimo), 0, halfWidth + infintesimo); //Bot left
	glm::vec3 offsetBR(halfWidth + infintesimo, 0, halfWidth + infintesimo); //Bot right
	glm::vec3 offsetTR(halfWidth + infintesimo, height + infintesimo, halfWidth + infintesimo); //Top right


	glm::vec3 dir(glm::normalize(end - start));
	glm::vec3 topleft, topright, botleft, botright;
	glm::vec3 tl, bl, br, tr;

	tl = glm::rotateY(offsetTL, rotY);
	bl = glm::rotateY(offsetBL, rotY);
	br = glm::rotateY(offsetBR, rotY);
	tr = glm::rotateY(offsetTR, rotY);

	float dist = glm::distance(start, end);

	float pieceStartX, pieceEndX,
		pieceStartY, pieceEndY, pieceStartZ, pieceEndZ;

	int times = dist / 3;
	float uvXStep = 1 / 3.0f;
	float uvYStep = (2.0f) / height;


	Mesh* m = new Mesh(true);
	glm::vec3 uVtopleft, uVtopright, uVbotleft, uVbotright;


	for (int i = 0; i < (int)dist + 1; ++i)
	{
		//front 
		pieceStartX = dir.x * i;
		pieceEndX = dir.x * (i + 1);

		pieceStartZ = dir.z * i;
		pieceEndZ = dir.z * (i + 1);

		pieceStartY = dir.y * i;
		pieceEndY = dir.y * (i + 1);


		uVtopleft = glm::vec3(
			uvXStep * i,
			uvYStep * (i + 1), 0);
		uVtopright = glm::vec3(
			uvXStep * (i + 1),
			uvYStep * (i + 1), 0);
		uVbotleft = glm::vec3(
			uvXStep * i,
			uvYStep * i, 0);
		uVbotright = glm::vec3(
			uvXStep * (i + 1),
			uvYStep * i, 0);

		topleft = glm::vec3(
			start.x + pieceEndX + tl.x,
			start.y + pieceEndY - 0.5f,
			start.z + pieceEndZ + tl.z);


		topright = glm::vec3(
			start.x + pieceEndX + tr.x,
			start.y + pieceEndY - 0.5f,
			start.z + pieceEndZ + tr.z);

		botleft = glm::vec3(
			start.x + pieceStartX + bl.x,
			start.y + pieceStartY - 0.5,
			start.z + pieceStartZ + bl.z);

		botright = glm::vec3(
			start.x + pieceStartX + br.x,
			start.y + pieceStartY - 0.5,
			start.z + pieceStartZ + br.z);

		QuadPrimitive front(false, topleft, topright, botleft, botright, uVtopleft, uVtopright, uVbotleft, uVbotright);
	
		m->addPrimitive(front);

	}


	return m;
}

Wall* Graph::drawNode(Node* nodei, Scene& sc, Texture t){

	Edge* edge = edgelist.front();
	Node* nodef;

	float ri = K_CIRCLE * nodei->getWidth()* 0.5f;
	int cont = 0;
	//iterate edgelist
	for (int i = 0; i < (int)edgelist.size(); i++)
	{
		edge = edgelist[i];
		if (nodei == edge->getNoi()){
			nodef = edge->getNof();
			cont++;
		}
		else if (nodei == edge->getNof()){
			nodef = edge->getNoi();
			cont++;
		}
		else{
			continue;
		}
	}

	float alfa = atan2((nodef->getPosition().x - nodei->getPosition().x), (nodef->getPosition().z - nodei->getPosition().z));
	float w = edge->getWidth();
	float si = K_CONNECTION * ri;

	Object* obj = sc.addNewObject();

	std::vector<Transform> transforms;

	float alfaD = glm::degrees(alfa);
	//transform 1
	if (cont > 2 || (abs(alfaD) != 180 && abs(alfaD) != 0 && abs(alfaD) != 90 && abs(alfaD) != 360))
	{
		Engine::Rotation rotation(90, Engine::Axis::X_AXIS);
		CylinderComponent *cc = new CylinderComponent(1.7f, 1.7f, 5.0f, 100, 100);
		transforms.push_back(Transform{ nodei->getPosition() + glm::vec3(0, 4.5f, 0), glm::vec3(1, 1, 1), rotation });
		cc->setTexture(t);
		obj->addComponent(cc);
		obj->sendMessage(&SetTransformMessage(0, &transforms));
	}
	else{
		Engine::Rotation rotation(alfa*180/M_PI, Engine::Axis::Y_AXIS);
		transforms.push_back(Transform{ nodei->getPosition() + glm::vec3(0.0f, 2.0f, 0.0f), glm::vec3(2, 5, 2), rotation });
		Mesh* m = CubeMeshFactory::GetInstance()->getMesh();
		m->setTexture(t);
		obj->addComponent(new RenderComponent(m));
		obj->sendMessage(&SetTransformMessage(0, &transforms));
	}
	glm::vec3 center = (nodei->getPosition() + glm::vec3(0, 2.0f, 0));
	glm::vec3 scaleX = glm::vec3(1.0f, 2.5f, 1.0f);
	Wall* wall = new Wall(center, scaleX, obj);
	wall->rotateBoxY(glm::degrees(alfa));

	return wall;
}




Engine::Mesh* getWallMesh(const glm::vec3& start, glm::vec3& end, float rot, int height, int width)
{
	float halfHeight = height / 2.0f;
	float halfWidth = width / 2.0f;

	glm::vec3 offsetTL(-(halfWidth + infintesimo), halfHeight + infintesimo, halfWidth + infintesimo); //Top left
	glm::vec3 offsetBL(-(halfWidth + infintesimo), -(halfHeight + infintesimo), halfWidth + infintesimo); //Bot left
	glm::vec3 offsetBR(halfWidth + infintesimo, -(halfHeight + infintesimo), halfWidth + infintesimo); //Bot right
	glm::vec3 offsetTR(halfWidth + infintesimo, halfHeight + infintesimo, halfWidth + infintesimo); //Top right


	glm::vec3 dir(glm::normalize(end - start));
	glm::vec3 topleft, topright, botleft, botright;
	glm::vec3 tl, bl, br, tr;

	tl = glm::rotateY(offsetTL, rot);
	bl = glm::rotateY(offsetBL, rot);
	br = glm::rotateY(offsetBR, rot);
	tr = glm::rotateY(offsetTR, rot);



	float dist = glm::distance(start, end);

	float pieceStartX, pieceEndX,
		pieceStartY, pieceEndY, pieceStartZ, pieceEndZ;

	int times = dist / 3;
	float uvXStep = 1 / 3.0f;
	float uvYStep = (2.0f) / height;


	Mesh* m = new Mesh(true);
	glm::vec3 uVtopleft, uVtopright, uVbotleft, uVbotright;

	for (int b = 0; b < height; b++)
	{
		for (int i = 0; i < (int)dist; ++i)
		{
			//front 
			pieceStartX = dir.x * i;
			pieceEndX = dir.x * (i + 1);

			pieceStartZ = dir.z * i;
			pieceEndZ = dir.z * (i + 1);


			uVtopleft = glm::vec3(
				uvXStep * i,
				uvYStep * (b + 1), 0);
			uVtopright = glm::vec3(
				uvXStep * (i + 1),
				uvYStep * (b + 1), 0);
			uVbotleft = glm::vec3(
				uvXStep * i,
				uvYStep * b, 0);
			uVbotright = glm::vec3(
				uvXStep * (i + 1),
				uvYStep * b, 0);


			topleft = glm::vec3(
				start.x + pieceStartX + tl.x,
				start.y + b + (0.5f),
				start.z + pieceStartZ + tl.z);

			topright = glm::vec3(
				start.x + pieceEndX + tl.x,
				end.y + b + (0.5f),
				start.z + pieceEndZ + tl.z);

			botleft = glm::vec3(
				start.x + pieceStartX + bl.x,
				start.y + b - (0.5f),
				start.z + pieceStartZ + bl.z);

			botright = glm::vec3(
				start.x + pieceEndX + bl.x,
				end.y + b - (0.5f),
				start.z + pieceEndZ + bl.z);

			QuadPrimitive front(true, topleft, topright, botleft, botright, uVtopleft, uVtopright, uVbotleft, uVbotright);
		
			m->addPrimitive(front);

			//back

			topleft = glm::vec3(
				start.x + pieceStartX + tr.x,
				start.y + b + (0.5f),
				start.z + pieceStartZ + tr.z);

			topright = glm::vec3(
				start.x + pieceEndX + tr.x,
				start.y + b + (0.5f),
				start.z + pieceEndZ + tr.z);

			botleft = glm::vec3(
				start.x + pieceStartX + br.x,
				start.y + b - (0.5f),
				start.z + pieceStartZ + br.z);

			botright = glm::vec3(
				start.x + pieceEndX + br.x,
				start.y + b - (0.5f),
				start.z + pieceEndZ + br.z);

			QuadPrimitive back(false, topleft, topright, botleft, botright, uVtopleft, uVtopright, uVbotleft, uVbotright);
		

			m->addPrimitive(back);
		}
	}
	float topHeight = height - 0.5f + infintesimo;
	for (int i = 0; i < (int)dist; ++i)
	{
		pieceStartX = dir.x * i;
		pieceEndX = dir.x * (i + 1);

		pieceStartZ = dir.z * i;
		pieceEndZ = dir.z * (i + 1);

		topleft = glm::vec3(
			start.x + pieceStartX + tl.x,
			topHeight,
			start.z + pieceStartZ + tl.z);

		topright = glm::vec3(
			start.x + pieceStartX + tr.x,
			topHeight,
			start.z + pieceStartZ + tr.z);

		botleft = glm::vec3(
			start.x + pieceEndX + tl.x,
			topHeight,
			start.z + pieceEndZ + tl.z);

		botright = glm::vec3(
			start.x + pieceEndX + tr.x,
			topHeight,
			start.z + pieceEndZ + tr.z);

		QuadPrimitive top(topleft, topright, botleft, botright);

		m->addPrimitive(top);

	}

	return m;
}
#include "MiniMapRenderComponent.h"
Wall* Graph::drawEdge(Edge* edge, Scene& sc, Texture t){

	Node* nodei = edge->getNoi();
	Node* nodef = edge->getNof();

	vec3 nodefv = nodef->getPosition();
	vec3 nodeiv = nodei->getPosition();
	float alfa = atan2((nodefv.x - nodeiv.x), (nodefv.z - nodeiv.z));			// orienta�ao

	Object* obj = sc.addNewObject();

	Engine::Rotation rotation(alfa * 180 / (float)M_PI, Engine::Axis::Y_AXIS);

	std::vector<Transform> transforms;

	glm::vec3 newVec(0.0f, 0.0f, 0.0f);
	float dist = glm::distance(nodefv, nodeiv);
	vec3 scale = vec3(1, 1, 1);
	//transform 1
	transforms.push_back(Transform{ newVec, scale, Rotation() });

	obj->sendMessage(&Engine::SetTransformMessage(0, &transforms));

	Mesh * mesh = new Mesh(true);

	float halfHeight = 5 / 2.0f;
	float halfWidth = 2 / 2.0f;

	glm::vec3 offsetTL(-(halfWidth + infintesimo), halfHeight + infintesimo, halfWidth + infintesimo); //Top left
	glm::vec3 offsetBL(-(halfWidth + infintesimo), -(halfHeight + infintesimo), halfWidth + infintesimo); //Bot left
	glm::vec3 offsetBR(halfWidth + infintesimo, -(halfHeight + infintesimo), halfWidth + infintesimo); //Bot right
	glm::vec3 offsetTR(halfWidth + infintesimo, halfHeight + infintesimo, halfWidth + infintesimo); //Top right


	glm::vec3 tl = glm::rotateY(offsetTL, alfa);
	glm::vec3 bl = glm::rotateY(offsetBL, alfa);
	glm::vec3 br = glm::rotateY(offsetBR, alfa);
	glm::vec3 tr = glm::rotateY(offsetTR, alfa);


	tl.y = tl.z;
	bl.y = bl.z;
	br.y = br.z;
	tr.y = tr.z;

	tl.z = bl.z = br.z = tr.z = 0;
	glm::vec3 iv(nodeiv.x, nodeiv.z, 0);
	glm::vec3 fv(nodefv.x, nodefv.z, 0);

	glm::vec3 temp1 = tl + iv;
	glm::vec3 temp2 = tr + iv;
	glm::vec3 temp3 = bl + fv;
	glm::vec3 temp4 = br + fv;

	QuadPrimitive q(temp1, temp2, temp3, temp4);
	mesh->addPrimitive(q);
	MiniMapRenderComponent* renderM = new MiniMapRenderComponent(mesh, glm::vec3(0, 0, 0), glm::vec3(1, 1, 1), 0);
	obj->addComponent(renderM);

	float pij = sqrt(pow((nodefv.x - nodeiv.x), 2) + pow((nodefv.y - nodeiv.y), 2));	// comprimento da projeção
	float hij = nodefv.y - nodeiv.y;

	float beta = (pij != 0) ? atan(hij / pij) : 0;

	if (beta == 0)
		mesh = getWallMesh(nodeiv, nodefv, alfa, 5, 2);
	else
		mesh = getWallMeshR(nodeiv, nodefv, alfa, beta, 5, 2);

	mesh->setTexture(t);

	obj->addComponent(new RenderComponent(mesh));

	glm::vec3 center = (nodeiv + nodefv) / glm::vec3(2, 2, 2);
	glm::vec3 scaleX = glm::vec3(0.5f, 2.5f, dist / 2.0f);

	PhysicsBox box(center, scaleX);
	box.rotate(glm::degrees(alfa));

	Wall* w = new Wall(box, obj);
	return w;
}

void drawGround(Engine::Scene& s, Engine::Texture text)
{
	int width = 100;
	int height = 100;

	Mesh * m = new Mesh(true);

	float maxX, maxY;
	for (int y = 0; y < height; ++y)
	{
		maxY = (y + 1);
		for (int x = 0; x < width; ++x)
		{
			maxX = (x + 1);

			glm::vec3 topLeft(maxX, -0.5f, maxY);
			glm::vec3 botLeft(maxX, -0.5f, y);
			glm::vec3 topRight(x, -0.5f, maxY);
			glm::vec3 botRight(x, -0.5f, y);

			QuadPrimitive back(topLeft, topRight, botLeft, botRight);
			m->addPrimitive(back);
		}
	}

	Engine::Object* o = s.addNewObject();
	m->setTexture(text);

	o->addComponent(new Engine::RenderComponent(m));

}

void Graph::drawGraph(Scene &scene, std::vector<GameObject*>& boxes)
{
	Texture t = TextureFactory::GetInstance()->createTexture("wall.jpg");
	Texture t1 = TextureFactory::GetInstance()->createTexture("floor.jpg");

	drawGround(scene, t1);
	for (int i = 0; i < (int)nodelist.size(); i++)
		boxes.push_back(drawNode(nodelist[i], scene, t));

	for (int i = 0; i < (int)edgelist.size(); i++)
		boxes.push_back(drawEdge(edgelist[i], scene, t));
}