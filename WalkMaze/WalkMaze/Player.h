#pragma once
#define _USE_MATH_DEFINES

#include "glm\vec3.hpp"
#include <vector>
#include "Camera.h"
#include "Edge.h"
#include "Character.h"
#include "Model.h"
#include "ModelFactory.h"
#include "ModelComponent.h"
#include "MiniMapRenderComponent.h"
#include "LifeBarRenderComponent.h"
#include "QuadPrimitive.h"
#include <math.h>
#include "Trap.h"
#include "Enemy.h"
#include "Bonus.h"
/**
*  This class represents an object Player, child of Character.
*  @see Character
*/

namespace Logic{
	class Player : public Character {

	private:
		float delta;
		bool canLoseLife;
		int score;
		bool gameOver;
		Camera c;
		Engine::LifeBarRenderComponent* LifeBar;
		Engine::Object* model;
		std::vector<Engine::Transform> transforms; //0 standard hormer rot 0 homer translate, 1 homer rot
		Object * teste;
		std::vector<Transform> v;
	public:
		Player(int,glm::vec3 pos, Engine::Scene& cena);
		~Player();

		int getScore() const;

		Engine::CameraType getCameraType();

		Camera getCamera(){ return c; };

		void setCamera(Camera cam){ c = cam; };
		
		void setScore(int);

		void update(Input&, float);
		 
		void setCameraType(Engine::CameraType a){
			c.setType(a);
		}

		virtual void setPosition(glm::vec3 pos) {
			GameObject::setPosition(pos);
			transforms[0].position = GameObject::getPosition();

		}

		virtual void setMeshRotation(float rot_y) {
			GameObject::setMeshRotation(Engine::Rotation(rot_y,Engine::Axis::Z_AXIS));
			transforms[1].rotation = Engine::Rotation(rot_y, Engine::Axis::Z_AXIS);
			
		}

		float calculate_rotate_angle(glm::vec3&);

		glm::vec3 getPlayerFront();
		/**
		*  Check if the player collide with other Game Objects (Enemies,Traps...).
		*  @see GameObject
		*/
		bool isColliding(GameObject*);

	};
}