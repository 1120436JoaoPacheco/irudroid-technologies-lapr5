#include "Obstacle.h"

using namespace Logic;
using namespace Engine;

Obstacle::Obstacle(int s, Scene& cena, ModelType& type, glm::vec3 pos)
{
	Transform t;
	ModelComponent* renderC;
	ModelFactory* meshFac;
	Model * modelo;
	QuadPrimitive q;
	Mesh * mesh = new Mesh(true);
	Object * model;
	switch (type){
	case ModelType::STATUE:
	{
		model = cena.addNewObject();
		this->setPosition(pos);

		meshFac = ModelFactory::Get();
		modelo = meshFac->createModel(type);
		renderC = new ModelComponent(modelo);
		mesh->addPrimitive(q);
		MiniMapRenderComponent* renderM = new MiniMapRenderComponent(mesh, glm::vec3(1, 1, 1), glm::vec3(2, 2, 2), 0);
		model->addComponent(renderM);
		model->addComponent(renderC);

		t.position = pos;
		t.scale = glm::vec3(0.05f, 0.05f, 0.05f);
		t.rotation = Rotation();
		transforms.push_back(t);

		t.position = glm::vec3(0, 0, 0);
		t.scale = glm::vec3(1.0f, 1.0f, 1.0f);
		t.rotation = Rotation(-90, Axis::X_AXIS);
		transforms.push_back(t);

		PhysicsBox box(pos, glm::vec3(0.3f, 0.3f, 0.3f));
		setBox(box);

		model->sendMessage(&Engine::SetTransformMessage(0, &transforms));
		break;
	}
	case ModelType::CRATE:
	{
		model = cena.addNewObject();
		this->setPosition(pos);
		meshFac = ModelFactory::Get();
		modelo = meshFac->createModel(type);
		renderC = new ModelComponent(modelo);
		mesh->addPrimitive(q);
		MiniMapRenderComponent* renderM = new MiniMapRenderComponent(mesh, glm::vec3(1, 1, 1), glm::vec3(2, 2, 2), 0);
		 model->addComponent(renderM);
		model->addComponent(renderC);
		
		t.position = pos;
		t.scale = glm::vec3(0.05f, 0.05f, 0.05f);
		t.rotation = Rotation();
		transforms.push_back(t);

		t.position = glm::vec3(0, 0, 0);
		t.scale = glm::vec3(1.0f, 1.0f, 1.0f);
		t.rotation = Rotation(-90, Axis::X_AXIS);
		transforms.push_back(t);
		model->sendMessage(&Engine::SetTransformMessage(0, &transforms));
		break;
	}
	case ModelType::SHELVE:
	{
		this->setPosition(pos);
		model = cena.addNewObject();
		meshFac = ModelFactory::Get();
		modelo = meshFac->createModel(type);
		renderC = new ModelComponent(modelo);
		model->addComponent(renderC);
		mesh->addPrimitive(q);
		MiniMapRenderComponent* renderM = new MiniMapRenderComponent(mesh, glm::vec3(1, 1, 1), glm::vec3(2, 2, 2), 0);
		model->addComponent(renderM);
		t.position = pos;
		t.scale = glm::vec3(0.05f, 0.05f, 0.05f);
		t.rotation = Rotation();
		transforms.push_back(t);

		t.position = glm::vec3(0, 0, 0);
		t.scale = glm::vec3(1.0f, 1.0f, 1.0f);
		t.rotation = Rotation(-90, Axis::X_AXIS);
		transforms.push_back(t);


		model->sendMessage(&Engine::SetTransformMessage(0, &transforms));
		break;
	}
	case ModelType::WRECKEDTABLE:
	{
		this->setPosition(pos);
		model = cena.addNewObject();
		meshFac = ModelFactory::Get();
		modelo = meshFac->createModel(type);
		renderC = new ModelComponent(modelo);
		mesh->addPrimitive(q);
		MiniMapRenderComponent* renderM = new MiniMapRenderComponent(mesh, glm::vec3(1, 1, 1), glm::vec3(2, 2, 2), 0);
		model->addComponent(renderM);
		model->addComponent(renderC);

		t.position = pos;
		t.scale = glm::vec3(0.05f, 0.05f, 0.05f);
		t.rotation = Rotation();
		transforms.push_back(t);

		t.position = glm::vec3(0, 0, 0);
		t.scale = glm::vec3(1.0f, 1.0f, 1.0f);
		t.rotation = Rotation(-90, Axis::X_AXIS);
		transforms.push_back(t);


		model->sendMessage(&Engine::SetTransformMessage(0, &transforms));
		break;
	}
	case ModelType::WARDROBE:
	{
		model = cena.addNewObject();
		this->setPosition(pos);
		meshFac = ModelFactory::Get();
		modelo = meshFac->createModel(type);
		renderC = new ModelComponent(modelo);
		mesh->addPrimitive(q);
		MiniMapRenderComponent* renderM = new MiniMapRenderComponent(mesh, glm::vec3(1, 1, 1), glm::vec3(2, 2, 2), 0);
		model->addComponent(renderM);
		model->addComponent(renderC);
		
		t.position = pos;
		t.scale = glm::vec3(0.05f, 0.05f, 0.05f);
		t.rotation = Rotation();
		transforms.push_back(t);
		
		t.position = glm::vec3(0, 0, 0);
		t.scale = glm::vec3(1.0f, 1.0f, 1.0f);
		t.rotation = Rotation(-90, Axis::X_AXIS);
		transforms.push_back(t);

		model->sendMessage(&Engine::SetTransformMessage(0, &transforms));
		break;
	}
	case ModelType::BARREL:
	{
		model = cena.addNewObject();
		this->setPosition(pos);
		meshFac = ModelFactory::Get();
		modelo = meshFac->createModel(type);
		renderC = new ModelComponent(modelo);
		mesh->addPrimitive(q);
		MiniMapRenderComponent* renderM = new MiniMapRenderComponent(mesh, glm::vec3(1, 1, 1), glm::vec3(2, 2, 2), 0);
		model->addComponent(renderM);
		model->addComponent(renderC);
		
		t.position = pos;
		t.scale = glm::vec3(0.05f, 0.05f, 0.05f);
		t.rotation = Rotation();
		transforms.push_back(t);
		t.position = glm::vec3(0, 0, 0);
		t.scale = glm::vec3(1.0f, 1.0f, 1.0f);
		t.rotation = Rotation(-90, Axis::X_AXIS);
		transforms.push_back(t);


		model->sendMessage(&Engine::SetTransformMessage(0, &transforms));
		break;
	}
	case ModelType::PIANO:
	{
		model = cena.addNewObject();
		this->setPosition(pos);
		meshFac = ModelFactory::Get();
		modelo = meshFac->createModel(type);
		renderC = new ModelComponent(modelo);
		mesh->addPrimitive(q);
		MiniMapRenderComponent* renderM = new MiniMapRenderComponent(mesh, glm::vec3(1, 1, 1), glm::vec3(2, 2, 2), 0);
		model->addComponent(renderM);
		model->addComponent(renderC);

		t.position = pos;
		t.scale = glm::vec3(0.05f, 0.05f, 0.05f);
		t.rotation = Rotation();
		transforms.push_back(t);

		t.position = glm::vec3(0, 0, 0);
		t.scale = glm::vec3(1.0f, 1.0f, 1.0f);
		t.rotation = Rotation(-90, Axis::X_AXIS);
		transforms.push_back(t);


		PhysicsBox box(pos, glm::vec3(2.0f, 2.0f, 2.0f));
		setBox(box);

		model->sendMessage(&Engine::SetTransformMessage(0, &transforms));
		break;
	}
	case ModelType::PLANT:
	{
		model = cena.addNewObject();
		this->setPosition(pos);
		meshFac = ModelFactory::Get();
		modelo = meshFac->createModel(type);
		renderC = new ModelComponent(modelo);
		mesh->addPrimitive(q);
		MiniMapRenderComponent* renderM = new MiniMapRenderComponent(mesh, glm::vec3(1, 1, 1), glm::vec3(2, 2, 2), 0);
		model->addComponent(renderM);
		model->addComponent(renderC);

		t.position = pos;
		t.scale = glm::vec3(1.0f, 1.0f, 1.0f);
		t.rotation = Rotation();
		transforms.push_back(t);

		t.position = glm::vec3(0, 0, 0);
		t.scale = glm::vec3(0.03f, 0.03f, 0.03f);
		t.rotation = Rotation(-90, Axis::X_AXIS);
		transforms.push_back(t);


		model->sendMessage(&Engine::SetTransformMessage(0, &transforms));
		break;
	}
	case ModelType::CHAIR:
	{
		model = cena.addNewObject();
		this->setPosition(pos);
		meshFac = ModelFactory::Get();
		modelo = meshFac->createModel(type);
		renderC = new ModelComponent(modelo);
		mesh->addPrimitive(q);
		MiniMapRenderComponent* renderM = new MiniMapRenderComponent(mesh, glm::vec3(1, 1, 1), glm::vec3(2, 2, 2), 0);
		model->addComponent(renderM);
		model->addComponent(renderC);



		/*
*/
		t.position = pos;
		t.scale = glm::vec3(0.05f, 0.05f, 0.05f);
		t.rotation = Rotation();
		transforms.push_back(t);

		t.position = glm::vec3(0, 0, 0);
		t.scale = glm::vec3(1.0f, 1.0f, 1.0f);
		t.rotation = Rotation(-90, Axis::X_AXIS);
		transforms.push_back(t);

		t.position = glm::vec3(0, 0, 0);
		t.scale = glm::vec3(1.0f, 1.0f, 1.0f);
		t.rotation = Rotation(45, Axis::Z_AXIS);
		transforms.push_back(t);

		model->sendMessage(&Engine::SetTransformMessage(0, &transforms));
		break;
	}
	}
	setObject(model);

}



Obstacle::~Obstacle(){}


void Obstacle::update(Input &i, float delta){


}
