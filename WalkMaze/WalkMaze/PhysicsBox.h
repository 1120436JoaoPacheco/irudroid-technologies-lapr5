#pragma once

#include "glm\glm.hpp"
#include "glm\gtx\rotate_vector.hpp"
#include "glm\gtx\transform.hpp"
#include "OBBCollision.h"
class PhysicsBox
{
public:
	PhysicsBox(glm::vec3 pos, glm::vec3 scale);
	PhysicsBox(glm::vec3 scale);
	PhysicsBox();
	PhysicsBox(const PhysicsBox&);

	void updatePos(glm::vec3 pos);	
	void setPos() ;

	bool checkCollision(PhysicsBox a);

	glm::vec3 getPos() const{ return _pos; }
	glm::vec3 getScale() const{ return _scale; }

	void setScale(glm::vec3 scale) 
	{ 
		scale = _scale; 
		box.extent[0] = scale.x;
		box.extent[1] = scale.y;
		box.extent[2] = scale.z;
	}
	void rotate(float);
	float getRotation() { return angle; }
	bool testCollision(const PhysicsBox& a) const;

	bool isDirty() {	return dirty; }
private:
	gte::OrientedBox3<float> box;
	glm::vec3 _pos, _tempPos;
	glm::vec3 _scale;
	glm::vec3 _rotation;
	float angle;
	bool dirty;
};