#pragma once
#include "Scene.h"
#include "glm\vec3.hpp"
#include <list>
#include <vector>
#include "Scene.h"
#include "Graph.h"
#include "Input.h"
#include "Player.h"
#include "ModelComponent.h"
#include "ModelFactory.h"
#include "Texture.h"
#include "GameObject.h"
using namespace std;

/**
*  This class represents an object Door child of GameObject.
*  A Door has two types: Connection or Exit.
* @see GameObject
*/
namespace Logic{

	enum class DoorType{
		Connection, Exit
	};

	class Door : public GameObject{

	private:
		int id;
		int nextRoom;
		int nextDoor;
		DoorType type;
		Mesh* mexer;
		Object* obj;
		RenderComponent* compo;
		bool black;
	public:
		Door();
		Door(int id,int nextR,int nextD, DoorType);
		Door(int id, DoorType);
		~Door();
		int getId() const;
		int getNextRoom() const;
		int getNextDoor() const;
		DoorType getDoorType() const;
		void setNextRoom(int);
		void setDoorType(DoorType);

		void update(Input&, float delta);
		/**
		*  Draw a door in the Scene.
		* @see Scene
		*/
		void drawDoor(Engine::Scene *s);

		/**
		*  The animation of the door is created when the player approaches.
		* 
		*/
		void changeblack();
		void changedoor();
	};
}