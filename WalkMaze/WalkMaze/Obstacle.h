#pragma once
#include "glm\vec3.hpp"
#include "Model.h"
#include "ModelFactory.h"
#include "ModelComponent.h"
#include "GameObject.h"
#include "Scene.h"
#include "MiniMapRenderComponent.h"
#include "QuadPrimitive.h"
#include "GameObject.h"
/**
*  This class represents an object Obstacle, child of GameObject.
*
*/

namespace Logic{
	class Obstacle : public GameObject {

	private:
		int idObstacle;
		
		Engine::ModelType* modelType;
		std::vector<Engine::Transform> transforms; 

	public:
		Obstacle(int s, Engine::Scene& cena,Engine::ModelType& modeltype,glm::vec3 pos);
		~Obstacle();


		virtual void setMeshRotation(float rot_y) {
			GameObject::setMeshRotation(Engine::Rotation(rot_y, Engine::Axis::Z_AXIS));
			transforms[1].rotation = Engine::Rotation(rot_y, Engine::Axis::Z_AXIS);

		}
		
		void update(Input&, float delta);

	};
}