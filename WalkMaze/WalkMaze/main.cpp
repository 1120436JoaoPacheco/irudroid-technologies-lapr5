#include <map>
#include <cstdio>
#include <chrono>
#include <thread>
#include <vector>
#include <ctime>
#include "GL\glew.h"
#include "GLFW\glfw3.h"
#include "SOIL.h"
#include "Scene.h"
#include "glm\glm.hpp"
#include "RenderComponent.h"
#include "CubeMeshFactory.h"
#include "Graph.h"
#include "Game.h"
#include "Music.h"
#include "ModelFactory.h"
#include "ModelComponent.h"
#include "Path.h"
#include "WorldsMenu.h"
#include "LoginMenu.h"
#include "Services.h"
#include "ChangeRoom.h"
#include "Download.h"
#define SONG_TIME	21
#define BORING		10

Object* ca;
glm::vec3 rotation(0, 0, 0);

bool windowsHasFocus = true;
using namespace Logic;
using namespace std;

float width, height;


inline int getTime()
{
	return static_cast<int>(static_cast<double>(clock()) / CLOCKS_PER_SEC);
}

int init_resources(void)
{

	glEnable(GL_SMOOTH); /*enable smooth shading */
	glEnable(GL_LIGHTING); /* enable lighting */
	glEnable(GL_DEPTH_TEST); /* enable z buffer */
	glEnable(GL_NORMALIZE);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	//glEnable(GL_LIGHT1);
	//glEnable(GL_LIGHT3);
	//glEnable(GL_LIGHT4);
	//glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glDepthFunc(GL_LESS);

	glEnable(GL_TEXTURE);
	glEnable(GL_TEXTURE_2D);
	GLfloat global_ambient[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	//glLightModelfv(GL_LIGHT_MODEL_AMBIENT, global_ambient);

	bool   gp;                      // G Pressed? ( New )
	GLuint filter;                      // Which Filter To Use
	GLuint fogMode[] = { GL_EXP, GL_EXP2, GL_LINEAR };   // Storage For Three Types Of Fog
	GLuint fogfilter = 1;                    // Which Fog To Use
	GLfloat fogColor[4] = { 0.5f, 0.5f, 0.5f, 1.0f };      // Fog Color
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);          // We'll Clear To The Color Of The Fog ( Modified )

	//glEnable(GL_FOG);                   // Enables GL_FOG

	return 1;
}

void window_size_callback(GLFWwindow* window, int w, int h)
{
	width = w;
	height = h;
}

void mouseEnter_callback(GLFWwindow* window, int entered)
{
	if (entered)
	{
		int width, height;
		glfwGetWindowSize(window, &width, &height);

		double centerx, centery;
		centerx = (double)width / 2;
		centery = (double)height / 2;

		glfwSetCursorPos(window, centerx, centery);
	}
}

void windowsFocus_callback(GLFWwindow* window, int focus)
{
	windowsHasFocus = (bool)focus;
}

int main(int argc, char* argv[])
{

	Download download;
	download.downloadAll();

	Music m;



	GLFWwindow* window;

	if (!glfwInit())
	{
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 1); // We want OpenGL 1
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	window = glfwCreateWindow(640, 480, "Walk the Maze", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	glfwSetInputMode(window, GLFW_STICKY_KEYS, 1);

	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		/* Problem: glewInit failed, something is seriously wrong. */
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
	}

	window_size_callback(window, 640, 480);


	Logic::Game game;
	//PARA TESTAR SEM INTERNET/SEM MENU, DESCOMENTAR ABAIXO
	//pugi::xml_document doc;

	//std::ifstream ifs("MAPA2.xml");
	//std::string content((std::istreambuf_iterator<char>(ifs)),
	//	(std::istreambuf_iterator<char>()));

	//doc.load_string(content.c_str());

	//game.setMap(new Map(doc));
	game.setGameStatus(GameStatus::LoginMenu);
	// FIM PARTE PRA DESCOMENTAR

	init_resources();

	//CALLBACKS
	glfwSetWindowSizeCallback(window, window_size_callback);
	glfwSetCursorEnterCallback(window, mouseEnter_callback);
	glfwSetWindowFocusCallback(window, windowsFocus_callback);
	glfwSetCursorPos(window, 640 / 2.0f, 480 / 2.0f);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

	float delta = 0;

	bool cresce = true;
	float escala = 1;
	float deltaS = 0;
	deltaS = (float)glfwGetTime();

	int time, initialtime = getTime();
	int timesound, initimesound = getTime();
	m.start(argc, argv, 1);

	Path p;
	bool replay = false;
	int j = 0;
	Logic::Path path;
	Logic::Input input;
	vector<Door*> listaportas;
	Logic::LoginMenu loginMenu;
	Logic::WorldsMenu worldsMenu;
	vec3 playerPosition;
	ChangeRoom* changeroom = new ChangeRoom();

	std::chrono::high_resolution_clock::time_point now, last = std::chrono::high_resolution_clock::now();
	std::chrono::duration<float> elapsed, lag = std::chrono::duration<float>(0), MS_PER_UPDATE = std::chrono::duration<float>(std::chrono::milliseconds(16));

	while (!glfwWindowShouldClose(window))
	{
		now = std::chrono::high_resolution_clock::now();
		elapsed = std::chrono::duration_cast<std::chrono::duration<float>>(now - last);
		

		//cout << std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count() << endl;
		delta = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
		deltaS = delta = delta / 1000;

		//cout << delta << endl;
		//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		if (windowsHasFocus)
		{

			if (game.getGameStatus() == GameStatus::LoginMenu)
			{
				loginMenu.start(window);
				bool loginSuccessful = loginMenu.process(input);
				if (loginSuccessful) {
					game.setGameStatus(GameStatus::WorldsMenu);
					loginMenu.end();
				}
			}

			if (game.getGameStatus() == GameStatus::WorldsMenu)
			{
				worldsMenu.start(window);
				std::string mapa = worldsMenu.process(input);
				if (mapa != "")
				{
					game.setGameStatus(GameStatus::Playing);
					worldsMenu.end();

					pugi::xml_document docXML;
					docXML.load_string(mapa.c_str());
					game.setMap(new Map(docXML));

					playerPosition = game.getMap()->getActiveRoom()->getPlayer()->getPosition();

				}
			}

			if (game.getGameStatus() == GameStatus::Playing)
			{
				// Coloquem isto tudo num m�todo :)
				//efeito audio se nao se mexer por + de 5 segundos
				timesound = getTime() - initimesound;
				if (playerPosition != game.getMap()->getActiveRoom()->getPlayer()->getPosition()){
					playerPosition = game.getMap()->getActiveRoom()->getPlayer()->getPosition();
					initimesound = getTime();
				}
				else{
					if (timesound > BORING){
						initimesound = getTime();
						m.parado();
					}
				}

				time = getTime() - initialtime;
				if (time >= SONG_TIME){
					initialtime = getTime();

					m.start(argc, argv, 1);
				}
				listaportas = game.getMap()->getActiveRoom()->getDoorlist();
				//verifica se o homer chegou a uma porta, caso tenha chegado muda de quarto
				if (changeroom->check(&game, &listaportas, &input, delta)){
					//efeito audio
					m.porta();
				}

			}

			if (game.getGameStatus() == GameStatus::Playing)
			{
				// Por isto num m�todo.
				//GRAVAR PERCURSO

				glm::vec3 posantcam;

				if (input.getKeyPressed(Key::ExecutePath) && p.getSize() > j && !replay)
				{
					Camera* cam = &(game.getMap()->getActiveRoom()->getPlayer()->getCamera());
					posantcam = cam->getPosition();

					game.reset(game.getMap()->getActiveRoom()->getPlayer());
					replay = true;

					string teclas = input.key_to_String(p.getTeclas());

					Services s;
					//				s.uploadPercurso(username,strmap,teclas);
				}
				else if (p.getSize() <= j && replay){


					Camera* cam = &(game.getMap()->getActiveRoom()->getPlayer()->getCamera());
					cam->setPosition(posantcam);

					replay = false;
				}
				if (replay)
				{
					game.update(p.getTeclas()[j], deltaS);
					j++;
				}
				else
				{
					j = 0;
					input.update(window, true, deltaS);
					game.update(input, deltaS);
					game.renderGame(width, height);
					
					p.addKey(input);
				}


			}



		}
		glfwSwapBuffers(window);
		glfwSetTime(0);

		glfwPollEvents();
		last = now;


		if (delta < 16)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds((int)(16 - delta)));
		}
	}

	glfwDestroyWindow(window);
	glfwTerminate();
	std::exit(EXIT_SUCCESS);
}