#pragma once

#define _USE_MATH_DEFINES


#include "Node.h"
#include "Edge.h"
#include "pugixml.hpp"
#include "Scene.h"
#include "CubeMeshFactory.h"
#include "RenderComponent.h"
#include "SetTransformMessage.h"
#include <math.h>
#include <vector>
#include <map>
#include "glm\geometric.hpp"
#include <string.h>
#include "Mesh.h"
#include "QuadPrimitive.h"
#include "TextureFactory.h"
#include "Wall.h"
#include "CylinderComponent.h"
using namespace std;

/**
*  Draws a Graph in a given Scene
*  @see Edge
*  @See Node
*/
namespace Logic{
	const float K_CIRCLE = 2.1f;
	const float K_CONNECTION = 1.1f;
	const int num_segmentos = 30;
	const float infintesimo = 0.001f;

	class Graph{

	private:
		vector<Node*> nodelist;
		vector<Edge*> edgelist;
		
	public:
		Graph();
		~Graph();
		void addNode(Node *);
		void deleteNode(int);
		void printNodes();
		void addEdge(Edge *);
		void deleteEdge(int);
		void listEdges();
		/**
		*  Receive the graph using a XML File.
		*
		*/
		void leGrafo(pugi::xml_node*);

		vector<Node*> getNodelist() const;
		vector<Edge*> getEdgelist() const;
		
		/**
		*  Draws a Node in a given Scene
		*
		*  @param nodei Node*, which is the Node to be drawn
		*  @param sc Scene, which is the Scene where the node will be drawn
		*
		*/
		Wall* drawNode(Node*, Scene&, Texture);

		/**
		*  Draws an Edge in a given Scene
		*
		*  @param edge Edge*, which is the Edge to be drawn
		*  @param sc Scene, which is the Scene where the edge will be drawn
		*
		*/
		Wall* drawEdge(Edge*, Scene&,Texture);

		/**
		*  Draws a Graph in a given Scene
		*
		*  @param sc Scene, which is the Scene where the graph will be drawn
		*
		*/
		void drawGraph(Scene&, std::vector<GameObject*>& boxes);
	};
}
