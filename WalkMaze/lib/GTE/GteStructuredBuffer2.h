// Geometric Tools LLC, Redmond WA 98052
// Copyright (c) 1998-2015
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
// File Version: 1.0.1 (2014/12/13)

#pragma once

#include "GteStructuredBuffer.h"

namespace gte
{

template <typename T>
class StructuredBuffer2 : public StructuredBuffer
{
public:
    // Construction.
    StructuredBuffer2(unsigned int width, unsigned int height,
        bool createStorage = true);

    // Member access.
    inline unsigned int GetWidth() const;
    inline unsigned int GetHeight() const;

    // The elements are accessed as if they are in row-major order; that is,
    // i = x + width*y.  No range checking is performed by the operators.
    inline T& operator[](unsigned int i);
    inline T const& operator[](unsigned int i) const;
    inline T& operator()(unsigned int x, unsigned int y);
    inline T const& operator()(unsigned int x, unsigned int y) const;

private:
    unsigned int mWidth, mHeight;
};

//----------------------------------------------------------------------------
template <typename T>
StructuredBuffer2<T>::StructuredBuffer2(unsigned int width,
    unsigned int height, bool createStorage)
    :
    StructuredBuffer(width*height, sizeof(T), createStorage),
    mWidth(width),
    mHeight(height)
{
}
//----------------------------------------------------------------------------
template <typename T> inline
unsigned int StructuredBuffer2<T>::GetWidth() const
{
    return mWidth;
}
//----------------------------------------------------------------------------
template <typename T> inline
unsigned int StructuredBuffer2<T>::GetHeight() const
{
    return mHeight;
}
//----------------------------------------------------------------------------
template <typename T> inline
T& StructuredBuffer2<T>::operator[](unsigned int i)
{
    return Get<T>()[i];
}
//----------------------------------------------------------------------------
template <typename T> inline
T const& StructuredBuffer2<T>::operator[](unsigned int i) const
{
    return Get<T>()[i];
}
//----------------------------------------------------------------------------
template <typename T> inline
T& StructuredBuffer2<T>::operator()(unsigned int x, unsigned int y)
{
    return Get<T>()[x + mWidth*y];
}
//----------------------------------------------------------------------------
template <typename T> inline
T const& StructuredBuffer2<T>::operator()(unsigned int x, unsigned int y) const
{
    return Get<T>()[x + mWidth*y];
}
//----------------------------------------------------------------------------

}
