#include "Services.h"
#include "soapSoapWebServiceSoapProxy.h" 
#include "SoapWebServiceSoap.nsmap" 

Services::Services(){}

Services::~Services(){}

bool Services::login(string username, string password){

	SoapWebServiceSoapProxy service;
	bool result = false;
	
	if (service.login(username, password, result) != SOAP_OK){
		service.soap_stream_fault(std::cerr);
	}

	service.destroy();

	return result;
}

bool Services::uploadPercurso(string username, string nomeMapa, string teclas){

	SoapWebServiceSoapProxy service;
	bool result = false;

	if (service.upercurso(username, nomeMapa, teclas, result) != SOAP_OK){
		service.soap_stream_fault(std::cerr);
	}

	service.destroy();

	return result;
}

bool Services::score(string username, string nomemapa, int pontos){

	SoapWebServiceSoapProxy service;
	bool result = false;

	if (service.pontuacao(username, nomemapa, pontos, result) != SOAP_OK){
		service.soap_stream_fault(std::cerr);
	}

	service.destroy();

	return result;
}

bool Services::downmaps(string nomemapa, string& result){

	SoapWebServiceSoapProxy service;
	bool ret = false;

	if (service.mapas(nomemapa, result) == SOAP_OK){
		if (result!=string("ERRO")){
			ret = true;
		}
	}
	else{
		service.soap_stream_fault(std::cerr);
	}

	service.destroy();

	return ret;
}