#pragma once
#include <iostream>
using namespace std;


/**
*  This class represents an object Edge that will represent an edge in a graph.
*  @see Graph
*
*/
class Services{

public:
	Services();
	~Services();
	

	bool login(string,string);
	bool score(string, string,int);
	bool downmaps(string, string&);
	bool uploadPercurso(string, string, string);
};



