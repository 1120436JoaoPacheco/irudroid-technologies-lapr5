/* soapSoapWebServiceSoapProxy.cpp
   Generated by gSOAP 2.8.21 from server.h

Copyright(C) 2000-2014, Robert van Engelen, Genivia Inc. All Rights Reserved.
The generated code is released under one of the following licenses:
GPL or Genivia's license for commercial use.
This program is released under the GPL with the additional exemption that
compiling, linking, and/or using OpenSSL is allowed.
*/

#include "soapSoapWebServiceSoapProxy.h"

SoapWebServiceSoapProxy::SoapWebServiceSoapProxy()
{	SoapWebServiceSoapProxy_init(SOAP_IO_DEFAULT, SOAP_IO_DEFAULT);
}

SoapWebServiceSoapProxy::SoapWebServiceSoapProxy(const struct soap &_soap) : soap(_soap)
{ }

SoapWebServiceSoapProxy::SoapWebServiceSoapProxy(const char *url)
{	SoapWebServiceSoapProxy_init(SOAP_IO_DEFAULT, SOAP_IO_DEFAULT);
	soap_endpoint = url;
}

SoapWebServiceSoapProxy::SoapWebServiceSoapProxy(soap_mode iomode)
{	SoapWebServiceSoapProxy_init(iomode, iomode);
}

SoapWebServiceSoapProxy::SoapWebServiceSoapProxy(const char *url, soap_mode iomode)
{	SoapWebServiceSoapProxy_init(iomode, iomode);
	soap_endpoint = url;
}

SoapWebServiceSoapProxy::SoapWebServiceSoapProxy(soap_mode imode, soap_mode omode)
{	SoapWebServiceSoapProxy_init(imode, omode);
}

SoapWebServiceSoapProxy::~SoapWebServiceSoapProxy()
{ }

void SoapWebServiceSoapProxy::SoapWebServiceSoapProxy_init(soap_mode imode, soap_mode omode)
{	soap_imode(this, imode);
	soap_omode(this, omode);
	soap_endpoint = NULL;
	static const struct Namespace namespaces[] =
{
	{"SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/", "http://www.w3.org/*/soap-envelope", NULL},
	{"SOAP-ENC", "http://schemas.xmlsoap.org/soap/encoding/", "http://www.w3.org/*/soap-encoding", NULL},
	{"xsi", "http://www.w3.org/2001/XMLSchema-instance", "http://www.w3.org/*/XMLSchema-instance", NULL},
	{"xsd", "http://www.w3.org/2001/XMLSchema", "http://www.w3.org/*/XMLSchema", NULL},
	{"ns1", "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/", NULL, NULL},
	{NULL, NULL, NULL, NULL}
};
	soap_set_namespaces(this, namespaces);
}

void SoapWebServiceSoapProxy::destroy()
{	soap_destroy(this);
	soap_end(this);
}

void SoapWebServiceSoapProxy::reset()
{	destroy();
	soap_done(this);
	soap_initialize(this);
	SoapWebServiceSoapProxy_init(SOAP_IO_DEFAULT, SOAP_IO_DEFAULT);
}

void SoapWebServiceSoapProxy::soap_noheader()
{	this->header = NULL;
}

const SOAP_ENV__Header *SoapWebServiceSoapProxy::soap_header()
{	return this->header;
}

const SOAP_ENV__Fault *SoapWebServiceSoapProxy::soap_fault()
{	return this->fault;
}

const char *SoapWebServiceSoapProxy::soap_fault_string()
{	return *soap_faultstring(this);
}

const char *SoapWebServiceSoapProxy::soap_fault_detail()
{	return *soap_faultdetail(this);
}

int SoapWebServiceSoapProxy::soap_close_socket()
{	return soap_closesock(this);
}

int SoapWebServiceSoapProxy::soap_force_close_socket()
{	return soap_force_closesock(this);
}

void SoapWebServiceSoapProxy::soap_print_fault(FILE *fd)
{	::soap_print_fault(this, fd);
}

#ifndef WITH_LEAN
#ifndef WITH_COMPAT
void SoapWebServiceSoapProxy::soap_stream_fault(std::ostream& os)
{	::soap_stream_fault(this, os);
}
#endif

char *SoapWebServiceSoapProxy::soap_sprint_fault(char *buf, size_t len)
{	return ::soap_sprint_fault(this, buf, len);
}
#endif

int SoapWebServiceSoapProxy::login(const char *endpoint, const char *soap_action, std::string _username, std::string _password, bool &_return_)
{	struct soap *soap = this;
	struct ns1__login soap_tmp_ns1__login;
	struct ns1__loginResponse *soap_tmp_ns1__loginResponse;
	if (endpoint)
		soap_endpoint = endpoint;
	if (soap_endpoint == NULL)
		soap_endpoint = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/api.php";
	if (soap_action == NULL)
		soap_action = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/login";
	soap_begin(soap);
	soap->encodingStyle = "http://schemas.xmlsoap.org/soap/encoding/";
	soap_tmp_ns1__login._username = _username;
	soap_tmp_ns1__login._password = _password;
	soap_serializeheader(soap);
	soap_serialize_ns1__login(soap, &soap_tmp_ns1__login);
	if (soap_begin_count(soap))
		return soap->error;
	if (soap->mode & SOAP_IO_LENGTH)
	{	if (soap_envelope_begin_out(soap)
		 || soap_putheader(soap)
		 || soap_body_begin_out(soap)
		 || soap_put_ns1__login(soap, &soap_tmp_ns1__login, "ns1:login", NULL)
		 || soap_body_end_out(soap)
		 || soap_envelope_end_out(soap))
			 return soap->error;
	}
	if (soap_end_count(soap))
		return soap->error;
	if (soap_connect(soap, soap_url(soap, soap_endpoint, NULL), soap_action)
	 || soap_envelope_begin_out(soap)
	 || soap_putheader(soap)
	 || soap_body_begin_out(soap)
	 || soap_put_ns1__login(soap, &soap_tmp_ns1__login, "ns1:login", NULL)
	 || soap_body_end_out(soap)
	 || soap_envelope_end_out(soap)
	 || soap_end_send(soap))
		return soap_closesock(soap);
	if (!&_return_)
		return soap_closesock(soap);
	soap_default_bool(soap, &_return_);
	if (soap_begin_recv(soap)
	 || soap_envelope_begin_in(soap)
	 || soap_recv_header(soap)
	 || soap_body_begin_in(soap))
		return soap_closesock(soap);
	if (soap_recv_fault(soap, 1))
		return soap->error;
	soap_tmp_ns1__loginResponse = soap_get_ns1__loginResponse(soap, NULL, "", NULL);
	if (!soap_tmp_ns1__loginResponse || soap->error)
		return soap_recv_fault(soap, 0);
	if (soap_body_end_in(soap)
	 || soap_envelope_end_in(soap)
	 || soap_end_recv(soap))
		return soap_closesock(soap);
	_return_ = soap_tmp_ns1__loginResponse->_return_;
	return soap_closesock(soap);
}

int SoapWebServiceSoapProxy::pontuacao(const char *endpoint, const char *soap_action, std::string _username, std::string _nomeMapa, int _pontos, bool &_return_)
{	struct soap *soap = this;
	struct ns1__pontuacao soap_tmp_ns1__pontuacao;
	struct ns1__pontuacaoResponse *soap_tmp_ns1__pontuacaoResponse;
	if (endpoint)
		soap_endpoint = endpoint;
	if (soap_endpoint == NULL)
		soap_endpoint = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/api.php";
	if (soap_action == NULL)
		soap_action = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/pontuacao";
	soap_begin(soap);
	soap->encodingStyle = "http://schemas.xmlsoap.org/soap/encoding/";
	soap_tmp_ns1__pontuacao._username = _username;
	soap_tmp_ns1__pontuacao._nomeMapa = _nomeMapa;
	soap_tmp_ns1__pontuacao._pontos = _pontos;
	soap_serializeheader(soap);
	soap_serialize_ns1__pontuacao(soap, &soap_tmp_ns1__pontuacao);
	if (soap_begin_count(soap))
		return soap->error;
	if (soap->mode & SOAP_IO_LENGTH)
	{	if (soap_envelope_begin_out(soap)
		 || soap_putheader(soap)
		 || soap_body_begin_out(soap)
		 || soap_put_ns1__pontuacao(soap, &soap_tmp_ns1__pontuacao, "ns1:pontuacao", NULL)
		 || soap_body_end_out(soap)
		 || soap_envelope_end_out(soap))
			 return soap->error;
	}
	if (soap_end_count(soap))
		return soap->error;
	if (soap_connect(soap, soap_url(soap, soap_endpoint, NULL), soap_action)
	 || soap_envelope_begin_out(soap)
	 || soap_putheader(soap)
	 || soap_body_begin_out(soap)
	 || soap_put_ns1__pontuacao(soap, &soap_tmp_ns1__pontuacao, "ns1:pontuacao", NULL)
	 || soap_body_end_out(soap)
	 || soap_envelope_end_out(soap)
	 || soap_end_send(soap))
		return soap_closesock(soap);
	if (!&_return_)
		return soap_closesock(soap);
	soap_default_bool(soap, &_return_);
	if (soap_begin_recv(soap)
	 || soap_envelope_begin_in(soap)
	 || soap_recv_header(soap)
	 || soap_body_begin_in(soap))
		return soap_closesock(soap);
	if (soap_recv_fault(soap, 1))
		return soap->error;
	soap_tmp_ns1__pontuacaoResponse = soap_get_ns1__pontuacaoResponse(soap, NULL, "", NULL);
	if (!soap_tmp_ns1__pontuacaoResponse || soap->error)
		return soap_recv_fault(soap, 0);
	if (soap_body_end_in(soap)
	 || soap_envelope_end_in(soap)
	 || soap_end_recv(soap))
		return soap_closesock(soap);
	_return_ = soap_tmp_ns1__pontuacaoResponse->_return_;
	return soap_closesock(soap);
}

int SoapWebServiceSoapProxy::mapas(const char *endpoint, const char *soap_action, std::string nomeMapa, std::string &return_)
{	struct soap *soap = this;
	struct ns1__mapas soap_tmp_ns1__mapas;
	struct ns1__mapasResponse *soap_tmp_ns1__mapasResponse;
	if (endpoint)
		soap_endpoint = endpoint;
	if (soap_endpoint == NULL)
		soap_endpoint = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/api.php";
	if (soap_action == NULL)
		soap_action = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/mapas";
	soap_begin(soap);
	soap->encodingStyle = "http://schemas.xmlsoap.org/soap/encoding/";
	soap_tmp_ns1__mapas.nomeMapa = nomeMapa;
	soap_serializeheader(soap);
	soap_serialize_ns1__mapas(soap, &soap_tmp_ns1__mapas);
	if (soap_begin_count(soap))
		return soap->error;
	if (soap->mode & SOAP_IO_LENGTH)
	{	if (soap_envelope_begin_out(soap)
		 || soap_putheader(soap)
		 || soap_body_begin_out(soap)
		 || soap_put_ns1__mapas(soap, &soap_tmp_ns1__mapas, "ns1:mapas", NULL)
		 || soap_body_end_out(soap)
		 || soap_envelope_end_out(soap))
			 return soap->error;
	}
	if (soap_end_count(soap))
		return soap->error;
	if (soap_connect(soap, soap_url(soap, soap_endpoint, NULL), soap_action)
	 || soap_envelope_begin_out(soap)
	 || soap_putheader(soap)
	 || soap_body_begin_out(soap)
	 || soap_put_ns1__mapas(soap, &soap_tmp_ns1__mapas, "ns1:mapas", NULL)
	 || soap_body_end_out(soap)
	 || soap_envelope_end_out(soap)
	 || soap_end_send(soap))
		return soap_closesock(soap);
	if (!&return_)
		return soap_closesock(soap);
	soap_default_std__string(soap, &return_);
	if (soap_begin_recv(soap)
	 || soap_envelope_begin_in(soap)
	 || soap_recv_header(soap)
	 || soap_body_begin_in(soap))
		return soap_closesock(soap);
	if (soap_recv_fault(soap, 1))
		return soap->error;
	soap_tmp_ns1__mapasResponse = soap_get_ns1__mapasResponse(soap, NULL, "", NULL);
	if (!soap_tmp_ns1__mapasResponse || soap->error)
		return soap_recv_fault(soap, 0);
	if (soap_body_end_in(soap)
	 || soap_envelope_end_in(soap)
	 || soap_end_recv(soap))
		return soap_closesock(soap);
	return_ = soap_tmp_ns1__mapasResponse->return_;
	return soap_closesock(soap);
}

int SoapWebServiceSoapProxy::upercurso(const char *endpoint, const char *soap_action, std::string _username, std::string _nomeMapa, std::string _teclas, bool &_return_)
{	struct soap *soap = this;
	struct ns1__upercurso soap_tmp_ns1__upercurso;
	struct ns1__upercursoResponse *soap_tmp_ns1__upercursoResponse;
	if (endpoint)
		soap_endpoint = endpoint;
	if (soap_endpoint == NULL)
		soap_endpoint = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/api.php";
	if (soap_action == NULL)
		soap_action = "http://uvm019.dei.isep.ipp.pt/~asist19/LAPR5/upercurso";
	soap_begin(soap);
	soap->encodingStyle = "http://schemas.xmlsoap.org/soap/encoding/";
	soap_tmp_ns1__upercurso._username = _username;
	soap_tmp_ns1__upercurso._nomeMapa = _nomeMapa;
	soap_tmp_ns1__upercurso._teclas = _teclas;
	soap_serializeheader(soap);
	soap_serialize_ns1__upercurso(soap, &soap_tmp_ns1__upercurso);
	if (soap_begin_count(soap))
		return soap->error;
	if (soap->mode & SOAP_IO_LENGTH)
	{	if (soap_envelope_begin_out(soap)
		 || soap_putheader(soap)
		 || soap_body_begin_out(soap)
		 || soap_put_ns1__upercurso(soap, &soap_tmp_ns1__upercurso, "ns1:upercurso", NULL)
		 || soap_body_end_out(soap)
		 || soap_envelope_end_out(soap))
			 return soap->error;
	}
	if (soap_end_count(soap))
		return soap->error;
	if (soap_connect(soap, soap_url(soap, soap_endpoint, NULL), soap_action)
	 || soap_envelope_begin_out(soap)
	 || soap_putheader(soap)
	 || soap_body_begin_out(soap)
	 || soap_put_ns1__upercurso(soap, &soap_tmp_ns1__upercurso, "ns1:upercurso", NULL)
	 || soap_body_end_out(soap)
	 || soap_envelope_end_out(soap)
	 || soap_end_send(soap))
		return soap_closesock(soap);
	if (!&_return_)
		return soap_closesock(soap);
	soap_default_bool(soap, &_return_);
	if (soap_begin_recv(soap)
	 || soap_envelope_begin_in(soap)
	 || soap_recv_header(soap)
	 || soap_body_begin_in(soap))
		return soap_closesock(soap);
	if (soap_recv_fault(soap, 1))
		return soap->error;
	soap_tmp_ns1__upercursoResponse = soap_get_ns1__upercursoResponse(soap, NULL, "", NULL);
	if (!soap_tmp_ns1__upercursoResponse || soap->error)
		return soap_recv_fault(soap, 0);
	if (soap_body_end_in(soap)
	 || soap_envelope_end_in(soap)
	 || soap_end_recv(soap))
		return soap_closesock(soap);
	_return_ = soap_tmp_ns1__upercursoResponse->_return_;
	return soap_closesock(soap);
}
/* End of client proxy code */
