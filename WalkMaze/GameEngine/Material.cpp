#include "Material.h"
#include "glm\vec4.hpp"

using namespace Engine;


Material::Material() : gl_ambient(0.2, 0.2, 0.2, 1.0), gl_diffuse(0.8, 0.8, 0.8, 1.0), gl_specular(0.0, 0.0, 0.0, 1.0),
gl_shininess(0.0), gl_emission(0.0, 0.0, 0.0, 1.0)
{

}

Material::Material(glm::vec4& _gl_ambient, glm::vec4& _gl_diffuse, glm::vec4& _gl_specular)
	:gl_ambient(_gl_ambient), gl_diffuse(_gl_diffuse), gl_specular(_gl_specular),
	gl_shininess(0.0), gl_emission(0.0, 0.0, 0.0, 1.0)
{

}

Material::Material(glm::vec4& gl_ambient, glm::vec4& gl_diffuse,
	glm::vec4& gl_specular, float gl_shininess, glm::vec4& gl_emission)
	: gl_ambient(gl_ambient),gl_diffuse(gl_diffuse),gl_specular(gl_specular),gl_shininess(gl_shininess),gl_emission(gl_emission)
	
{

}



//GETS

glm::vec4 Material::getGl_ambient()
{
	return gl_ambient;
}
 
glm::vec4 Material::getGl_diffuse()
{
	return  gl_diffuse;
}

glm::vec4 Material::getGl_specular()
{
	return gl_specular;
}

float Material::getGl_shininess()
{
	return gl_shininess;
}

glm::vec4 Material::getGl_emission()
{
	return gl_emission;
}

//SETS

void Material::setGl_ambient(glm::vec4& gl_ambient)
{
	this->gl_ambient = gl_ambient;
}

void Material::setGl_diffuse(glm::vec4 &gl_diffuse)
{
	this->gl_diffuse = gl_diffuse;
}

void Material::setGl_specular(glm::vec4 &gl_specular)
{
	this->gl_specular = gl_specular;
}

void Material::setGl_shininess(float gl_shininess)
{
	this->gl_shininess=gl_shininess;
}

void Material::setGl_emission(glm::vec4 &gl_emission)
{
	this->gl_emission = gl_emission;
}