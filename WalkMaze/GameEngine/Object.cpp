#include "Object.h"
using namespace Engine;

Object::Object(int id) : id(id)
{
	TransformComponent * t = new  TransformComponent();
	addComponent(t);
}


Object::~Object()
{
	for (Component *c : components)
	{
		delete c;
	}
}

bool Object::sendMessage(Message * msg)
{
	bool messageHandled = false;
	//Handle the messages we want
	switch (msg->getMessageType())
	{
	default:
		break;
	}
	//If the message wasnt for the object but for the components 
	//return true
	messageHandled |= passMessageToComponents(msg);

	return messageHandled;
}

bool Object::passMessageToComponents(Message * msg)
{
	bool messageHandled = false;

	auto it = components.begin();

	for (; it != components.end(); ++it)
		messageHandled |= (*it)->sendMessage(msg);
	

	return messageHandled;
}
