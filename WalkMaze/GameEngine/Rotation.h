#pragma once

#include "glm\vec3.hpp"
#include "glm\vec4.hpp"
#include "glm\gtx\rotate_vector.hpp"

namespace Engine
{

	enum class Axis
	{
		X_AXIS,
		Y_AXIS,
		Z_AXIS
	};

	/**
	*   A class that represents a rotation
	*/
	class Rotation
	{
	public:

		Rotation() : angle(0), axis(Axis::X_AXIS) {}

		/**
		*  Creates the rotation with the angle and the axis
		*  @param angle the rotation angle
		*  @param axis the axis where the rotation is done
		*/
		Rotation(const float& angle, const Axis& axis) : angle(angle), axis(axis)
		{
		}

		Rotation(const Rotation& r) : angle(r.angle), axis(r.axis)
		{
			
		}

		~Rotation()
		{
		}

		/**
		*  Gets the rotation axis
		*  @return the rotation axis
		*/
		Axis getRotationAxis() const { return axis; }

		/**
		*  Return the rotation angle
		*  @param the rotation angle
		*/
		float getAngle()  const { return angle; }

		/**
		*  Returns the rotation in a vector with 4 elements.
		*  The first element in the vector is the angle and the other three are the rotation axis
		*  @return the rotation vector
		*/
		glm::vec4 getRotationV4() const
		{
			switch (axis)
			{
			case Engine::Axis::X_AXIS:
				return glm::vec4(angle, 1, 0, 0);
				break;
			case Engine::Axis::Y_AXIS:
				return glm::vec4(angle, 0, 1, 0);
				break;
			case Engine::Axis::Z_AXIS:
				return glm::vec4(angle, 0, 0, 1);
				break;
			}
			return glm::vec4();
		}

		/**
		*  Rotates the given vector
		*  @return a vector rotated with the set rotation
		*/
		glm::vec3 rotateVector(const glm::vec3& v) 
		{
			switch (axis)
			{
			case Engine::Axis::X_AXIS:
				return glm::rotateX(v, angle);
				break;
			case Engine::Axis::Y_AXIS:
				return glm::rotateY(v, angle);
				break;
			case Engine::Axis::Z_AXIS:
				return glm::rotateZ(v, angle);
				break;
			}
			return glm::vec3();
		}

		/**
		*  Set the axis
		*  @param axis the axis to be setted
		*/
		void setAxis(const Axis &axis) { this->axis = axis; }

		/**
		*  Sets the rotation angle
		*  @param angle the angle
		*/
		void setAngle(const float& angle) { this->angle = angle; }

	private:
		Axis axis;
		float angle;
	};

}