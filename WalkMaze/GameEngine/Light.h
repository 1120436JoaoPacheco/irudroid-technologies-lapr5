#pragma once
#include "Component.h"
#include "Message.h"
#include "glm\vec3.hpp"
#include "glm\vec4.hpp"
#include <stdlib.h>
#include <gl\glew.h>

namespace Engine
{
	/**
	* Light can be define by 5 atributes 
	*/

	class Light:
		public Component
	{

	public:
		/**
		*  Create a object with 0, 4 or 5 arguments
		*  @return a object with a specific id
		*/
		Light();		
		Light(GLenum gl_light, glm::vec4& gl_ambient, glm::vec4& gl_diffuse, glm::vec4& gl_specular, glm::vec4& gl_position,
			glm::vec3& gl_spot_direction, GLfloat& gl_spot_cutoff, GLfloat& spot_exponent, GLfloat& gl_quadratic_attenuation,
			GLfloat& gl_linear_attenuation);

		Light(GLenum gl_light, glm::vec4& gl_ambient, glm::vec4& gl_diffuse, glm::vec4& gl_specular, glm::vec4& gl_position,
			GLfloat& gl_quadratic_attenuation,
			GLfloat& gl_linear_attenuation, glm::vec3& gl_spot_direction);
		Light(const Light &otherText);

		GLfloat getGl_quadratic_attenuation();
		GLfloat getGl_linear_attenuation();
		GLenum getGl_light();
		GLfloat getGl_spot_exponent();
		GLfloat getGl_spot_cutoff();
		/**
		*  Gets the gl_ambient of the object 
		*  @return the gl_ambient of the destination
		*/
		glm::vec4 getGl_ambient();
		/**
		*  Gets the Gl_diffuse of the object 
		*  @return the Gl_diffuse of the destination
		*/
		glm::vec4 getGl_diffuse();
		/**
		*  Gets the Gl_specular of the object 
		*  @return the Gl_specular of the destination
		*/
		glm::vec4 getGl_specular();
		/**
		*  Gets the Gl_position of the object 
		*  @return the Gl_position of the destination
		*/
		glm::vec4 getGl_position();
		/**
		*  Gets the Gl_spot_direction of the object 
		*  @return the Gl_spot_direction of the destination
		*/
		glm::vec3 getGl_spot_direction();



		/**
		*  Set Gl_ambient
		*/
		void setGl_ambient(glm::vec4 &gl_ambient);
		/**
		*  Set Gl_diffuse
		*/
		void setGl_diffuse(glm::vec4 &gl_diffuse);
		/**
		*  Set Gl_specular
		*/
		void setGl_specular(glm::vec4 &gl_specular);
		/**
		*  Set Gl_position
		*/
		void setGl_position(glm::vec4 &gl_position);
		/**
		*  Set Gl_spot_direction
		*/
		void setGl_spot_direction(glm::vec3 &gl_spot_direction);
		void setGl_spot_cutoff(GLfloat &gl_spot_cutoff);
		void setGl_spot_exponent(GLfloat &gl_spot_exponent);
		void setGl_light(GLenum &gl_light);
		void setGl_linear_attenuation(GLfloat &gl_linear_attenuation);
		void setGl_quadratic_attenuation(GLfloat &gl_quadratic_attenuation);
	private:
		GLenum gl_light;
		GLfloat gl_spot_exponent;
		GLfloat gl_spot_cutoff;
		GLfloat gl_linear_attenuation;
		GLfloat gl_quadratic_attenuation;
		/**
		*  ambient RGBA
		*  intensity of light
		*/
		glm::vec4 gl_ambient;

		/**
		* diffuse RGBA intensity
		* of light
		*/
		glm::vec4 gl_diffuse;

		/**
		* specular RGBA
		* intensity of light
		*/
		glm::vec4 gl_specular;

		/**
		* (x, y, z, w) position of
		* light
		*/
		glm::vec4 gl_position;

		/**
		*(x, y, z) direction of
		*spotlight
		*/
		glm::vec3 gl_spot_direction;

	};

}