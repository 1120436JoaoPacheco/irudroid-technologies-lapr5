#include "TextureFactory.h"

using namespace Engine;




Texture TextureFactory::createTexture(std::string s)
{

	GLuint tex_2d = SOIL_load_OGL_texture
		(
		s.c_str(),
		SOIL_LOAD_AUTO,
		SOIL_CREATE_NEW_ID,
		SOIL_FLAG_MIPMAPS  | SOIL_FLAG_INVERT_Y| SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
		);

	return Texture(s, tex_2d);
}

