#include "RenderLifeBarMessage.h"

using namespace Engine;

RenderLifeBarMessage::RenderLifeBarMessage(int destId, SceneRenderer * sc)
: Message(destId, MessageType::DRAW_LIFE), sc(sc)
{
}


RenderLifeBarMessage::~RenderLifeBarMessage()
{
}
