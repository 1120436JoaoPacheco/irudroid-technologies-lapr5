#pragma once
#include "PrimitiveMesh.h"
#include "glm\glm.hpp"
namespace Engine
{
	/**
	*  Represents a Quad Primitive
	*/
	class QuadPrimitive : public PrimitiveMesh
	{
	public:
		/**
		*	Creates a Quad with length 1 centered at origin
		*/
		QuadPrimitive();

		/**
		*  Creates a Quad with the give vertices
		*  lT left Top vertice
		*  @rT right Top Vertice
		*  @lB left bottom Vertice
		*  @rB right bottom Vertice
		*/
		QuadPrimitive(glm::vec3& lT, glm::vec3& rT, glm::vec3 lB, glm::vec3& rB);

		QuadPrimitive(bool front, glm::vec3& lT, glm::vec3& rT, glm::vec3 lB, glm::vec3& rB, glm::vec3& uVlT, glm::vec3& uVrT, glm::vec3 uVlB, glm::vec3& uVrB);

		void subdivide();

		QuadPrimitive(const QuadPrimitive& q);
		~QuadPrimitive();
	};

}