#include "PrimitiveMesh.h"

using namespace Engine;

PrimitiveMesh::PrimitiveMesh(PrimitiveType type) : type(type)
{

}

PrimitiveMesh::PrimitiveMesh(const PrimitiveMesh& p) : type(p.type)
{
	for (glm::vec3* v : p.meshInfo)
	{
		meshInfo.push_back(new glm::vec3(*v));
	}
}

PrimitiveMesh::~PrimitiveMesh()
{
	for (glm::vec3* v : meshInfo)
	{
		delete v;
	}
}


std::vector<glm::vec3*> PrimitiveMesh::getVertexList()
{
	return meshInfo;
}