#pragma once
#include "Component.h"
#include "Message.h"
#include "glm\vec4.hpp"
#include <stdlib.h>
#include <gl\glew.h>

namespace Engine
{

	class Material :
		public Component
	{

	public :
		Material();
		Material(glm::vec4& gl_ambient, glm::vec4& gl_diffuse, glm::vec4& gl_specular);
		Material(glm::vec4& gl_ambient, glm::vec4& gl_diffuse, glm::vec4& gl_specular, float gl_shininess, glm::vec4& gl_emission);

		glm::vec4 getGl_ambient();
		glm::vec4 getGl_diffuse();
		glm::vec4 getGl_specular();
		float getGl_shininess();
		glm::vec4 getGl_emission();

		void setGl_ambient(glm::vec4 &gl_ambient);
		void setGl_diffuse(glm::vec4 &gl_diffuse);
		void setGl_specular(glm::vec4 &gl_specular);
		void setGl_shininess(float gl_shininess);
		void setGl_emission(glm::vec4 &gl_emission);		

	private:
		glm::vec4 gl_ambient;
		glm::vec4 gl_diffuse;
		glm::vec4 gl_specular;
		float gl_shininess;
		glm::vec4 gl_emission;
	};

}