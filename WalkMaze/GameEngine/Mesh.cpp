#include "Mesh.h"

using namespace Engine;

Mesh::Mesh(bool store) : store(store), generated(false), texture(), material()
{
}

Mesh::Mesh(const Mesh& m)
{
	for (PrimitiveMesh* v : m.primitiveList)
	{
		primitiveList.push_back(new PrimitiveMesh(*v));
	}
}

Mesh::~Mesh()
{
	for (PrimitiveMesh* permitive : primitiveList)
	{
		delete permitive;
	}
}

void Mesh::addPrimitive(PrimitiveMesh const &  v)
{
	PrimitiveMesh* newVec = new PrimitiveMesh(v);
	primitiveList.push_back(newVec);
}


std::vector<PrimitiveMesh*> Mesh::getPrimitives()
{
	return primitiveList;
}