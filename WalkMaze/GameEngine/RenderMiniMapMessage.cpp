#include "RenderMiniMapMessage.h"

using namespace Engine;

RenderMiniMapMessage::RenderMiniMapMessage(int destId, SceneRenderer * sc)
: Message(destId, MessageType::DRAW_MINI_MAP), sc(sc)
{
}


RenderMiniMapMessage::~RenderMiniMapMessage()
{
}
