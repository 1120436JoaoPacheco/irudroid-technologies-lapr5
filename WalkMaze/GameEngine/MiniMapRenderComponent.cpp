#include "MiniMapRenderComponent.h"

using namespace Engine;

MiniMapRenderComponent::MiniMapRenderComponent(Mesh* m, glm::vec3 col, glm::vec3 scale,float rot) : mesh(m), color(col), scale(scale), rot(rot)
{

}

MiniMapRenderComponent::MiniMapRenderComponent(const MiniMapRenderComponent& c)
: Component(c), mesh(c.mesh)
{

}
bool MiniMapRenderComponent::sendMessage(Message* msg)
{
	assert(msg != NULL);
	switch (msg->getMessageType())
	{
	case MessageType::DRAW_MINI_MAP:
	{
		RenderMiniMapMessage * renderMiniMapMessage = static_cast<RenderMiniMapMessage*>(msg);
		
		render(renderMiniMapMessage->getSceneRenderer(), mesh);
		break;
	}
	default:
		return Component::sendMessage(msg);
	}

	return false;
}

void MiniMapRenderComponent::render(SceneRenderer * sceneRenderer, Mesh* mesh)
{

	sceneRenderer->renderMinimapMesh(mesh, color, scale,rot);

}