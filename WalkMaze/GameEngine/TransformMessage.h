#pragma once
#include "Message.h"
#include <glm\vec3.hpp>
#include "Rotation.h"
#include <vector>
#include "Transform.h"

namespace Engine
{

	class TransformMessage : public Message
	{
	protected:
		TransformMessage(int destID, MessageType typeMessage);
		
		TransformMessage(int destID, MessageType typeMessage, std::vector<Transform>* Transforms);

		TransformMessage();

	public:

		~TransformMessage();

		std::vector<Transform>* getTransforms() { return Transforms; }


		void setTransform(std::vector<Transform>* p) { Transforms = p; }
	private:
		std::vector<Transform>  *Transforms;
	};
}