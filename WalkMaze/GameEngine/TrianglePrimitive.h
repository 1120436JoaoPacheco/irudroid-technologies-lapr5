#pragma once
#include "PrimitiveMesh.h"

namespace Engine
{
	/**
	* Represents a triangle geometry
	*/
	class TrianglePrimitive : PrimitiveMesh
	{
	public:

		/**
		*  Creates a triangle with size 1 centered at origin
		*/
		TrianglePrimitive();

		/*
		* Create a triangle geometry with the given vertices
		* @param T  Top vertice
		* @param lB left bottom Vertice
		* @param rB right bottom Vertice
		*/
		TrianglePrimitive(glm::vec3& T, glm::vec3& lB, glm::vec3 rB);
		~TrianglePrimitive();
	};

}