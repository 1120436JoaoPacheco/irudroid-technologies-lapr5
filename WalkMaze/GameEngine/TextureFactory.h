#pragma once

#include "Texture.h"
namespace Engine
{

	class TextureFactory
	{
	public:

		
		Texture createTexture(std::string );

		
		static TextureFactory *GetInstance()
		{
			static TextureFactory instance;
			return &instance;
		}


	private:
		TextureFactory(){}

	};

}