#pragma once
#include "Light.h"
#include "Component.h"
#include "Component.h"
#include "glm\vec3.hpp"


namespace Engine
{
	class LightComponent : public Component
	{
	public:
		LightComponent();
		~LightComponent();

		bool LightComponent::sendMessage(Message* msg);
	private:
		Light light;
		
	};
}