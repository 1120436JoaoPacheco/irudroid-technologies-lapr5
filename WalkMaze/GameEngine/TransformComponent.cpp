#include "TransformComponent.h"

using namespace Engine;

TransformComponent::TransformComponent() 
{
	Transforms.push_back(Transform{ glm::vec3(0, 0, 0), glm::vec3(1, 1, 1) });
}


TransformComponent::TransformComponent(const TransformComponent& t)
	: Component(t)
{
	for (Transform t : t.Transforms)
	{
		Transforms.push_back(Transform{ t.position, t.scale, t.rotation });
	}
}

TransformComponent::~TransformComponent()
{	
}


bool TransformComponent::sendMessage(Message* msg)
{

	switch (msg->getMessageType())
	{
	case MessageType::DRAW_MINI_MAP:
	case MessageType::SET_TO_DRAW:
	case MessageType::DRAW:
	{
		RenderMessage * renderMessage = static_cast<RenderMessage*>(msg);
		renderMessage->getSceneRenderer()->setRenderTransformContext(&Transforms);
		break;
	}
	case MessageType::SET_Transform:
	{
		SetTransformMessage * setTransform = static_cast<SetTransformMessage*>(msg);
		if (setTransform->getTransforms() != NULL)
		{
			Transforms.clear();
			for (Transform t : *setTransform->getTransforms())
			{
				Transforms.push_back(Transform{ t.position, t.scale, t.rotation });
			}
		}

		break;
	}
	case MessageType::GET_Transform:
	{
		GetTransformMessage * getTransform = static_cast<GetTransformMessage*>(msg);
		getTransform->setTransform(&Transforms);
		
		break;
	}
	default:
		return Component::sendMessage(msg);
	}

	return false;
}