#pragma once
#include "Message.h"
#include "SceneRenderer.h"

namespace Engine
{
	/**
	*  A message that causes the object or the components to render 
	*  @see Message
	*  @see Component
	*  @see SceneRenderer
	*/
	class RenderMessage : public Message
	{
	public:
		/**
		*  @param destID the id of the object to render
		*  @param renderer the renderer of the scene
		*/
		RenderMessage(int destID, SceneRenderer* renderer);
		~RenderMessage();

		/**
		*  Gets the scene render
		*  @see SceneRenderer
		*/
		SceneRenderer* getSceneRenderer(){ return sceneRenderer; }

		//Todo
		//RenderTarget getRenderTarget();
	private:
		SceneRenderer * sceneRenderer;
	};

}