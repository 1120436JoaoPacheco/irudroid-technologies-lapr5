#pragma once
#include "Message.h"
#include "Light.h"
#include "glm\vec3.hpp"
namespace Engine
{
	class SetLightMessage : public Message
	{
	public:
		SetLightMessage(int id, Light* light);
		~SetLightMessage();

		Light* getLight() { return light; }

	private:
		Light* light;
	};
}
