#include "QuadPrimitive.h"

using namespace Engine;

#define LT glm::vec3(-0.5f, 0.5f, 0.0f)
#define LB glm::vec3(-0.5f, -0.5f, 0.0f)
#define RB glm::vec3(0.5f, -0.5f, 0.0f)
#define RT glm::vec3(0.5f, 0.5f, 0.0f)

glm::vec3 normal(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3)
{
	glm::vec3 v(p2 - p1);
	glm::vec3 u(p3 - p1);

	glm::vec3 normal;
	normal.x = (u.y * v.z) - (u.z * v.y);
	normal.y = (u.z * v.x) - (u.x * v.z);
	normal.z = (u.x * v.y) - (u.y * v.x);

	return glm::normalize(normal);
}

glm::vec3 midPoint(glm::vec3 a, glm::vec3 b)
{
	return glm::vec3((a.x + b.x) / 2.0f, (a.y + b.y) / 2.0f, (a.z + b.z) / 2.0f);
}


QuadPrimitive::QuadPrimitive() : PrimitiveMesh(PrimitiveType::QUAD)
{
	//First Triangle
	meshInfo.push_back(new glm::vec3(normal(RB, LB, LT)));
	meshInfo.push_back(new glm::vec3(1, 0, 0));

	meshInfo.push_back(new RB);

	meshInfo.push_back(new glm::vec3(0, 0, 0));
	meshInfo.push_back(new LB);

	meshInfo.push_back(new glm::vec3(0, 1, 0));
	meshInfo.push_back(new LT);




	//Second Triangle
	meshInfo.push_back(new glm::vec3(normal(RB, LT, RT)));
	meshInfo.push_back(new glm::vec3(1, 0, 0));

	meshInfo.push_back(new RB);

	meshInfo.push_back(new glm::vec3(0, 1, 0));
	meshInfo.push_back(new LT);

	meshInfo.push_back(new glm::vec3(1, 1, 0));

	meshInfo.push_back(new RT);

}


QuadPrimitive::QuadPrimitive(glm::vec3& lT, glm::vec3& rT, glm::vec3 lB, glm::vec3& rB) : PrimitiveMesh(PrimitiveType::QUAD)
{
	//glm::vec3 mid = midPoint(midPoint(lT, rB), midPoint(lB, rT));


	////First Triangle
	//meshInfo.push_back(new glm::vec3(normal(lT, mid, rT)));
	//meshInfo.push_back(new glm::vec3(lT));
	//meshInfo.push_back(new glm::vec3(mid));
	//meshInfo.push_back(new glm::vec3(rT));

	////Second Triangle
	//meshInfo.push_back(new glm::vec3(normal(lT, lB, mid)));
	//meshInfo.push_back(new glm::vec3(lT));
	//meshInfo.push_back(new glm::vec3(lB));
	//meshInfo.push_back(new glm::vec3(mid));

	////third
	//meshInfo.push_back(new glm::vec3(normal(mid, lB, rB)));
	//meshInfo.push_back(new glm::vec3(mid));
	//meshInfo.push_back(new glm::vec3(lB));
	//meshInfo.push_back(new glm::vec3(rB));



	////fourth
	//meshInfo.push_back(new glm::vec3(normal(rT, mid, rB)));
	//meshInfo.push_back(new glm::vec3(rT));
	//meshInfo.push_back(new glm::vec3(mid));
	//meshInfo.push_back(new glm::vec3(rB));

	//First Triangle
	meshInfo.push_back(new glm::vec3(normal(rB, lB, lT)));
	meshInfo.push_back(new glm::vec3(1, 0, 0));

	meshInfo.push_back(new glm::vec3(rB));

	meshInfo.push_back(new glm::vec3(0, 0, 0));
	meshInfo.push_back(new glm::vec3(lB));

	meshInfo.push_back(new glm::vec3(0, 1, 0));
	meshInfo.push_back(new glm::vec3(lT));




	//Second Triangle
	meshInfo.push_back(new glm::vec3(normal(rB, lT, rT)));
	meshInfo.push_back(new glm::vec3(1, 0, 0));

	meshInfo.push_back(new glm::vec3(rB));

	meshInfo.push_back(new glm::vec3(0, 1, 0));
	meshInfo.push_back(new glm::vec3(lT));

	meshInfo.push_back(new glm::vec3(1, 1, 0));

	meshInfo.push_back(new glm::vec3(rT));


}

QuadPrimitive::QuadPrimitive(bool front, glm::vec3& lT, glm::vec3& rT, glm::vec3 lB, glm::vec3& rB, glm::vec3& uVlT, glm::vec3& uVrT, glm::vec3 uVlB, glm::vec3& uVrB) : PrimitiveMesh(PrimitiveType::QUAD)
{
	//glm::vec3 mid = midPoint(midPoint(lT, rB), midPoint(lB, rT));


	////First Triangle
	//meshInfo.push_back(new glm::vec3(normal(lT, mid, rT)));
	//meshInfo.push_back(new glm::vec3(lT));
	//meshInfo.push_back(new glm::vec3(mid));
	//meshInfo.push_back(new glm::vec3(rT));

	////Second Triangle
	//meshInfo.push_back(new glm::vec3(normal(lT, lB, mid)));
	//meshInfo.push_back(new glm::vec3(lT));
	//meshInfo.push_back(new glm::vec3(lB));
	//meshInfo.push_back(new glm::vec3(mid));

	////third
	//meshInfo.push_back(new glm::vec3(normal(mid, lB, rB)));
	//meshInfo.push_back(new glm::vec3(mid));
	//meshInfo.push_back(new glm::vec3(lB));
	//meshInfo.push_back(new glm::vec3(rB));



	////fourth
	//meshInfo.push_back(new glm::vec3(normal(rT, mid, rB)));
	//meshInfo.push_back(new glm::vec3(rT));
	//meshInfo.push_back(new glm::vec3(mid));
	//meshInfo.push_back(new glm::vec3(rB));

	if (front)
	{
		//First Triangle
		meshInfo.push_back(new glm::vec3(normal(rT, lB, lT)));

		meshInfo.push_back(new glm::vec3(uVrB));
		meshInfo.push_back(new glm::vec3(rB));

		meshInfo.push_back(new glm::vec3(uVlB));
		meshInfo.push_back(new glm::vec3(lB));


		meshInfo.push_back(new glm::vec3(uVlT));
		meshInfo.push_back(new glm::vec3(lT));




		//Second Triangle
		meshInfo.push_back(new glm::vec3(normal(rB, lT, rT)));


		meshInfo.push_back(new glm::vec3(uVrB));
		meshInfo.push_back(new glm::vec3(rB));

		meshInfo.push_back(new glm::vec3(uVlT));
		meshInfo.push_back(new glm::vec3(lT));

		meshInfo.push_back(new glm::vec3(uVrT));
		meshInfo.push_back(new glm::vec3(rT));

	}
	else
	{
		//First Triangle
		meshInfo.push_back(new glm::vec3(normal(lT, lB, rB)));

		meshInfo.push_back(new glm::vec3(uVlT));
		meshInfo.push_back(new glm::vec3(lT));

		meshInfo.push_back(new glm::vec3(uVlB));
		meshInfo.push_back(new glm::vec3(lB));

		meshInfo.push_back(new glm::vec3(uVrB));
		meshInfo.push_back(new glm::vec3(rB));



		//Second Triangle
		meshInfo.push_back(new glm::vec3(normal(rT, lT, rB)));

		meshInfo.push_back(new glm::vec3(uVrT));
		meshInfo.push_back(new glm::vec3(rT));

		meshInfo.push_back(new glm::vec3(uVlT));
		meshInfo.push_back(new glm::vec3(lT));

		meshInfo.push_back(new glm::vec3(uVrB));
		meshInfo.push_back(new glm::vec3(rB));


	}

}

void QuadPrimitive::subdivide()
{
	std::vector<glm::vec3*> oldInfo = meshInfo;
	meshInfo.clear();

	glm::vec3 mid1, mid2, mid3;
	glm::vec3 a, b, c;
	glm::vec3 uva, uvb, uvc;
	glm::vec3 uvMid1, uvMid2, uvMid3;
	int size = oldInfo.size();

	for (int i = 0; i < size; i += 7)
	{
		uva = *oldInfo[i + 1];
		a = *oldInfo[i + 2];
		uvb = *oldInfo[i + 3];
		b = *oldInfo[i + 4];
		uvc = *oldInfo[i + 5];
		c = *oldInfo[i + 6];


		mid1 = midPoint(a, b);
		mid2 = midPoint(b, c);
		mid3 = midPoint(c, a);

		uvMid1 = midPoint(uva, uvb);
		uvMid2 = midPoint(uvb, uvc);
		uvMid3 = midPoint(uvc, uva);

		//First Triangle
		meshInfo.push_back(new glm::vec3(normal(a, mid1, mid3)));
		
		meshInfo.push_back(new glm::vec3(uva));
		meshInfo.push_back(new glm::vec3(a));
		meshInfo.push_back(new glm::vec3(uvMid1));
		meshInfo.push_back(new glm::vec3(mid1));
		meshInfo.push_back(new glm::vec3(uvMid3));
		meshInfo.push_back(new glm::vec3(mid3));

		//Second Triangle
		meshInfo.push_back(new glm::vec3(normal(mid1, b, mid2)));
		meshInfo.push_back(new glm::vec3(uvMid1));
		meshInfo.push_back(new glm::vec3(mid1));
		meshInfo.push_back(new glm::vec3(uvb));
		meshInfo.push_back(new glm::vec3(b));
		meshInfo.push_back(new glm::vec3(uvMid2));
		meshInfo.push_back(new glm::vec3(mid2));

		//Third Triangle
		meshInfo.push_back(new glm::vec3(normal(mid2, c, mid3)));
		meshInfo.push_back(new glm::vec3(uvMid2));
		meshInfo.push_back(new glm::vec3(mid2));
		meshInfo.push_back(new glm::vec3(uvc));
		meshInfo.push_back(new glm::vec3(c));
		meshInfo.push_back(new glm::vec3(uvMid3));
		meshInfo.push_back(new glm::vec3(mid3));

		//Fourth Triangle
		meshInfo.push_back(new glm::vec3(normal(mid1, mid2, mid3)));
		meshInfo.push_back(new glm::vec3(uvMid1));
		meshInfo.push_back(new glm::vec3(mid1));
		meshInfo.push_back(new glm::vec3(uvMid2));
		meshInfo.push_back(new glm::vec3(mid2));
		meshInfo.push_back(new glm::vec3(uvMid3));
		meshInfo.push_back(new glm::vec3(mid3));
	}

	for (glm::vec3 * v : oldInfo)
	{
		delete v;
	}

}

QuadPrimitive::QuadPrimitive(const QuadPrimitive& p) : PrimitiveMesh(p)
{

}

QuadPrimitive::~QuadPrimitive()
{

}
