#include "TransformMessage.h"

using namespace Engine;

TransformMessage::TransformMessage(int destID, MessageType type)
	: Message(destID, type)
{

}

TransformMessage::TransformMessage(int destID, MessageType type, std::vector<Transform>* Transforms)
	: Message(destID, type), Transforms(Transforms)
{

}

TransformMessage::~TransformMessage()
{

}
