#pragma once
#include "Message.h"

namespace Engine
{
	class SequenceMessage : public Message
	{
	public:
		SequenceMessage(int id, int numSeq);
		~SequenceMessage();

	
		int getNumSequence(){ return numSequence; }

	private:
		int numSequence;
	};

}