#include "SetTransformMessage.h"

using namespace Engine;

SetTransformMessage::SetTransformMessage(int id, std::vector<Transform>* t)
	: TransformMessage(id, MessageType::SET_Transform, t)
{

}


SetTransformMessage::~SetTransformMessage()
{

}
