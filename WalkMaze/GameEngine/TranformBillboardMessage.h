#pragma once
#include "Message.h"
#include "SceneRenderer.h"

namespace Engine{

	class TranformBillboardMessage : public Message
	{

	public:

		TranformBillboardMessage(int destId, glm::vec3 size, glm::vec3 color);
		~TranformBillboardMessage();

		glm::vec3  getSize() { return size; }
		glm::vec3  getColor() { return color; }

	private:
		glm::vec3  size;
		glm::vec3  color;
	};


}