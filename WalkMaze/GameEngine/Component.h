#pragma once

#include "Message.h"
namespace Engine
{
	/**
	*  A component represents a behaviour of o object.
	*  All the behaviours are triggred by a message.
	*  @see Object
	*  @see Message
	*/
	class Component
	{
	public:
		Component() {}
		Component(const Component& c) {}
		
		/**
		* Sends a message to the component
		* @param m the message to send
		* @return true if the message was handled by the component false if not
		* @see Message
		*/
		virtual bool sendMessage(Message* msg) { return false; }
	};
}