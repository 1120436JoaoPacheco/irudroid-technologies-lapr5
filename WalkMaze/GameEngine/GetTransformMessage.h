#pragma once
#include "TransformMessage.h"

namespace Engine
{
	class GetTransformMessage :
		public TransformMessage
	{
	public:
		GetTransformMessage(int destId);
		virtual ~GetTransformMessage();

	}; 
}
