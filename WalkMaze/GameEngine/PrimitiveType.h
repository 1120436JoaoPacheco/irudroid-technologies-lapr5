#pragma once


enum class PrimitiveType
{
	QUAD,
	TRIANGLE
};