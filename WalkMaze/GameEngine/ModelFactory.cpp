#include "ModelFactory.h"

using namespace Engine;

Model* ModelFactory::createModel(ModelType mt)
{
	
	if (mt==ModelType::PLAYER){
		return new Model("daffy.mdl");
	}
	if (mt == ModelType::SHELVE){
		return new Model("shelves_wood.mdl");
	}
	if (mt == ModelType::STATUE){
		return new Model("statue2.mdl");
	}
	if (mt == ModelType::CRATE){
		return new Model("crate.mdl");
	}
	if (mt == ModelType::WARDROBE){
		return new Model("wardrobe.mdl");
	}
	if (mt == ModelType::WRECKEDTABLE){
		return new Model("wrecked_table.mdl");
	}
	if (mt == ModelType::BARREL){
		return new Model("barrel.mdl");
	}
	if (mt == ModelType::PLANT){
		return new Model("plant.mdl");
	}
	if (mt == ModelType::PIANO){
		return new Model("piano.mdl");
	}
	if (mt == ModelType::CHAIR){
		return new Model("chair.mdl");
	}
	if (mt == ModelType::ENEMY_RAPTOR){
		return new Model("raptor.mdl");
	}
	if (mt == ModelType::ENEMY_ALIEN){
		return new Model("scream.mdl");
	}
	if (mt == ModelType::BULLET){
		return new Model("rshell.mdl");
	}
	if (mt == ModelType::LIFEBONUS){
		return new Model("icecream.mdl");
	}
	if (mt == ModelType::POINTBONUS){
		return new Model("star.mdl");
	}
	return NULL;
}