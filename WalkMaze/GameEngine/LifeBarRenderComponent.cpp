#include "LifeBarRenderComponent.h"

using namespace Engine;

LifeBarRenderComponent::LifeBarRenderComponent(glm::vec3 size, glm::vec3 color) : size(size), color(color)
{

}

bool LifeBarRenderComponent::sendMessage(Message* msg)
{
	assert(msg != NULL);
	switch (msg->getMessageType())
	{
	case MessageType::DRAW_LIFE:
	{
							  RenderLifeBarMessage * renderLifeBarMessage = static_cast<RenderLifeBarMessage*>(msg);
							  render(renderLifeBarMessage->getSceneRenderer());
									   break;
	}
	case MessageType::TANSFORM_LIFE:
	{
									   TranformLifeBarMessage * tranformLifeBarMessage = static_cast<TranformLifeBarMessage*>(msg);
									   size = tranformLifeBarMessage->getSize();
									   color = tranformLifeBarMessage->getColor();
									   render(new SceneRenderer());
									   break;
	}
	default:
		return Component::sendMessage(msg);
	}

	return false;
}

void LifeBarRenderComponent::render(SceneRenderer * sceneRenderer)
{
	sceneRenderer->renderLifeBarMesh(size, color);
}