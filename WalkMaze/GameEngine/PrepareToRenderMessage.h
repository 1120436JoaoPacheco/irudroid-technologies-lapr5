#pragma once
#include "Message.h"
#include "SceneRenderer.h"

namespace Engine
{
	class PrepareToRenderMessage : public Message
	{
	public:
		PrepareToRenderMessage(int destId, SceneRenderer* sc);
		~PrepareToRenderMessage();


		SceneRenderer* getSceneRenderer() { return sc; }

	private:
		SceneRenderer* sc;
	};

}
