#pragma once
#include "Message.h"
#include "DayTime.h"

namespace Engine
{
class DayTimeMessage :
	public Message
{
public:
	DayTimeMessage(int id, DayTime);
	~DayTimeMessage();

	DayTime getDayTime() { return dt; }

private:
	DayTime dt;
};

}
