#include "SetLightMessage.h"

using namespace Engine;

SetLightMessage::SetLightMessage(int id, Light* light)
	: Message(id, MessageType::SET_LIGHT), light(light)
{

}


SetLightMessage::~SetLightMessage()
{
}
