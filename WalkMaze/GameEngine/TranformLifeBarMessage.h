#pragma once
#include "glm\glm.hpp"
#include "Message.h"

namespace Engine{

	class TranformLifeBarMessage : public Message
	{

	public:

		TranformLifeBarMessage(int destId, glm::vec3, glm::vec3);
		~TranformLifeBarMessage();

		glm::vec3 getSize() { return size; }
		glm::vec3 getColor() { return color; }

	private:
		glm::vec3 size;
		glm::vec3 color;
	};


}