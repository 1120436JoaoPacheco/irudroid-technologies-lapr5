#pragma once
#include "GL\glew.h"
#include <vector>
#include <glm/vec3.hpp>
#include "Message.h"
#include "TransformComponent.h"
#include <algorithm>
namespace Engine
{
	/**
	*  This class represents a object to be handled by the render.
	*  A object is composed by Components that represent the behaviours of the object
	*  @see Component
	*  @see Message
	*/
	class Object
	{
	public:
		
		/**
		*  Constructs a object 
		*  Every object is constructed with a Transform Component already added
		*  @param id the unique object ID
		*  @see TransformComponent
		*/
		Object(int id);

		/**
		*  The Constructor
		*/
		~Object();

		/**
		*  Gets the id of the object 
		*  @return the object id
		*/
		int getObjectId() const { return id; }

		/**
		*  Adds a component to the object
		*  @param comp a component to add
		*  @see Component
		*/
		void addComponent(Component* comp) { components.push_back(comp); }

		void removeComponent(Component * comp) 
		{ 
			std::vector<Component*>::iterator it;
			if (( it = std::find(components.begin(), components.end(), comp)) != components.end())
			{
				components.erase(it);
			}
		}

		/**
		* Sends a message to the object and all components in the object
		* @param m the message to send
		* @return true if the message was handled by someone false if not
		* @see Message
		*/
		bool sendMessage(Message* m);
	
	private:
		/**
		*  The id of the object
		*/
		int id;

		/**
		*  Passes a message to all internal components of the object
		*  @param m the message to send
		*  @return true if the message was handle false if not
		*  @see Message
		*/
		bool passMessageToComponents(Message* m);

		/**
		*  A list that contains all the components of the object
		*/
		std::vector<Component*> components; 
	};

}