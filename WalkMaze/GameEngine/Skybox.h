#pragma once
#include "RenderComponent.h"
#include "TextureFactory.h"
#include "Scene.h"
#include "QuadPrimitive.h"
#include "DayTimeMessage.h"
namespace Engine
{
	class Skybox :public RenderComponent
	{
	public:
		Skybox();
		~Skybox();

		void initskybox();

		bool sendMessage(Message * msg);
	private:
		Texture night, day;
		Mesh* drawSkybox(float,float);
		void render(SceneRenderer * sceneRenderer, Mesh* mesh);
	};


}