#pragma once
#include "Component.h"
#include "Rotation.h"
#include "RenderMessage.h"
#include "SetTransformMessage.h"
#include "GetTransformMessage.h"
#include "Transform.h"
namespace Engine
{
	/**
	* A component who stores all the fields needed to Transform a object
	* @see Component
	*/
	class TransformComponent : public Component
	{
	public:
		TransformComponent();
		TransformComponent(const TransformComponent& t);
		~TransformComponent();

		bool sendMessage(Message* msg);
	private:

		std::vector<Transform> Transforms;
	};

}