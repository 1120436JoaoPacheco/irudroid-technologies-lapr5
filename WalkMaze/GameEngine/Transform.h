#pragma once

#include "Rotation.h"
#include "glm\vec3.hpp"

namespace Engine
{
	typedef struct
	{
		glm::vec3 position;
		glm::vec3 scale;

		Rotation rotation;
	}Transform;

}
