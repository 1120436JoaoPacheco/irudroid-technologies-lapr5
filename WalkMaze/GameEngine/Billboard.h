#pragma once
#include "glm\vec3.hpp"
#include "RenderComponent.h"
#include "TextureFactory.h"
#include "Scene.h"
#include "QuadPrimitive.h"
#include "DayTimeMessage.h"
#include "TranformBillboardMessage.h"

namespace Engine
{
	class Billboard :public RenderComponent
	{
	public:
		Billboard(glm::vec3 pos, glm::vec3 size, glm::vec3 color);
		~Billboard();

		bool sendMessage(Message * msg);

	private:

		void render(SceneRenderer * sceneRenderer, Mesh* mesh);
		glm::vec3 pos;
		glm::vec3 size;
		glm::vec3 color;
	};


}