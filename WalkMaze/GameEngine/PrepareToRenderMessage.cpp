#include "PrepareToRenderMessage.h"

using namespace Engine;

PrepareToRenderMessage::PrepareToRenderMessage(int destId, SceneRenderer * sc)
	: Message(destId, MessageType::SET_TO_DRAW), sc(sc)
{
}


PrepareToRenderMessage::~PrepareToRenderMessage()
{
}
