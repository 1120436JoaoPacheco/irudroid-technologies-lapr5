#pragma once

#include <vector>
#include "PrimitiveType.h"
#include "glm\vec3.hpp"
namespace Engine
{

	/**
	*  Represents a basic geometry
	*  @see Mesh
	*/
	class PrimitiveMesh
	{
	public:
		PrimitiveMesh(const PrimitiveMesh&);
		~PrimitiveMesh();

		/**
		*  Gets the list os vertices of the geometry
		*  @return the list of vertices
		*/
		virtual std::vector< glm::vec3*> getVertexList();

		/**
		*  Gets the type of the primitive
		*  @return the primitive type
		*/
		PrimitiveType getPrimitiveType() { return type; }
	protected:

		PrimitiveMesh(PrimitiveType type);
		std::vector < glm::vec3* > meshInfo;

	private:
		PrimitiveType type;
	};

}

