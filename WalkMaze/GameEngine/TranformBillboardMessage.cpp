#include "TranformBillboardMessage.h"

using namespace Engine;

TranformBillboardMessage::TranformBillboardMessage(int destId, glm::vec3 size, glm::vec3 color)
: Message(destId, MessageType::Tranform_BB), size(size), color(color)
{
}


TranformBillboardMessage::~TranformBillboardMessage()
{
}
