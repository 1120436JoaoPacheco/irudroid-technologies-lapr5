#pragma once
#include "Message.h"
#include "glm\vec3.hpp"
namespace Engine
{
	class SetFpsCamMessage : public Message
	{
	public:
		SetFpsCamMessage(int id, glm::vec3* pos, float angleX, float angleY);
		~SetFpsCamMessage();
	
		float getAngleX(){ return angleX; }
		float getAngleY(){ return angleY; }
		glm::vec3* getPos() { return pos; }

	private:
		float angleY;
		float angleX;
		glm::vec3* pos;
	};
}
