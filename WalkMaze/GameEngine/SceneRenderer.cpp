#include "SceneRenderer.h"
#include "Light.h"
using namespace Engine;



SceneRenderer::SceneRenderer()
{

}


SceneRenderer::~SceneRenderer()
{

}

void SceneRenderer::renderModel(Model * m)
{
	glPushMatrix();
	for (Transform t : *Transforms){
		glTranslatef(t.position.x, t.position.y, t.position.z);
		glRotatef(t.rotation.getRotationV4()[0], t.rotation.getRotationV4()[1], t.rotation.getRotationV4()[2], t.rotation.getRotationV4()[3]);
		glScalef(t.scale.x, t.scale.y, t.scale.z);
	}
	float a[]{ 1.0f, 1.0f, 1.0f, 1.0f };
	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, a);

	mdlviewer_display(*m->getStudioModel());
	glPopMatrix();
}

void renderMesh(Mesh*& m)
{
	glColor3f(1, 1, 1);
	Texture t = m->getTexture();

	if (t.getTexName() != NULL)
	{
		glBindTexture(GL_TEXTURE_2D, t.getTexName());
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}

	int primitives;
	std::vector<glm::vec3*> info;
	primitives = m->getPrimitives().size();

	for (int i = 0; i < primitives; ++i)
	{

		glBegin(GL_TRIANGLES);

		info = m->getPrimitives()[i]->getVertexList();
		int size = info.size();
		for (int b = 0; b < size; b += 7) // 3 vertices 1 normal + 3 texturas uma pa cada vertice =7
		{
			glNormal3fv((GLfloat*)info[b]);
			glTexCoord2fv((GLfloat*)info[b + 1]);
			glVertex3fv((GLfloat*)info[b + 2]);
			glTexCoord2fv((GLfloat*)info[b + 3]);
			glVertex3fv((GLfloat*)info[b + 4]);
			glTexCoord2fv((GLfloat*)info[b + 5]);
			glVertex3fv((GLfloat*)info[b + 6]);
		}

		glEnd();
	}
	glBindTexture(GL_TEXTURE_2D, NULL);

}

void SceneRenderer::renderInternal(Mesh * m)
{

	if (m->storeInList())
	{
		if (m->listGenerated())
		{
			glCallList(m->getListIndex());
		}
		else
		{
			GLuint list = glGenLists(1);
			glNewList(list, GL_COMPILE);
			renderMesh(m);
			glEndList();
			m->setListIndex(list);
		}
	}
	else
		renderMesh(m);


	glFlush();

}



void SceneRenderer::renderCylinder(GLUquadric* quad,GLdouble base, GLdouble top, GLdouble height, GLint slices, GLint stack, Texture texture)
{
	glDisable(GL_CULL_FACE);
	glPushMatrix();
	//gluQuadricDrawStyle(GLU_LINE);
	if(texture.getTexName() != NULL)
	{
		glBindTexture(GL_TEXTURE_2D, texture.getTexName());
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}
	 
	for (Transform t : *Transforms){
		glTranslatef(t.position.x, t.position.y, t.position.z);
		glRotatef(t.rotation.getRotationV4()[0], t.rotation.getRotationV4()[1], t.rotation.getRotationV4()[2], t.rotation.getRotationV4()[3]);
		glScalef(t.scale.x, t.scale.y, t.scale.z);
	}
	gluQuadricTexture(quad, true);
	gluQuadricOrientation(quad,GLU_OUTSIDE);
	gluCylinder(quad, base, top, height, slices, stack);
	glPopMatrix();
	glPushMatrix();
	for (Transform t : *Transforms){
		glTranslatef(t.position.x, t.position.y , t.position.z);
		glScalef(t.scale.x, t.scale.y, t.scale.z);
	}
	glRotatef(90, 1, 0, 0);
	gluDisk(quad, 0, base, stack, slices);
	glBindTexture(GL_TEXTURE_2D, NULL);
	glPopMatrix();
}
void SceneRenderer::renderMinimapMesh(Mesh * m, glm::vec3& color, glm::vec3& scale, float rot)
{
	glPushMatrix();
	for (Transform t : *Transforms)
	{
		glTranslatef(t.position.x, t.position.z, 0);
	}

	glRotatef(rot, 0, 0, 1);
	glScalef(scale.x, scale.y, scale.z);

	glPushAttrib(GL_COLOR);
	glColor3f(color.r, color.g, color.b);
	renderInternal(m);
	glPopAttrib();
	glPopMatrix();
}
void SceneRenderer::renderLifeBarMesh(glm::vec3 scale, glm::vec3 color)
{

	int tamanho = scale.x * 100;

	glBegin(GL_QUADS);
	glColor4f(color.r, color.g, color.b, 1);
	glVertex2f(0, 0);
	glVertex2f(0, 100);
	glVertex2f(tamanho, 100);
	glVertex2f(tamanho, 0);
	glEnd();
}

static glm::vec3 bar_vert[] = {
	glm::vec3(-0.7f, -0.15f, 0.0f),
	glm::vec3(-0.7f, 0.15f, 0.0f),
	glm::vec3(0.7f, 0.15f, 0.0f),
	glm::vec3(0.7f, -0.15f, 0.0f)

};

void SceneRenderer::renderBillboardMesh(Mesh * m, glm::vec3 position, glm::vec3 scale, glm::vec3 color)
{
	glPushMatrix();

	glm::vec3 rightW = cameraPos * cameraRight;
	glm::vec3 upW = cameraPos * cameraUp;


	glm::vec3 s;
	glColor3f(color.r, color.g, color.b);
	glBegin(GL_QUADS);
	for (glm::vec3 v : bar_vert)
	{
		s = position + cameraRight * v.x * scale.x + cameraUp * v.y  * scale.y;
		glVertex3f(s.x, s.y, s.z);
		//printf("X=%f Y=%f Z=%f\n ", s.x, s.y, s.z);
	}
	glColor3f(1, 1, 1);
	glEnd();
	glFlush();
	//printf("\n");

	glPopMatrix();
}

void SceneRenderer::render(Mesh * m)
{

	glm::vec4 rot;
	glPushMatrix();
	if (Transforms != NULL)
	{
		for (Transform t : *Transforms)
		{
			glTranslatef(t.position.x, t.position.y, t.position.z);
			rot = t.rotation.getRotationV4();
			glRotated(rot[0], rot[1], rot[2], rot[3]);
			glScalef(t.scale.x, t.scale.y, t.scale.z);
		}
	}
	renderInternal(m);

	glPopMatrix();
}


GLfloat mat_specular[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat mat_diffuse[] = { 0.0f, 0.0f, 0.8f, 1.0f };
GLfloat mat_ambient[] = { 0.0f, 0.0f, 0.2f, 1.0f };
GLfloat mat_shininess[] = { 96.0f };
GLfloat light_position1[] = { 0,10, 0, 1.0, 1 };
GLfloat light_position2[] = { 0.0f, -1.0f, 0.0f };
GLfloat light_position3[] = { 75, 10, 25, 1.0 };
GLfloat light_position4[] = { 75, 10, 75, 1.0 };
GLfloat ambientLight[] = { 0.2f, 0.2f, 0.2f, 1.0f };
GLfloat diffuseLight[] = { 0.8f, 0.8f, 0.8f, 1.0f };
GLfloat specularLight[] = { 0.5f, 0.5f, 0.5f, 1.0f };

GLfloat spot_direction[3] = { -1.0, -0.0, -1.0 };


GLfloat cutoff = 45;
GLfloat spot_exponent = 1;
float spin = 0;

void SceneRenderer::clear()
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	//glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_specular);
	//glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mat_diffuse);
	//glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mat_shininess);

}

void SceneRenderer::setCameraToRender(CameraType type, glm::vec3& pos, glm::vec3& direction, glm::vec3& up, glm::vec3& right)
{
	glm::vec3 dir = pos + direction;

	cameraUp = up; cameraDirection = direction; cameraRight = right; cameraPos = pos;


	//printf("DIR X = %f; Y = %f; Z = %f \n", dir.x, dir.y, dir.z);
	//printf("POS X = %f; Y = %f; Z = %f \n", pos.x, pos.y, pos.z);
	//printf("UP X = %f; Y = %f; Z = %f \n", up.x, up.y, up.z);
	gluLookAt(pos.x, pos.y, pos.z,
		dir.x, dir.y, dir.z,
		up.x, up.y, up.z);
	/*
		glm::vec4 lighpos(pos, 1);
		glLightfv(GL_LIGHT0, GL_POSITION, (GLfloat*)&lighpos);
		glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
		glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
		glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, (GLfloat*)&direction);
		glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, cutoff);
		glLightf(GL_LIGHT0, GL_SPOT_EXPONENT, spot_exponent);*/
	/*glm::vec4 lightPos(pos, 1);
	dir = direction;
	glLightfv(GL_LIGHT0, GL_POSITION, (GLfloat*)&lightPos);
	printf("POS X = %f; Y = %f; Z = %f W = %f\n", lightPos.x, lightPos.y, lightPos.z, lightPos.w);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, (GLfloat*)&dir);
	printf("DIR X = %f; Y = %f; Z = %f\n", dir.x, dir.y, dir.z);
	glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, cutoff);
	glLightf(GL_LIGHT0, GL_SPOT_EXPONENT, spot_exponent);*/
	//glLightfv(GL_LIGHT1, GL_POSITION, light_position2);
	//glLightfv(GL_LIGHT1, GL_SPECULAR, specularLight);
	//glLightfv(GL_LIGHT1, GL_AMBIENT, ambientLight);
	//glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuseLight);
	//glLightfv(GL_LIGHT2, GL_POSITION, light_position3);
	//glLightfv(GL_LIGHT2, GL_SPECULAR, specularLight);
	//glLightfv(GL_LIGHT2, GL_AMBIENT, ambientLight);
	//glLightfv(GL_LIGHT2, GL_DIFFUSE, diffuseLight);
	//glLightfv(GL_LIGHT3, GL_POSITION, light_position4);
	//glLightfv(GL_LIGHT3, GL_SPECULAR, specularLight);
	//glLightfv(GL_LIGHT3, GL_AMBIENT, ambientLight);
	//glLightfv(GL_LIGHT3, GL_DIFFUSE, diffuseLight);
}


void SceneRenderer::setLightToRender(Light& light)
{
	
	/*glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light_position2);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position1);
	glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 22);*/

	glLightfv(light.getGl_light(), GL_SPECULAR, (GLfloat*)&light.getGl_specular());
	glLightfv(light.getGl_light(), GL_AMBIENT, (GLfloat*)&light.getGl_ambient());
	glLightfv(light.getGl_light(), GL_DIFFUSE, (GLfloat*)&light.getGl_diffuse());
	glLightfv(light.getGl_light(), GL_POSITION, (GLfloat*)&light.getGl_position());
	glLightfv(light.getGl_light(), GL_SPOT_DIRECTION, (GLfloat*)&light.getGl_spot_direction());
	glLightf(light.getGl_light(),  GL_SPOT_CUTOFF, light.getGl_spot_cutoff());
	glLightf(light.getGl_light(), GL_LINEAR_ATTENUATION, light.getGl_linear_attenuation());
	
	//glLightf(light.getGl_light(), GL_QUADRATIC_ATTENUATION, light.getGl_quadratic_attenuation());
//	glLightf(light.getGl_light(), GL_SPOT_EXPONENT, light.getGl_spot_exponent());
}

 

void SceneRenderer::setRenderTransformContext(std::vector<Transform>* t)
{
	this->Transforms = t;

}
void SceneRenderer::renderQuads(PrimitiveMesh*& m)
{

}


void SceneRenderer::renderTriangles(PrimitiveMesh*& m)
{
	glPushMatrix();

	glBegin(GL_TRIANGLES);

	for (glm::vec3 *v : m->getVertexList())
	{
		glVertex3fv((GLfloat*)v);
	}

	glEnd();
	glPopMatrix();
}