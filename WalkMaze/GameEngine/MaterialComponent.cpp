#include "MaterialComponent.h"


using namespace Engine;


bool MaterialComponent::sendMessage(Message* msg)
{
	switch (msg->getMessageType())
	{
	case MessageType::DRAW:
	{
		glMaterialfv(GL_FRONT, GL_AMBIENT, (GLfloat*)&(this->gl_ambient));
		glMaterialfv(GL_FRONT, GL_DIFFUSE, (GLfloat*)&(this->gl_diffuse));
		glMaterialfv(GL_FRONT, GL_SPECULAR, (GLfloat*)&(this->gl_specular));
		glMaterialfv(GL_FRONT, GL_SHININESS, (GLfloat*)&(this->gl_shininess));
		glMaterialfv(GL_FRONT, GL_EMISSION, (GLfloat*)&(this->gl_emission));

		break;
	}
	default:
		break;
	}
	return true;
}

MaterialComponent::MaterialComponent() : gl_ambient(0.2, 0.2, 0.2, 1.0), gl_diffuse(0.8, 0.8, 0.8, 1.0), gl_specular(0.0, 0.0, 0.0, 1.0),
gl_shininess(0.0), gl_emission(0.0, 0.0, 0.0, 1.0)
{

}

MaterialComponent::MaterialComponent(glm::vec4& _gl_ambient, glm::vec4& _gl_diffuse, glm::vec4& _gl_specular)
	:gl_ambient(_gl_ambient) , gl_diffuse(_gl_diffuse), gl_specular(_gl_specular)
{

}

MaterialComponent::MaterialComponent(glm::vec4& gl_ambient, glm::vec4& gl_diffuse,
	glm::vec4& gl_specular, float gl_shininess, glm::vec4& gl_emission)
	: gl_ambient(gl_ambient),gl_diffuse(gl_diffuse),gl_specular(gl_specular),gl_shininess(gl_shininess),gl_emission(gl_emission)
	
{
	
}