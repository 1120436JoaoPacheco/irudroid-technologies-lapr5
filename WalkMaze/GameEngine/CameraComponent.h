#pragma once
#include "Component.h"
#include "Component.h"
#include "glm\vec3.hpp"
#include "Rotation.h"
#include "RenderMessage.h"
#include "SetCameraMessage.h"
#include "SetFpsCamMessage.h"
#include "SetTpsCamMessage.h"
#include "SetTpvCamMessage.h"
#include "GetCameraDirectionMessage.h"

namespace Engine
{
	class CameraComponent : public Component
	{
	public:
		CameraComponent();
		~CameraComponent();

		bool CameraComponent::sendMessage(Message* msg);
	private:
		float angleY;
		float angleX;
		float radius;
		float dist;
		CameraType type;
		glm::vec3 up, right, target, pos;

		void ortbit(glm::vec3& target, float angleX, float angleY);
		void fps();
		void topview(glm::vec3& target, float dist, float angle);
		void setCameraTarget(glm::vec3);




	};
}
