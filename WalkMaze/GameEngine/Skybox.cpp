#include "Skybox.h"

using namespace Engine;

Skybox::Skybox() : RenderComponent(drawSkybox(200, 200))
{
	day = TextureFactory::GetInstance()->createTexture("sky.png");
	night = TextureFactory::GetInstance()->createTexture("sky.jpg");

	mesh->setTexture(day);

}
Skybox::~Skybox(){}

void Skybox::initskybox()
{
}

Mesh* Skybox::drawSkybox(float height, float widthMap)
{
	Mesh * mesh = new Mesh(false);

	glm::vec3 back1(-widthMap / 2, -height / 2, -widthMap / 2);
	glm::vec3 back2(-widthMap / 2, height / 2, -widthMap / 2);
	glm::vec3 back3(-widthMap / 2, -height / 2, widthMap / 2);
	glm::vec3 back4(-widthMap / 2, height / 2, widthMap / 2);
	QuadPrimitive back(false, back1, back2, back3, back4, glm::vec3(0, 0.333f, 0), glm::vec3(0, 0.666f, 0), glm::vec3(0.25f, 0.333f, 0), glm::vec3(0.25f, 0.666f, 0));
	mesh->addPrimitive(back);

	glm::vec3 front1(widthMap / 2, -height / 2, -widthMap / 2);
	glm::vec3 front2(widthMap / 2, height / 2, -widthMap / 2);
	glm::vec3 front3(widthMap / 2, -height / 2, widthMap / 2);
	glm::vec3 front4(widthMap / 2, height / 2, widthMap / 2);
	QuadPrimitive front(false, front3, front4, front1, front2, glm::vec3(0.5f, 0.333f, 0), glm::vec3(0.5f, 0.666f, 0), glm::vec3(0.75f, 0.333f, 0), glm::vec3(0.75f, 0.666f, 0));
	mesh->addPrimitive(front);


	glm::vec3 left1(-widthMap / 2, -height / 2, -widthMap / 2);
	glm::vec3 left2(-widthMap / 2, height / 2, -widthMap / 2);
	glm::vec3 left3(widthMap / 2, -height / 2, -widthMap / 2);
	glm::vec3 left4(widthMap / 2, height / 2, -widthMap / 2);
	QuadPrimitive left(false, left3, left4, left1, left2, glm::vec3(0.75f, 0.333f, 0), glm::vec3(0.75f, 0.666f, 0), glm::vec3(1, 0.333f, 0), glm::vec3(1, 0.666f, 0));
	mesh->addPrimitive(left);



	glm::vec3 right1(-widthMap / 2, -height / 2, widthMap / 2);
	glm::vec3 right2(-widthMap / 2, height / 2, widthMap / 2);
	glm::vec3 right3(widthMap / 2, -height / 2, widthMap / 2);
	glm::vec3 right4(widthMap / 2, height / 2, widthMap / 2);
	QuadPrimitive right(true, right3, right4, right1, right2, glm::vec3(0.5f, 0.333f, 0), glm::vec3(0.5f, 0.666f, 0), glm::vec3(0.25f, 0.333f, 0), glm::vec3(0.25f, 0.666f, 0));
	mesh->addPrimitive(right);



	glm::vec3 top1(-widthMap / 2, height / 2, -widthMap / 2);
	glm::vec3 top2(widthMap / 2, height / 2, -widthMap / 2);
	glm::vec3 top3(-widthMap / 2, height / 2, widthMap / 2);
	glm::vec3 top4(widthMap / 2, height / 2, widthMap / 2);
	QuadPrimitive top(false, top1, top2, top3, top4, glm::vec3(0.25f, 1, 0), glm::vec3(0.5f, 1, 0), glm::vec3(0.25f, 0.666f, 0), glm::vec3(0.5f, 0.666f, 0));
	mesh->addPrimitive(top);
	//glm::vec3(0.25f, 0.666f, 0), glm::vec3(0.25f, 1, 0), glm::vec3(0.5f, 0.666f, 0), glm::vec3(0.5f, 1, 0)

	
	return mesh;
}


bool Skybox::sendMessage(Message * msg)
{


	switch (msg->getMessageType())
	{
	case MessageType::SETDAYTIME:
	{
									DayTimeMessage * renderMessage = static_cast<DayTimeMessage*>(msg);
									switch (renderMessage->getDayTime())
									{
									case DayTime::DAY:
										mesh->setTexture(day);
										break;
									case DayTime::NIGHT:
										mesh->setTexture(night);
										break;
									}
									break;
	}
	}
	return RenderComponent::sendMessage(msg);

}
void Skybox::render(SceneRenderer * sceneRenderer, Mesh* mesh)
{
	glDisable(GL_LIGHTING);
	sceneRenderer->render(mesh);
	glEnable(GL_LIGHTING);
}