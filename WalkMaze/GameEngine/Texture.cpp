#include "Texture.h"


using namespace Engine;


Texture::Texture() : textName(NULL), filename()
{
}

Texture::Texture(std::string& data, int textName) : textName(textName), filename(data)
{
	
}

Texture::Texture(const Texture &otherText)
{
	filename = otherText.filename;
	textName = otherText.textName;
}

GLuint Texture::getTexName()const
{
	return textName;
}

std::string Texture::getFileName()const
{
	return filename;
}



void Texture::destroy(GLuint textures)
{
	glDeleteTextures(1, &textures);
}
