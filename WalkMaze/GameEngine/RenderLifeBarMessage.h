#pragma once
#include "Message.h"
#include "SceneRenderer.h"

namespace Engine{

	class RenderLifeBarMessage : public Message
	{

	public:

		RenderLifeBarMessage(int destId, SceneRenderer* sc);
		~RenderLifeBarMessage();

		SceneRenderer* getSceneRenderer() { return sc; }

	private:
		SceneRenderer* sc;
	};


}