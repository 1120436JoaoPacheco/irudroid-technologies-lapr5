#include "ModelComponent.h"

using namespace Engine;

ModelComponent::ModelComponent(Model* m) : model(m), sequence(0)
{

}

ModelComponent::ModelComponent(const ModelComponent& c)
: Component(c), model(c.model)
{

}
bool ModelComponent::sendMessage(Message* msg)
{
	assert(msg != NULL);
	switch (msg->getMessageType())
	{
	case MessageType::DRAW:
	{
							  RenderMessage * renderMessage = static_cast<RenderMessage*>(msg);
							  if (model->getStudioModel()->GetSequence()!=sequence)
								model->getStudioModel()->SetSequence(sequence);

							  render(renderMessage->getSceneRenderer(), model);
							  break;
	}
	case MessageType::SEQUENCE:
	{
							SequenceMessage * sequenceMessage = static_cast<SequenceMessage*>(msg);
							sequence = sequenceMessage->getNumSequence();
							break;
	}
	default:
		return Component::sendMessage(msg);
	}

	return false;
}

void ModelComponent::render(SceneRenderer * sceneRenderer, Model* model)
{
	sceneRenderer->renderModel(model);
}