#include "ObjectFactory.h"

using namespace Engine;

int ObjectFactory::nextID = 0;

Object* ObjectFactory::createObject()
{
	Object * object = new Object(nextID++);
	return object;
}