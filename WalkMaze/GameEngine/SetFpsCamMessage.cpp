#include "SetFpsCamMessage.h"

using namespace Engine;

SetFpsCamMessage::SetFpsCamMessage(int id, glm::vec3* pos, float angleX, float angleY) 
	: Message(id, MessageType::SET_CAMERA_FPS), pos(pos), angleX(angleX), angleY(angleY)
{

}


SetFpsCamMessage::~SetFpsCamMessage()
{
}
