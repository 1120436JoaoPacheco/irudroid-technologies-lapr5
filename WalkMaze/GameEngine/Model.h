#pragma once
#include "mathlib.h"
#include "studio.h"
#include "mdlviewer.h"
#include <string>

namespace Engine
{
	/**
	*  Represents a geometry to be drawn.
	*  A mesh contains several "sub" meshes that can have different geometries
	*  @see PrimitiveMesh
	*/
	class Model
	{
	public:
		Model(std::string);
		Model(const Model&);
		~Model();

		StudioModel* getStudioModel();

	private:
		StudioModel  * model;
	};


}