#include "LightComponent.h"
#include "SetLightMessage.h"
#include "RenderMessage.h"
#include "Light.h"

using namespace Engine;

 
LightComponent::LightComponent() 
{
}


LightComponent::~LightComponent()
{
	glDisable(light.getGl_light());
}



bool LightComponent::sendMessage(Message* msg)
{

	switch (msg->getMessageType())
	{
	case MessageType::SET_LIGHT:
	{
		SetLightMessage * sm = static_cast<SetLightMessage*> (msg);
		light = *(sm->getLight());
		glEnable(light.getGl_light());
		break;
	}
	case MessageType::SET_TO_DRAW:
	{
		RenderMessage * rm = static_cast<RenderMessage*>(msg);
		rm->getSceneRenderer()->setLightToRender(light);
		break;
	}
	
	default:
		return Component::sendMessage(msg);
	}


	return false;
}
 

