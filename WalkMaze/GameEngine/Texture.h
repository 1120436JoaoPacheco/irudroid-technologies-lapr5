#pragma once
#include "Component.h"
#include "SOIL.h"
#include <stdlib.h>
#include <gl\glew.h>
#include <string>

namespace Engine
{


	class Texture 
	{

	public:
		Texture();
		Texture(std::string& filename, int texName);
		Texture(const Texture &otherText);
		void destroy(GLuint texName);
		GLuint getTexName()const;
		std::string getFileName()const;

	private:
		GLuint textName;
		std::string filename;
		
	};

}