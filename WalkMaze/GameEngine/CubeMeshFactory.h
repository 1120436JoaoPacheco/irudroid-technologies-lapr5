#pragma once
#include "Mesh.h"
#include "QuadPrimitive.h"
namespace Engine
{
	/**
	*  A factory used to create cube meshes
	*  @see Mesh
	*  @see PrimitiveMesh
	*/
	class CubeMeshFactory
	{
	public:

		/**
		*  Gets a shared cube mesh with lenght 1 centered at the origin
		*  @return the shared mesh
		*/
		Mesh * getMesh();

		/**
		*  Creates a cube mesh with length 1 centered at the origin
		*  @return the create mesh
		*/
		Mesh * createMesh();
		
		/**
		* Gets the instance of the factory
		* @return the factory instance
		*/
		static CubeMeshFactory *GetInstance()
		{
			static CubeMeshFactory instance;
			return &instance;
		}


	private:
		CubeMeshFactory();
		~CubeMeshFactory();

		Mesh *mesh;
	};

}