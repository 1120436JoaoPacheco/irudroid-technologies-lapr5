#include "DayTimeMessage.h"

using namespace Engine;

DayTimeMessage::DayTimeMessage(int id, DayTime d): Message(id, MessageType::SETDAYTIME), dt(d)
{
}


DayTimeMessage::~DayTimeMessage()
{
}
