#include "RenderComponent.h"

using namespace Engine;

RenderComponent::RenderComponent(Mesh* m) : mesh(m)
{

}

RenderComponent::RenderComponent(const RenderComponent& c) 
	: Component(c), mesh(c.mesh)
{

}
bool RenderComponent::sendMessage(Message* msg)
{
	assert(msg != NULL);
	switch (msg->getMessageType())
	{
	case MessageType::DRAW:
	{
		RenderMessage * renderMessage = static_cast<RenderMessage*>(msg);
		render(renderMessage->getSceneRenderer(), mesh);
		break;
	}
	default:
		return Component::sendMessage(msg);
	}

	return false;
}

void RenderComponent::render(SceneRenderer * sceneRenderer, Mesh* mesh)
{
	sceneRenderer->render(mesh);
}
