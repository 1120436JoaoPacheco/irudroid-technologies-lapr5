#include "CylinderComponent.h"
#include "RenderMessage.h"
using namespace Engine;

CylinderComponent::CylinderComponent()
{
	quad = gluNewQuadric();
}

CylinderComponent::CylinderComponent(GLdouble base, GLdouble top, GLdouble height, GLint slices, GLint stacks) :
base(base), top(top), height(height), slices(slices), stacks(stacks)
{
	quad = gluNewQuadric();
}

CylinderComponent::~CylinderComponent()
{
}

bool  CylinderComponent::sendMessage(Message* msg)
{
 
	switch (msg->getMessageType())
	{
	case MessageType::DRAW:
	{		 
		RenderMessage * rm = static_cast<RenderMessage*>(msg);
		rm->getSceneRenderer()->renderCylinder(quad, base, top, height, slices, stacks, texture);
		
		break;
	}
	default:
		return Component::sendMessage(msg);
	}

	return false;
}

