#include "CameraComponent.h"

using namespace Engine;

#define HALF_PI 1.5708f 

CameraComponent::CameraComponent() : up(0, 1, 0), right(1, 0, 0), target(0, 0, -1), pos(0, 0, 3), radius(5), angleX(0), angleY(0)
{
}


CameraComponent::~CameraComponent()
{
}



bool CameraComponent::sendMessage(Message* msg)
{

	switch (msg->getMessageType())
	{
	case MessageType::SET_CAMERA:
	{

									//ortbit(*sm->setTarget(), sm->getAngleX(), sm->getAngleY());
									break;
	}
	case MessageType::SET_CAMERA_FPS:
	{
										SetFpsCamMessage * sm = static_cast<SetFpsCamMessage*> (msg);
										if (sm->getAngleX() != 0)
										{
											angleX = sm->getAngleX();

										}
										if (sm->getAngleY() != 0)
										{
											angleY = sm->getAngleY();

										}
										if (sm->getPos() != NULL)
										{
											pos = *sm->getPos();
										}
										if (type != CameraType::FPS)
										{
											setCameraTarget(glm::vec3(0, 0, -1));
											type = CameraType::FPS;
										}
										fps();
										break;
	}
	case MessageType::SET_CAMERA_TPS:
	{
										SetTpsCamMessage * sm = static_cast<SetTpsCamMessage*> (msg);
										if (sm->getAngleX() != 0)
										{
											angleX = sm->getAngleX();
										}
										if (sm->getAngleY() != 0)
										{
											angleY = sm->getAngleY();
										}
										if (sm->getRadius() != NULL)
										{
											radius = sm->getRadius();
										}
										if (type != CameraType::TPS)
										{
											type = CameraType::TPS;
										}
										ortbit(*sm->getPos(), angleX, angleY);
										break;
	}
	case MessageType::SET_CAMERA_TPV:
	{
										SetTpvCamMessage * sm = static_cast<SetTpvCamMessage*> (msg);
										if (sm->getDistance() != 0){
											dist = sm->getDistance();
										}
										if (type != CameraType::TVS)
										{
											type = CameraType::TVS;
										}
										topview(*sm->getPos(),dist, sm->getAngle());
										break;
	}
	case MessageType::SET_TO_DRAW:
	{
									 RenderMessage * rm = static_cast<RenderMessage*>(msg);
									 rm->getSceneRenderer()->setCameraToRender(type, pos, target, up, right);
									 break;
	}
	case MessageType::DRAW:
	{
							  RenderMessage * rm = static_cast<RenderMessage*>(msg);
							  break;
	}
	case MessageType::GET_DIRECTION:
	{
									   GetCameraDirectionMessage * dir = static_cast<GetCameraDirectionMessage*> (msg);

									   if (dir->getUp() != 0)
									   {
										   dir->setUp(&up);
									   }
									   if (dir->getFront() != 0)
									   {
										   dir->setFront(&target);
									   }
									   if (dir->getRight() != 0)
									   {
										   dir->setRight(&right);
									   }
									   break;
	}
	default:
		return Component::sendMessage(msg);
	}


	return false;
}

void CameraComponent::topview(glm::vec3& target, float dist, float angle){

	this->pos.z = target.z;
	this->pos.y = target.y + dist;
	this->pos.x = target.x;

	setCameraTarget(target);

	this->up = glm::rotateY(glm::vec3(0, 0, 1), angle);
	this->right = glm::rotateY(glm::vec3(1, 0, 0), angle);
}


void CameraComponent::ortbit(glm::vec3& target, float angleX, float angleY)
{
	//theta = y ; phi = x
	//printf(" POLAR = %f ; LATITUDE = %f\n", angleX, angleY);

	angleX = glm::radians(angleX);
	angleY = glm::radians(angleY);

	if (angleY != 0 && angleX <= 0)
	{
		angleX = 0.01f;
	}


	this->pos.z = target.z + radius*glm::cos(angleY)*glm::sin(angleX);
	this->pos.x = target.x + radius*glm::sin(angleY)*glm::sin(angleX);
	this->pos.y = target.y + radius*glm::cos(angleX);

	setCameraTarget(target);


}

void CameraComponent::fps()
{

	/*printf("angle = %f\n", angle);
	float radians = glm::radians(angle);
	target = glm::normalize(glm::rotateX(target, radians));
	printf("X = %f; Y = %f; Z = %f \n", this->target.x, this->target.y, this->target.z);
	up = glm::normalize(glm::rotateX(up, radians));*/

	target = glm::vec3(
		cos(angleY) * sin(angleX),
		sin(angleY),
		cos(angleY) * cos(angleX)
		);

	
	setCameraTarget((target * 1.5f)+ pos);

}



void CameraComponent::setCameraTarget(glm::vec3 target)
{
	glm::vec3 projectedTarget, savedTarget = target;

	target = target - this->pos;

	projectedTarget = target;

	if (fabs(target.x) < 0.00001f && fabs(target.z) < 0.00001f) {  // YZ 
		projectedTarget.x = 0.0;
		projectedTarget = glm::normalize(projectedTarget);

		this->right = glm::vec3(1, 0, 0);
		this->up = glm::cross(projectedTarget, right);


		this->target = target;
		this->right = -glm::cross(target, this->up);
	}
	else
	{
		projectedTarget.y = 0.0f;
		projectedTarget = glm::normalize(projectedTarget);

		this->up = glm::vec3(0.0f, 1.0f, 0.0f);
		this->right = -glm::cross(projectedTarget, this->up);

		this->target = target;
		this->up = glm::cross(this->target, this->right);
	}
	if (type == CameraType::FPS)
		this->target = glm::normalize(this->target);
	else
		this->target = this->target;
	this->right = glm::normalize(this->right);
	this->up = glm::normalize(this->up);
}
