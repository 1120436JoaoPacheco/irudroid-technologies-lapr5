#pragma once
#include "TransformMessage.h"
#include "Transform.h"
#include <vector>
namespace Engine
{
	class SetTransformMessage : public TransformMessage
	{
	public:
		SetTransformMessage(int, std::vector<Transform>* t);
		virtual ~SetTransformMessage();
	};
}
