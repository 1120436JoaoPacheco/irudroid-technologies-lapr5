#pragma once

#include "PrimitiveMesh.h"
#include "Texture.h"
#include "Material.h"

namespace Engine
{
	/**
	*  Represents a geometry to be drawn.
	*  A mesh contains several "sub" meshes that can have different geometries
	*  @see PrimitiveMesh
	*/
	class Mesh
	{
	public:
		Mesh(bool storeInList);
		Mesh(const Mesh&);
		~Mesh();

		/**
		*  Adds a sub mesh
		*  @param m a sub mesh to be added
		*/
		void addPrimitive(PrimitiveMesh const & m);

		bool storeInList() { return store; }

		bool listGenerated() { return generated; }

		void setListIndex(unsigned int i) { listIndex = i; generated = true; }

		unsigned int getListIndex() { return listIndex; }

		void setMaterial(const Material& m) { material = m; }
		void setTexture(const Texture& t) { texture = t; }

		Material getMaterial() { return material; }
		Texture  getTexture() { return texture; }
		
		/**
		*  Gets all the submeshes of the mesh
		*  @return the submeshes
		*  @see PrimitiveMesh
		*/
		std::vector<PrimitiveMesh*> getPrimitives();

	private:
		std::vector<PrimitiveMesh*> primitiveList;

		Texture texture;
		Material material;
		unsigned int listIndex;

		bool store, generated;
	};


}