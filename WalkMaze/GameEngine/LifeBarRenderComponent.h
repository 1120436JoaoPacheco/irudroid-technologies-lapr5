#pragma once
#include "Component.h"
#include "RenderLifeBarMessage.h"	
#include "TranformLifeBarMessage.h"	

namespace Engine{

	class LifeBarRenderComponent : public Component
	{

	public:

		LifeBarRenderComponent(glm::vec3, glm::vec3);
		~LifeBarRenderComponent();

		bool sendMessage(Message * msg);

	private:

		void render(SceneRenderer * sceneRenderer);
		glm::vec3 color;
		glm::vec3 size;
	};




}