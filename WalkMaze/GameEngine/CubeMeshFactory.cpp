#include "CubeMeshFactory.h"

using namespace Engine;

CubeMeshFactory::CubeMeshFactory()
{
	mesh = createMesh();
}


CubeMeshFactory::~CubeMeshFactory()
{
}

Mesh * CubeMeshFactory::createMesh()
{
	//TODO MAKE ALL THE FACES

	glm::vec3 v[] =
	{
		glm::vec3(-0.5f, -0.5f, -0.5f), //0 
		glm::vec3(0.5f, -0.5f, -0.5f), //1
		glm::vec3(0.5f, 0.5f, -0.5f), //2
		glm::vec3(-0.5f, 0.5f, -0.5f), //3
		glm::vec3(-0.5f, -0.5f, 0.5f), //4
		glm::vec3(0.5f, -0.5f, 0.5f), //5
		glm::vec3(0.5f, 0.5f,	0.5f), //6
		glm::vec3(-0.5f, 0.5f, 0.5f), //7
	};

	//Front Face
	QuadPrimitive frontFace = QuadPrimitive(v[7], v[6], v[4], v[5]);

	//Left Face
	QuadPrimitive leftFace = QuadPrimitive(v[3], v[7], v[0], v[4]);

	//Back Face
	QuadPrimitive backFace = QuadPrimitive(v[2], v[3], v[1], v[0]);

	//Right Face
	QuadPrimitive rightFace = QuadPrimitive(v[6], v[2], v[5], v[1]);

	//Bottom Face
	QuadPrimitive bottomFace = QuadPrimitive(v[4], v[5], v[0], v[1]);

	//Top Face
	QuadPrimitive topFace = QuadPrimitive(v[3], v[2], v[7], v[6]);


	Mesh * m = new Mesh(true);
	m->addPrimitive(frontFace);
	m->addPrimitive(leftFace);
	m->addPrimitive(backFace);
	m->addPrimitive(rightFace);
	m->addPrimitive(bottomFace);
	m->addPrimitive(topFace);

	return m;
}

Mesh * CubeMeshFactory::getMesh()
{
	if (mesh == NULL)
		mesh = createMesh();
	return mesh;
}
