#pragma once
#include "Object.h"

namespace Engine
{
	/**
	*  A factory that creates objects with a unique id
	*  @see Object
	*/
	class ObjectFactory
	{
	public:
		~ObjectFactory() {}

		/**
		*  Create a object with a unique id
		*  @return a object with a specific id
		*/
		Object* createObject();

		/**
		*  Get the factory instance
		*  @return the factory instance
		*/
		static ObjectFactory *Get()
		{
			static ObjectFactory instance;
			return &instance;
		}

	private:
		static int nextID;
		ObjectFactory() {}
	};

}