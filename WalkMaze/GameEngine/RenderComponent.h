#pragma once
#include "Component.h"
#include "RenderMessage.h"	
#include "RenderMiniMapMessage.h"

namespace Engine
{
	class Mesh;
	
	/**
	*  A component that draws a geometry
	*  @see Component
	*  @see Mesh
	*/
	class RenderComponent :	public Component
	{
		
	public:
		/**
		*  Create a component that renders the given mesh
		*  @param mesh the mesh to render
		*  @see Mesh
		*/
		RenderComponent(Mesh* mesh);
		RenderComponent(const RenderComponent &);

		/**
		*  Overrided 
		*  @see Component
		*/
		virtual bool sendMessage(Message * msg);

	private:

		/**
		*  Sends mesh to render with the given scene renderer
		*  @param sceneRenderer the scene renderer
		*  @param mesh the mesh to render
		*  @see SceneRenderer
		*  @see Mesh
		*/
		virtual void render(SceneRenderer * sceneRenderer, Mesh * mesh);
	protected:
		Mesh * mesh;	
	};

}