#include "Scene.h"

using namespace Engine;

Scene::Scene() 
{
	camera = addNewObject();
	camera->addComponent(new CameraComponent());
}

Scene::~Scene()
{
	std::map<int, Object*>::iterator it = objects.begin();

	//Deletes all objects created by the scene
	for (it; it != objects.end(); ++it)
	{
		delete it->second;
	}
}

bool Scene::sendMessage(Message* msg)
{
	//Look for the object by the id
	std::map<int, Object*>::iterator it = objects.find(msg->getDestinationID());

	if (it != objects.end()) //If found the object
	{
		return it->second->sendMessage(msg); //Send the message
	}

	return false; //else return false
}

bool Scene::sendallMessage(Message* msg)
{
	bool r = false;
	std::map<int, Object*>::iterator it = objects.begin();

	for (; it != objects.end(); ++it)
	{
		r |= it->second->sendMessage(msg);
	}

	return r;
}

Object* Scene::getCameraObject()
{
	return camera;
}

void Scene::removeObject(int id)
{
	objects.erase(objects.find(id));
}

Object * Scene::addNewObject()
{
	Object* newObject = ObjectFactory::Get()->createObject();
	objects[newObject->getObjectId()] = newObject;

	return newObject;
}

void Scene::renderScene(int width, int height)
{
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(42.5f, (GLfloat)width / height, 1, 500.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	//glEnable(GL_CULL_FACE);


	sc.clear();
	//printf("SET TO RENDER\n");

	//camera->sendMessage(&PrepareToRenderMessage(0, &sc));
	sendallMessage(&PrepareToRenderMessage(0, &sc));

	RenderMessage render(0, &sc);

	sendallMessage(&render);
	//printf("FINISHED\n");
	renderMiniMap(width, height);
	renderLifeBar(width, height);
	renderPointBar(width, height);

	glViewport(0, 0, 0, 0);
}

void Scene::renderMiniMap(int width, int height){

	// glViewport(botom, left, width, height)
	float miniMapwidth = width / 5.0f;
	float border = 5.0f;
	glViewport(width - miniMapwidth - border, height - miniMapwidth - border, miniMapwidth, miniMapwidth);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, 100, 0,100);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	glBegin(GL_QUADS);
	glColor4f(0.0, 0.0, 0.0, 0.2);
	glVertex2f(0, 0);
	glVertex2f(0, 100);
	glVertex2f(100, 100);
	glVertex2f(100, 0);
	glEnd();
	glDisable(GL_BLEND);
	sendallMessage(&RenderMiniMapMessage(0, &sc));
	
	// scale ? prepartorenderminimap ? mensagem ? 
}

void Scene::renderLifeBar(int width, int height){

	// glViewport(botom, left, width, height)
	float barwidth = 128;
	float barheight = 25;
	float border = 5.0f;
	glViewport(width - barwidth - border, border, barwidth, barheight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, 100, 0, 100);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	glBegin(GL_QUADS);
	glColor4f(0.0, 0.0, 0.0, 0.2);
	glVertex2f(0, 0);
	glVertex2f(0, 100);
	glVertex2f(100, 100);
	glVertex2f(100, 0);
	glEnd();
	glDisable(GL_BLEND);
	sendallMessage(&RenderLifeBarMessage(0, &sc));

	// scale ? prepartorenderminimap ? mensagem ? 
}

void Scene::renderPointBar(int width, int height){

	// glViewport(botom, left, width, height)
	float barwidth = 100;
	float barheight = 25;
	float border = 5.0f;
	glViewport(border, border, barwidth, barheight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, 100, 0, 100);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	glBegin(GL_QUADS);
	glColor4f(1, 1, 1, 1);
	glVertex2f(0, 0);
	glVertex2f(0, 100);
	glVertex2f(100, 100);
	glVertex2f(100, 0);
	glEnd();
	glDisable(GL_BLEND);



}