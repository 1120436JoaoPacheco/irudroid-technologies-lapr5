#pragma once
#include "Component.h"
#include "GL\glew.h"
#include "Texture.h"
 
namespace Engine
{

	class CylinderComponent : public Component
	{
	public:
	 
        CylinderComponent(GLdouble base, GLdouble top, GLdouble height, GLint slices, GLint stack);
		CylinderComponent();
		~CylinderComponent();
		void setTexture(const Texture& t) { texture = t; }
		Texture  getTexture() { return texture; }
		bool  CylinderComponent::sendMessage(Message* msg);
	private:
		GLUquadric*  	quad;
		GLdouble  	base;
		GLdouble  	top;
		GLdouble  	height;
		GLint  	slices;
		GLint  	stacks;
		Texture texture;
		 
		void createCylinder();
		 
	};

}