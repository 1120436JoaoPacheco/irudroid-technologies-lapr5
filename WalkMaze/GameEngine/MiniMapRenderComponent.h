#pragma once
#include "Component.h"
#include "RenderMiniMapMessage.h"	

namespace Engine{

	class MiniMapRenderComponent : public Component
	{

	public:

		MiniMapRenderComponent(Mesh*, glm::vec3, glm::vec3, float);
		MiniMapRenderComponent(const MiniMapRenderComponent &);
		~MiniMapRenderComponent();

		bool sendMessage(Message * msg);

	private:

		void render(SceneRenderer * sceneRenderer, Mesh * m);
		glm::vec3 color;
		glm::vec3 scale;
		float rot;
		Mesh * mesh;
	};




}