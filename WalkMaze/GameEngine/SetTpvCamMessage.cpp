#include "SetTpvCamMessage.h"

using namespace Engine;

SetTpvCamMessage::SetTpvCamMessage(int id, glm::vec3* pos, float dis, float angle)
: Message(id, MessageType::SET_CAMERA_TPV), pos(pos), dis(dis), angle(angle)
{

}


SetTpvCamMessage::~SetTpvCamMessage()
{
}
