#pragma once

#include "Mesh.h"
#include "Model.h"
#include "glm\glm.hpp"
#include "GL\glew.h"
#include "Rotation.h"
#include <map>
#include "Transform.h"
#include "Light.h"

namespace Engine
{

	enum class CameraType
	{
		FPS, TPS, TVS
	};

	/**
	*  The render that draws all the objects of the scene
	*  @see Mesh
	*  @see PrimitiveMesh
	*  @see RenderMessage
	*/
	class SceneRenderer
	{
	public:
		SceneRenderer();
		~SceneRenderer();

		void setCameraToRender(CameraType type, glm::vec3& pos, glm::vec3& direction, glm::vec3& up, glm::vec3& right);
		void setLightToRender(Light& light);
		/**
		*  Renderes a mesh to the current target with the current Transformation
		*  @param m the mesh to draw
		*  @see Mesh
		*  @see setRenderTransformContext
		*/
		void render(Mesh * m);
		
		void renderMinimapMesh(Mesh * m, glm::vec3& color, glm::vec3& scale,float);
		void renderLifeBarMesh(glm::vec3 size, glm::vec3 color);
		void renderBillboardMesh(Mesh * m, glm::vec3 position, glm::vec3 scale, glm::vec3 color);

		void renderModel(Model * m);

		/**
		*  Sets the current Transformation that affect all the objects drawn
		*  @param pos Position
		*  @param scal Scale
		*  @param rotation Rotation
		*/
		void setRenderTransformContext(std::vector<Transform>* t);

		void clear();

		void SceneRenderer::renderCylinder(GLUquadric* quad,GLdouble base, GLdouble top, GLdouble height, GLint slices, GLint stack, Texture texture);

	private:
		/**
		*  Renders a list of quads
		*  @param m the list of quads
		*  @see QuadPrimitive
		*/
		void renderQuads(PrimitiveMesh*& m);

		/**
		*  Renders a list of triangles
		*  @param m the list of triangles
		*  @see TrianglePrimitive
		*/
		void renderTriangles(PrimitiveMesh*& m);

		void renderInternal(Mesh *m);

		std::vector<Transform>* Transforms;

		glm::vec3 cameraUp, cameraDirection, cameraRight, cameraPos;
		CameraType cameraType;

	};

}