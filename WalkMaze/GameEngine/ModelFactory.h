#pragma once
#include "Model.h"

namespace Engine{

	enum class ModelType{ PLAYER, ENEMY_ALIEN, ENEMY_RAPTOR, SHELVE, CRATE, CHAIR, BARREL, STATUE, WARDROBE, WRECKEDTABLE, PIANO, PLANT, BULLET,LIFEBONUS, POINTBONUS };

	class ModelFactory{

	public:

		~ModelFactory() {}
		Model* createModel(ModelType);
		
		static ModelFactory *Get()
		{
			static ModelFactory instance;
			return &instance;
		}

	private:

		ModelFactory() {}




	};






}