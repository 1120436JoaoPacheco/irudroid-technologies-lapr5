#include "Model.h"

using namespace Engine;

Model::Model(std::string type){
	model = new StudioModel();

	char* temp = (char*)type.c_str();
	mdlviewer_init(temp, *model);
	
}

Model::Model(const Model& m){ model = m.model; }

Model::~Model(){ delete model; }

StudioModel *Model::getStudioModel()
{
	return model;
}