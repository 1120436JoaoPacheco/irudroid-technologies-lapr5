#include "SetTpsCamMessage.h"

using namespace Engine;

SetTpsCamMessage::SetTpsCamMessage(int id, glm::vec3* pos, float angleX, float angleY, float radius)
: Message(id, MessageType::SET_CAMERA_TPS), pos(pos), angleX(angleX), angleY(angleY), radius(radius)
{

}


SetTpsCamMessage::~SetTpsCamMessage()
{
}
