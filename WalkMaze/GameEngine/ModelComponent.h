#pragma once
#include "Component.h"
#include "RenderMessage.h"	
#include "SequenceMessage.h"	


namespace Engine
{
	class Model;

	/**
	*  A component that draws a geometry
	*  @see Component
	*  @see Mesh
	*/
	class ModelComponent : public Component
	{

	public:
		/**
		*  Create a component that renders the given mesh
		*  @param mesh the mesh to render
		*  @see Mesh
		*/
		ModelComponent(Model* model);
		ModelComponent(const ModelComponent &);

		/**
		*  Overrided
		*  @see Component
		*/
		bool sendMessage(Message * msg);

	private:

		/**
		*  Sends mesh to render with the given scene renderer
		*  @param sceneRenderer the scene renderer
		*  @param mesh the mesh to render
		*  @see SceneRenderer
		*  @see Mesh
		*/
		void render(SceneRenderer * sceneRenderer, Model * model);

		Model * model;
		int sequence;
		bool dirty;
	};

}