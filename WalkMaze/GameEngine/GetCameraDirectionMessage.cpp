#include "GetCameraDirectionMessage.h"

using namespace Engine;

GetCameraDirectionMessage::GetCameraDirectionMessage(int id) : Message(id, MessageType::GET_DIRECTION)
{

}


GetCameraDirectionMessage ::~GetCameraDirectionMessage()
{
}


//GETS

glm::vec3* GetCameraDirectionMessage::getUp()
{
	return up;
}

glm::vec3* GetCameraDirectionMessage::getRight()
{
	return right;
}

glm::vec3* GetCameraDirectionMessage::getFront()
{
	return front;
}



//SETS

void GetCameraDirectionMessage::setUp(glm::vec3* up)
{
	this->up = up;
}

void GetCameraDirectionMessage::setRight(glm::vec3* right)
{
	this->right = right;
}

void GetCameraDirectionMessage::setFront(glm::vec3* front)
{
	this->front = front;
}