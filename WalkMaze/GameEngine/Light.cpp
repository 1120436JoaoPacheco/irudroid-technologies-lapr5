#include "Light.h"
#include "glm\vec4.hpp"
using namespace Engine;


Light::Light() : gl_light(GL_LIGHT0), gl_ambient(0.0, 0.0, 0.0, 1.0), gl_diffuse(1.0, 1.0, 1.0, 1.0), gl_specular(1.0, 1.0, 1.0, 1.0),
gl_position(0.0, 0.0, 1.0, 0.0), gl_spot_direction(0.0, 0.0, -1.0), gl_spot_cutoff(180.0), gl_spot_exponent(0.0),
gl_quadratic_attenuation(0.0), gl_linear_attenuation(0.0)
{
}

Light::Light(GLenum gl_light, glm::vec4& gl_ambient, glm::vec4& gl_diffuse, glm::vec4& gl_specular, glm::vec4& gl_position, GLfloat& gl_quadratic_attenuation,
	GLfloat& gl_linear_attenuation, glm::vec3& gl_spot_direction)
	: gl_light(gl_light), gl_ambient(gl_ambient), gl_diffuse(gl_diffuse), gl_specular(gl_specular), gl_position(gl_position), gl_spot_direction(gl_spot_direction), gl_spot_exponent(gl_spot_exponent),
	gl_quadratic_attenuation(gl_quadratic_attenuation), gl_linear_attenuation(gl_linear_attenuation)

{
}

Light::Light(GLenum gl_light, glm::vec4& gl_ambient, glm::vec4& gl_diffuse, glm::vec4& gl_specular, glm::vec4& gl_position,
	glm::vec3& gl_spot_direction, GLfloat& gl_spot_cutoff, GLfloat& gl_spot_exponent , GLfloat& gl_quadratic_attenuation,
	GLfloat& gl_linear_attenuation)
	: gl_light(gl_light), gl_ambient(gl_ambient), gl_diffuse(gl_diffuse), gl_specular(gl_specular),
	gl_position(gl_position), gl_spot_direction(gl_spot_direction), gl_spot_cutoff(gl_spot_cutoff), gl_spot_exponent(gl_spot_exponent),
	gl_quadratic_attenuation(gl_quadratic_attenuation), gl_linear_attenuation(gl_linear_attenuation)
{
}


Light::Light(const Light &otherText)
{ 
	gl_ambient = otherText.gl_ambient;
	gl_diffuse = otherText.gl_diffuse;
	gl_specular = otherText.gl_specular;
	gl_position = otherText.gl_position;
	gl_spot_direction = otherText.gl_spot_direction;
	gl_spot_cutoff = otherText.gl_spot_cutoff;
	gl_spot_exponent = otherText.gl_spot_exponent;
	gl_light = otherText.gl_light;
	gl_quadratic_attenuation = otherText.gl_quadratic_attenuation;
	gl_linear_attenuation = otherText.gl_linear_attenuation;
}

//GETS
GLfloat Light::getGl_quadratic_attenuation()
{
	return gl_quadratic_attenuation;
}
GLfloat Light::getGl_linear_attenuation()
{
	return gl_linear_attenuation;
}

GLenum Light::getGl_light()
{
	return gl_light;
}
GLfloat Light::getGl_spot_exponent()
{
	return gl_spot_exponent;
}
GLfloat Light::getGl_spot_cutoff()
{
	return gl_spot_cutoff;
}

glm::vec4 Light::getGl_ambient()
{
	return gl_ambient;
}

glm::vec4 Light::getGl_diffuse()
{
	return  gl_diffuse;
}

glm::vec4 Light::getGl_specular()
{
	return gl_specular;
}

glm::vec4 Light::getGl_position()
{
	return gl_position;
}

glm::vec3 Light::getGl_spot_direction()
{
	return gl_spot_direction;
}

//SETS

void Light::setGl_ambient(glm::vec4& gl_ambient)
{
	this->gl_ambient = gl_ambient;
}

void Light::setGl_light(GLenum& gl_light)
{
	this->gl_light = gl_light;
}
void Light::setGl_diffuse(glm::vec4 &gl_diffuse)
{
	this->gl_diffuse = gl_diffuse;
}

void Light::setGl_specular(glm::vec4 &gl_specular)
{
	this->gl_specular = gl_specular;
}

void Light::setGl_position(glm::vec4 &gl_position)
{
	this->gl_position = gl_position;
}

void Light::setGl_spot_direction(glm::vec3 &gl_spot_direction)
{
	this->gl_spot_direction = gl_spot_direction;
}

void Light::setGl_spot_cutoff(GLfloat &gl_spot_cutoff)
{
	this->gl_spot_cutoff = gl_spot_cutoff;
}

void Light::setGl_spot_exponent(GLfloat &gl_spot_exponent)
{
	this->gl_spot_exponent = gl_spot_exponent;
}
void Light::setGl_quadratic_attenuation(GLfloat &gl_spot_exponent)
{
	this->gl_quadratic_attenuation = gl_quadratic_attenuation;
}

void Light::setGl_linear_attenuation(GLfloat &gl_linear_attenuation)
{
	this->gl_linear_attenuation = gl_linear_attenuation;
}