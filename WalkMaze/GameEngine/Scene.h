#pragma once
#include <map>
#include "Object.h"
#include "ObjectFactory.h"
#include "SceneRenderer.h"
#include "PrepareToRenderMessage.h"
#include "RenderMiniMapMessage.h"
#include "RenderLifeBarMessage.h"
#include "RenderMessage.h"
#include "CameraComponent.h"

namespace Engine
{

	/**
	*  A container of all objects to be managed
	*  @see SceneRenderer
	*  @see Object
	*  @see Message
	*/
	class Scene
	{
	public:
		Scene();

		~Scene();
		/**
		*  Sends a message to a object in the scene
		*  @param msg the message
		*  @return true if the message was handled false if not
		*  @see Message
		*/
		bool sendMessage(Message * msg);
		

		bool sendallMessage(Message* msg);

		/**
		*  Creates a new object and adds it to the scene
		*  @return the object created
		*/
		Object* addNewObject();
		
		/**
		*  Removes the object associated with the specific id 
		*  @param id the id of the object
		*/
		void removeObject(int id);
		
		Object*  getCameraObject();

		/**
		*  Renders the scene
		*/
		void renderScene(int width, int height);

	private:
		
		void renderMiniMap(int width, int height);
		void renderLifeBar(int width, int height);
		void renderPointBar(int width, int height);

		std::map<int, Object*> objects;
		SceneRenderer sc;
		Object*  camera;
	};
}