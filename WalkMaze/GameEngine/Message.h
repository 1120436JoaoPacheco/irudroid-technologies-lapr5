#pragma once
#include "MessageType.h"

namespace Engine
{
	/**
	* A message to send to Objects and Components. 
	* A message can be anything needed to send or receive information to 
	* Objects and Components.
	* @see Component
	* @see Object
	* @see MessageType
	*/
	class Message
	{
	protected:
		Message(int destinationObjectId, MessageType messageType);
		~Message();

	public:
		/**
		*  Gets the id of the object that this message is for
		*  @return the id of the destination
		*/
		int getDestinationID() { return destId; }
		
		/**
		* Gets the message type of this message
		* return the message type
		*/
		MessageType getMessageType() { return messageType; }

	private:
		int destId;
		MessageType messageType;
	};
}