#pragma once
#include "Message.h"
#include "SceneRenderer.h"

namespace Engine{

	class RenderMiniMapMessage : public Message
	{

	public:

		RenderMiniMapMessage(int destId, SceneRenderer* sc);
		~RenderMiniMapMessage();

		SceneRenderer* getSceneRenderer() { return sc; }
	
	private:
		SceneRenderer* sc;
	};


}