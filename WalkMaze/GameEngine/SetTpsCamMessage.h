#pragma once
#include "Message.h"
#include "glm\vec3.hpp"
namespace Engine
{
	class SetTpsCamMessage : public Message
	{
	public:
		SetTpsCamMessage(int id, glm::vec3* pos, float angleX, float angleY, float radius);
		~SetTpsCamMessage();

		float getAngleX(){ return angleX; }
		float getAngleY(){ return angleY; }
		glm::vec3* getPos() { return pos; }
		float getRadius(){ return radius; }

	private:
		float angleY;
		float angleX;
		glm::vec3* pos;
		float radius;
	};
}
