#include "TrianglePrimitive.h"

using namespace Engine;

TrianglePrimitive::TrianglePrimitive() : PrimitiveMesh(PrimitiveType::TRIANGLE)
{
	//TOP
	meshInfo.push_back(new glm::vec3(0.0f, 0.5f, 0.0f));

	//LEFT BOTTOM
	meshInfo.push_back(new glm::vec3(-0.5f, -0.5f, 0.0f));

	//RIGHT BOTTOM
	meshInfo.push_back(new glm::vec3(0.5f, -0.5f, 0.0f));
}

TrianglePrimitive::TrianglePrimitive(glm::vec3& T, glm::vec3& lB, glm::vec3 rB) : PrimitiveMesh(PrimitiveType::TRIANGLE)
{
	//TOP
	meshInfo.push_back(new glm::vec3(T));

	//LEFT BOTTOM
	meshInfo.push_back(new glm::vec3(lB));

	//RIGHT BOTTOM
	meshInfo.push_back(new glm::vec3(rB));
}


TrianglePrimitive::~TrianglePrimitive()
{
}
