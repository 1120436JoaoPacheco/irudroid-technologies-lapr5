#include "SequenceMessage.h"

using namespace Engine;

SequenceMessage::SequenceMessage(int id, int numSeq)
: Message(id, MessageType::SEQUENCE), numSequence(numSeq)
{
}


SequenceMessage::~SequenceMessage()
{
}
