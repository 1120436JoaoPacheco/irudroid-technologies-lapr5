#include "Billboard.h"

using namespace Engine;
Billboard::Billboard(glm::vec3 pos, glm::vec3 size, glm::vec3 color) : RenderComponent(new Mesh(true)), pos(pos), size(size), color(color)
{

}

Billboard::~Billboard()
{
}

bool Billboard::sendMessage(Message * msg)
{
	assert(msg != NULL);
	switch (msg->getMessageType())
	{
	case MessageType::Tranform_BB:
	{
		TranformBillboardMessage * renderBBMessage = static_cast<TranformBillboardMessage*>(msg);
		size = renderBBMessage->getSize();
		color = renderBBMessage->getColor();
		render(new SceneRenderer(), mesh);
		break;
	}
	default:
		return RenderComponent::sendMessage(msg);
	}

	return false;
}

void Billboard::render(SceneRenderer * sceneRenderer, Mesh* mesh)
{
	glDisable(GL_LIGHTING);
	glDisable(GL_CULL_FACE);
	sceneRenderer->renderBillboardMesh(mesh, pos, size,color);
	glEnable(GL_CULL_FACE);
	glEnable(GL_LIGHTING);
}