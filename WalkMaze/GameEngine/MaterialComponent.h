#pragma once
#include "Component.h"
#include "Message.h"
#include "glm\vec4.hpp"
#include <stdlib.h>
#include <gl\glew.h>

namespace Engine
{

	class MaterialComponent :
		public Component
	{

	public:
		/* Overrided /*/
		bool sendMessage(Message * msg);
		MaterialComponent();
		MaterialComponent(glm::vec4& gl_ambient, glm::vec4& gl_diffuse, glm::vec4& gl_specular);
		MaterialComponent(glm::vec4& gl_ambient, glm::vec4& gl_diffuse, glm::vec4& gl_specular, float gl_shininess, glm::vec4& gl_emission);
		void setGl_ambiente(glm::vec3 gl_ambient);

	private:
		glm::vec4 gl_ambient;
		glm::vec4 gl_diffuse;
		glm::vec4 gl_specular;
		float gl_shininess;
		glm::vec4 gl_emission;
	};

}