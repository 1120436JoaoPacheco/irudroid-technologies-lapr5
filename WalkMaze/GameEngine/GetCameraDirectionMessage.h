#pragma once
#include "glm\vec3.hpp"
#include "Message.h"

namespace Engine
{
	class GetCameraDirectionMessage : public Message
	{
	
	public:
		GetCameraDirectionMessage(int id);
		virtual ~GetCameraDirectionMessage();

		glm::vec3 * up, *right, *front;

		glm::vec3* getUp();
		glm::vec3* getRight();
		glm::vec3* getFront();


		void setUp(glm::vec3 *up);
		void setRight(glm::vec3* right);
		void setFront(glm::vec3* front);


	};
}
