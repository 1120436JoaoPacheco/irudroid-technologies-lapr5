#include "RenderMessage.h"

using namespace Engine;

RenderMessage::RenderMessage(int destID, SceneRenderer* sr) 
	: Message(destID, MessageType::DRAW), sceneRenderer(sr)

{
}


RenderMessage::~RenderMessage()
{
}
