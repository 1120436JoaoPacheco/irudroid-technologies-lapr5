#pragma once
#include "Message.h"
#include "glm\vec3.hpp"
namespace Engine
{
	class SetTpvCamMessage : public Message
	{
	public:
		SetTpvCamMessage(int id, glm::vec3* pos, float dist, float angle);
		~SetTpvCamMessage();

		glm::vec3* getPos() { return pos; }
		float getDistance(){ return dis; }
		float getAngle(){ return angle; }
	private:
		glm::vec3* pos;
		float dis,angle;
	};
}