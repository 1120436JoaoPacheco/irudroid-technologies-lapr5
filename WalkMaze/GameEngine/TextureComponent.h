#pragma once
#include "Component.h"
#include "Message.h"
#include "glm\vec4.hpp"
#include <stdlib.h>
#include <gl\glew.h>


namespace Engine
{

	typedef struct {
		int         sizeX, sizeY, bpp;
		char        *data;
	}JPGImage;

	typedef struct {
		int         sizeX, sizeY;
		char        *data;
	} PPMImage;

	extern "C" int read_JPEG_file(char *, char **, int *, int *, int *);
	extern "C" PPMImage *LoadPPM(char *);

	class TextureComponent :
		public Component
	{

	public:
		/* Overrided /*/
		bool sendMessage(Message * msg);
		TextureComponent(char *filename);

	private:
		JPGImage imagemJPG;
		PPMImage *imagemPPM;
		char *filename;
	};

}