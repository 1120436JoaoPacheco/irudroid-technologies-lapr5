#include "GetTransformMessage.h"

using namespace Engine;

GetTransformMessage::GetTransformMessage(int destId) : TransformMessage(destId, MessageType::GET_Transform)
{
}


GetTransformMessage::~GetTransformMessage()
{
}
